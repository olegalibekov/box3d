import 'dart:io';

import 'package:path/path.dart' as path;

Future<void> main(params) async {
  print('start');
  final Directory src = Directory('../ts_engine');
  List noNeed = ['Vec3', 'Record', 'Map', 'Infinity'];

  // print((await Process.run(
  //     "Xcopy", [src.path, Directory('src_out').path,"/E /H /C /I"])).exitCode);
  //copyDirectory(src, Directory('src_out'));

  for (var element in src.listSync(recursive: true)) {

    if (element is File && element.path.contains('.ts') && !noNeed.contains(path.basename(element.path))) {
      var done =
          Process.runSync("ts2dart", [element.absolute.path], runInShell: true);
      print("ready: ${element.path}");
      String dartFileContent = element.absolute.uri.path
          .replaceAll('.ts', '.dart')
          .replaceAll('ts_engine', 'lib')
          .replaceFirst('/', '');
      File(dartFileContent).createSync(recursive: true);
      File dartFile = File(element.absolute.uri.path
          .replaceAll('.ts', '.dart')
          .replaceFirst('/', ''))
        ..copySync(dartFileContent)
        ..deleteSync();
    }
  }
}

void copyDirectory(Directory source, Directory destination) =>
    source.listSync(recursive: false).forEach((var entity) {
      if (entity is Directory) {
        var newDirectory = Directory(
            path.join(destination.absolute.path, path.basename(entity.path)));
        newDirectory.createSync();

        copyDirectory(entity.absolute, newDirectory);
      } else if (entity is File) {
        entity
            .copySync(path.join(destination.path, path.basename(entity.path)));
      }
    });
