import '../lib/box3d.dart';
import 'package:test/test.dart';

void main() {
  World world;
  test('world create test', () {
    world = new World();
    expect(world != null, true);
  });
  test('world gravity set', () {
    world.gravity.set(0, 0, -9.82);
    expect(world.gravity.z, -9.82);
  });
  Body sphereBody;
  test('sphere addition', () {
    var radius = 1;
    sphereBody = new Body(
        BodyOptions(mass: 5, position: Vec3(0, 0, 10), shape: Sphere(radius)));
    world.addBody(sphereBody);
  });

  Body groundBody;
  Plane groundShape;
  test('plane adding', (){
    groundBody = new Body(
      BodyOptions(mass: 0)
    );
    groundShape = new Plane();
    groundBody.addShape(groundShape);
    world.addBody(groundBody);
  });

  Body boxBody;
  test('box adding', (){
    var sideLength = 10.0;
    boxBody = new Body(
      BodyOptions(mass: 0, shape: Box(Vec3(sideLength, sideLength, sideLength)))
    );
    world.addBody(boxBody);
  });

  var fixedTimeStep = 1.0 / 60.0;
  var maxSubSteps = 3;
  var lastTime = DateTime.now().millisecondsSinceEpoch;
  test('world loop step', () {
    //print("Sphere z position before: ${sphereBody.position.z}");
    var dt = (lastTime - DateTime.now().millisecondsSinceEpoch) / 1000;
    world.step(fixedTimeStep, dt, maxSubSteps);
    //print("Sphere z position: ${sphereBody.position.z}");
  });

  test('world loop 10seconds simulation', () async {
    world.gravity.set(0, 0, 1.9);
    print("Sphere z position before: ${sphereBody.position.z}");
    Stopwatch stopwatch = Stopwatch();
    stopwatch.start();
    while (stopwatch.elapsed < Duration(seconds: 10)) {
      lastTime = DateTime.now().millisecondsSinceEpoch;
      var dt = (lastTime - DateTime.now().millisecondsSinceEpoch) / 1000;
      world.step(fixedTimeStep, 10, maxSubSteps);
      print("Sphere z position: ${sphereBody.position.z}");
      await Future.delayed(Duration(milliseconds: 16));
    }
    stopwatch.stop();
    stopwatch.reset();
  });
}
