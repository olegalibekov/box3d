import '../equations/Equation.dart';
import "../solver/Solver.dart" show Solver;
import "../world/World.dart" show World;

/**
 * Constraint equation Gauss-Seidel solver.
 * @class GSSolver
 * @constructor
 * @todo The spook parameters should be specified for each constraint, not globally.
 * @author schteppe / https://github.com/schteppe
 * @see https://www8.cs.umu.se/kurser/5DV058/VT09/lectures/spooknotes.pdf
 * @extends Solver
 */
class GSSolver extends Solver {
  num iterations;

  num tolerance;

  GSSolver() : super() {
    /* super call moved to initializer */;
    this.iterations = 10;
    this.tolerance = 1e-7;
  }

  /**
   * Solve
   * @method solve
   *
   *
   *
   */
  num solve(num dt, World world) {
    var iter = 0;
    final maxIter = this.iterations;
    final tolSquared = this.tolerance * this.tolerance;
    final equations = this.equations;
    final Neq = equations.length;
    final bodies = world.bodies;
    final Nbodies = bodies.length;
    final h = dt;
    var q;
    var B;
    var invC;
    var deltalambda;
    var deltalambdaTot;
    var GWlambda;
    var lambdaj;
    // Update solve mass
    if (!identical(Neq, 0)) {
      for (var i = 0; !identical(i, Nbodies); i++) {
        bodies[i].updateSolveMassProperties();
      }
    }
    // Things that does not change during iteration can be computed once
    final invCs = GSSolver_solve_invCs;
    final Bs = GSSolver_solve_Bs;
    final lambda = GSSolver_solve_lambda;
    invCs.length = Neq;
    Bs.length = Neq;
    lambda.length = Neq;
    for (var i = 0; !identical(i, Neq); i++) {
      final c = equations[i];
      lambda[i] = 0.0;
      Bs[i] = c.computeB(h);
      invCs[i] = 1.0 / c.computeC();
    }
    if (!identical(Neq, 0)) {
      // Reset vlambda
      for (var i = 0; !identical(i, Nbodies); i++) {
        final b = bodies[i];
        final vlambda = b.vlambda;
        final wlambda = b.wlambda;
        vlambda.set(0, 0, 0);
        wlambda.set(0, 0, 0);
      }
      // Iterate over equations
      for (iter = 0; !identical(iter, maxIter); iter++) {
        // Accumulate the total error for each iteration.
        deltalambdaTot = 0.0;
        for (var j = 0; !identical(j, Neq); j++) {
          final c = equations[j];
          // Compute iteration
          B = Bs[j];
          invC = invCs[j];
          lambdaj = lambda[j];
          GWlambda = c.computeGWlambda();
          deltalambda = invC * (B - GWlambda - c.eps * lambdaj);
          // Clamp if we are not within the min/max interval
          if (lambdaj + deltalambda < c.minForce) {
            deltalambda = c.minForce - lambdaj;
          } else if (lambdaj + deltalambda > c.maxForce) {
            deltalambda = c.maxForce - lambdaj;
          }
          lambda[j] += deltalambda;
          deltalambdaTot += deltalambda > 0.0 ? deltalambda : -deltalambda;
          c.addToWlambda(deltalambda);
        }
        // If the total error is small enough - stop iterate
        if (deltalambdaTot * deltalambdaTot < tolSquared) {
          break;
        }
      }
      // Add result to velocity
      for (var i = 0; !identical(i, Nbodies); i++) {
        final b = bodies[i];
        final v = b.velocity;
        final w = b.angularVelocity;
        b.vlambda.vmul(b.linearFactor, b.vlambda);
        v.vadd(b.vlambda, v);
        b.wlambda.vmul(b.angularFactor, b.wlambda);
        w.vadd(b.wlambda, w);
      }
      // Set the .multiplier property of each equation
      var l = equations.length;
      final invDt = 1 / h;
      while (l-- != null) {
        equations[l].multiplier = lambda[l] * invDt;
      }
    }
    return iter;
  }
}

final List<num> GSSolver_solve_lambda = [];

final List<num> GSSolver_solve_invCs = [];

final List<num> GSSolver_solve_Bs = [];
