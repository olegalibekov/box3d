import "../equations/Equation.dart" show Equation;
import "../world/World.dart" show World;

/**
 * Constraint equation solver base class.
 * @class Solver
 * @constructor
 * @author schteppe / https://github.com/schteppe
 */
class Solver {
  List<Equation> equations;
  Solver() {
    this.equations = [];
  }
  /**
   * Should be implemented in subclasses!
   * @method solve
   * 
   * 
   * 
   */
  num solve(num dt, World world) {
    return (
        // Should return the number of iterations done!
        0);
  }

  /**
   * Add an equation
   * @method addEquation
   * 
   */
  void addEquation(Equation eq) {
    if (eq.enabled) {
      this.equations.add(eq);
    }
  }

  /**
   * Remove an equation
   * @method removeEquation
   * 
   */
  void removeEquation(Equation eq) {
    final eqs = this.equations;
    final i = eqs.indexOf(eq);
    if (!identical(i, -1)) {
      eqs.removeRange(i, 1);
    }
  }

  /**
   * Add all equations
   * @method removeAllEquations
   */
  void removeAllEquations() {
    this.equations.length = 0;
  }
}
