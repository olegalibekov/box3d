import "../solver/Solver.dart" show Solver;
import "../objects/Body.dart" show Body, BODY_TYPES;
import "../equations/Equation.dart" show Equation;
import "../world/World.dart" show World;


class SplitSolverNode {
  Body body;
  List <SplitSolverNode> children;
  List <Equation> eqs;
  bool visited;

  SplitSolverNode(Body body, List <SplitSolverNode> children,
      List <Equation> eqs,
      bool visited) {
    this.body = body;
    this.children = children;
    this.eqs = eqs;
    this.visited = visited;
  }
}


/**
 * Splits the equations into islands and solves them independently. Can improve performance.
 * @class SplitSolver
 * @constructor
 * @extends Solver
 *
 */
class SplitSolver extends Solver {
  num iterations;

  num tolerance;

  SplitSolver subsolver;

  List <SplitSolverNode> nodes;

  List <SplitSolverNode> nodePool;

  SplitSolver(SplitSolver subsolver) : super () {
    /* super call moved to initializer */ ;
    this.iterations = 10;
    this.tolerance = 1e-7;
    this.subsolver = subsolver;
    this.nodes = [];
    this.nodePool = [];
    // Create needed nodes, reuse if possible
    while (this.nodePool.length < 128) {
      this.nodePool.add(this.createNode());
    }
  }

  SplitSolverNode createNode() {
    return SplitSolverNode(
        null,
        [],
        [],
        false
    );
  }

  /**
   * Solve the subsystems
   * @method solve
   *
   *
   *
   */
  num solve(num dt, World world) {
    final nodes = SplitSolver_solve_nodes;
    final nodePool = this.nodePool;
    final bodies = world.bodies;
    final equations = this.equations;
    final Neq = equations.length;
    final Nbodies = bodies.length;
    final subsolver = this.subsolver;
    // Create needed nodes, reuse if possible
    while (nodePool.length < Nbodies) {
      nodePool.add(this.createNode());
    }
    nodes.length = Nbodies;
    for (var i = 0; i < Nbodies; i ++) {
      nodes [ i ] = nodePool [ i ];
    }
    // Reset node values
    for (var i = 0; !identical(i, Nbodies); i ++) {
      final node = nodes [ i ];
      node.body = bodies [ i ];
      node.children.length = 0;
      node.eqs.length = 0;
      node.visited = false;
    }
    for (var k = 0; !identical(k, Neq); k ++) {
      final eq = equations [ k ];
      final i = bodies.indexOf(eq.bi);
      final j = bodies.indexOf(eq.bj);
      final ni = nodes [ i ];
      final nj = nodes [ j ];
      ni.children.add(nj);
      ni.eqs.add(eq);
      nj.children.add(ni);
      nj.eqs.add(eq);
    }
    dynamic /* SplitSolverNode |  */ child = false;
    var n = 0;
    var eqs = SplitSolver_solve_eqs;
    subsolver.tolerance = this.tolerance;
    subsolver.iterations = this.iterations;
    final dummyWorld = SplitSolver_solve_dummyWorld;
    while ((child = getUnvisitedNode(nodes))) {
      eqs.length = 0;
      dummyWorld.bodies.length = 0;
      bfs(child, visitFunc, dummyWorld.bodies, eqs);
      final Neqs = eqs.length;
      eqs.sort((a, b) => a.id.compareTo(b.id));
      for (var i = 0; !identical(i, Neqs); i ++) {
        subsolver.addEquation(eqs [ i ]);
      }
      final iter = subsolver.solve(dt, dummyWorld as World);
      subsolver.removeAllEquations();
      n ++;
    }
    return n;
  }
}
// Returns the number of subsystems
final List <SplitSolverNode> SplitSolver_solve_nodes = [];

final List <SplitSolverNode> SplitSolver_solve_nodePool = [];

final List <Equation> SplitSolver_solve_eqs = [];

final List <Body> SplitSolver_solve_bds = [];

final dynamic SplitSolver_solve_dummyWorld = { "bodies": []};

final STATIC = BODY_TYPES.STATIC;

dynamic /* SplitSolverNode |  */ getUnvisitedNode(
    List <SplitSolverNode> nodes) {
  final Nnodes = nodes.length;
  for (var i = 0; !identical(i, Nnodes); i ++) {
    final node = nodes [ i ];
    if (!node.visited &&
        !((!(node.body.type as int > 0)) & (STATIC as int > 0))) {
      return node;
    }
  }
  return false;
}


final List <SplitSolverNode> queue = [];

void bfs(SplitSolverNode root,
    void visitFunc(SplitSolverNode node, List <Body> bds, List <Equation> eqs),
    List <Body> bds, List <Equation> eqs) {
  queue.add(root);
  root.visited = true;
  visitFunc(root, bds, eqs);
  while (queue.length > 0) {
    final node = queue.last;
    queue.removeLast();
    // Loop over unvisited child nodes
    dynamic /* SplitSolverNode |  */ child = false;
    while ((child = getUnvisitedNode(node.children))) {
      child.visited = true;
      visitFunc(child, bds, eqs);
      queue.add(child);
    }
  }
}

void visitFunc(SplitSolverNode node, List <Body> bds, List <Equation> eqs) {
  bds.add(node.body);
  final Neqs = node.eqs.length;
  for (var i = 0; !identical(i, Neqs); i ++) {
    final eq = node.eqs [ i ];
    if (!eqs.contains(eq)) {
      eqs.add(eq);
    }
  }
}

num sortById(dynamic a, dynamic b) {
  return b.id - a.id;
}