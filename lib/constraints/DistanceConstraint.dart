import "../constraints/Constraint.dart" show Constraint;
import "../equations/ContactEquation.dart" show ContactEquation;
import "../objects/Body.dart" show Body;

/**
 * Constrains two bodies to be at a constant distance from each others center of mass.
 * @class DistanceConstraint
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 * @extends Constraint
 */
class DistanceConstraint extends Constraint {
  num distance;

  ContactEquation distanceEquation;

  DistanceConstraint(Body bodyA, Body bodyB, [ num distance, maxForce = 1e6 ])
      : super (bodyA, bodyB) {
    /* super call moved to initializer */ ;
    if (identical(distance.toString(), "undefined")) {
      distance = bodyA.position.distanceTo(bodyB.position);
    }
    this.distance = distance;
    final eq = (this.distanceEquation = new ContactEquation (bodyA, bodyB));
    this.equations.add(eq);
    // Make it bidirectional
    eq.minForce = -maxForce;
    eq.maxForce = maxForce;
  }

  void update() {
    final bodyA = this.bodyA;
    final bodyB = this.bodyB;
    final eq = this.distanceEquation;
    final halfDist = this.distance * 0.5;
    final normal = eq.ni;
    bodyB.position.vsub(bodyA.position, normal);
    normal.normalize();
    normal.scale(halfDist, eq.ri);
    normal.scale(-halfDist, eq.rj);
  }
}