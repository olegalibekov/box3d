import "../constraints/PointToPointConstraint.dart" show PointToPointConstraint;
import "../equations/RotationalEquation.dart" show RotationalEquation;
import "../equations/RotationalMotorEquation.dart" show RotationalMotorEquation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

class HingeConstraintOptions {
  num maxForce;
  Vec3 pivotA;
  Vec3 pivotB;
  Vec3 axisA;
  Vec3 axisB;
  bool collideConnected;

  HingeConstraintOptions(num maxForce,
  Vec3 pivotA,
  Vec3 pivotB,
  Vec3 axisA,
  Vec3 axisB,
  bool collideConnected){
    this.maxForce = maxForce;
    this.pivotA = pivotA;
    this.pivotB = pivotB;
    this.axisA = axisA;
    this.axisB = axisB;
    this.collideConnected = collideConnected;
  }
}


/**
 * Hinge constraint. Think of it as a door hinge. It tries to keep the door in the correct place and with the correct orientation.
 * @class HingeConstraint
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 *
 *
 *
 *
 * @extends PointToPointConstraint
 */
class HingeConstraint extends PointToPointConstraint {
  var motorTargetVelocity;
  Vec3 axisA;

  Vec3 axisB;

  RotationalEquation rotationalEquation1;

  RotationalEquation rotationalEquation2;

  RotationalMotorEquation motorEquation;

  HingeConstraint(Body bodyA, Body bodyB, [var options = const {}])
      : super(bodyA,
            pivotA: options?.pivotA?.clone() ?? new Vec3(),
            bodyB: bodyB,
            pivotB: options?.pivotB?.clone() ?? new Vec3(),
            maxForce: options?.maxForce ?? 1e6) {
    final maxForce = options?.maxForce ?? 1e6;
    final pivotA = options?.pivotA?.clone() ?? new Vec3();
    final pivotB = options?.pivotB?.clone() ?? new Vec3();
    /* super call moved to initializer */
    ;
    final axisA = (this.axisA =
        options.axisA ? options.axisA.clone() : new Vec3(1, 0, 0));
    axisA.normalize();
    final axisB = (this.axisB =
        options.axisB ? options.axisB.clone() : new Vec3(1, 0, 0));
    axisB.normalize();
    this.collideConnected = !!options.collideConnected;
    final rotational1 = (this.rotationalEquation1 =
        new RotationalEquation(bodyA, bodyB, options));
    final rotational2 = (this.rotationalEquation2 =
        new RotationalEquation(bodyA, bodyB, options));
    final motor = (this.motorEquation =
        new RotationalMotorEquation(bodyA, bodyB, maxForce));
    motor.enabled = false;
    // Equations to be fed to the solver
    this.equations.add(rotational1);
    this.equations.add(rotational2);
    this.equations.add(motor);
  }

  /**
   * @method enableMotor
   */
  void enableMotor() {
    this.motorEquation.enabled = true;
  }

  /**
   * @method disableMotor
   */
  void disableMotor() {
    this.motorEquation.enabled = false;
  }

  /**
   * @method setMotorSpeed
   *
   */
  void setMotorSpeed(num speed) {
    this.motorEquation.targetVelocity = speed;
  }

  /**
   * @method setMotorMaxForce
   *
   */
  void setMotorMaxForce(num maxForce) {
    this.motorEquation.maxForce = maxForce;
    this.motorEquation.minForce = -maxForce;
  }

  void update() {
    final bodyA = this.bodyA;
    final bodyB = this.bodyB;
    final motor = this.motorEquation;
    final r1 = this.rotationalEquation1;
    final r2 = this.rotationalEquation2;
    final worldAxisA = HingeConstraint_update_tmpVec1;
    final worldAxisB = HingeConstraint_update_tmpVec2;
    final axisA = this.axisA;
    final axisB = this.axisB;
    super.update();
    // Get world axes
    bodyA.quaternion.vmult(axisA, worldAxisA);
    bodyB.quaternion.vmult(axisB, worldAxisB);
    worldAxisA.tangents(r1.axisA, r2.axisA);
    r1.axisB.copy(worldAxisB);
    r2.axisB.copy(worldAxisB);
    if (this.motorEquation.enabled) {
      bodyA.quaternion.vmult(this.axisA, motor.axisA);
      bodyB.quaternion.vmult(this.axisB, motor.axisB);
    }
  }
}

final HingeConstraint_update_tmpVec1 = new Vec3();

final HingeConstraint_update_tmpVec2 = new Vec3();
