import "../constraints/Constraint.dart" show Constraint;
import "../equations/ContactEquation.dart" show ContactEquation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

/**
 * Connects two bodies at given offset points.
 * @class PointToPointConstraint
 * @extends Constraint
 * @constructor
 *
 *
 *
 *
 *
 *
 * @example
 *     const bodyA = new Body({ mass: 1 });
 *     const bodyB = new Body({ mass: 1 });
 *     bodyA.position.set(-1, 0, 0);
 *     bodyB.position.set(1, 0, 0);
 *     bodyA.addShape(shapeA);
 *     bodyB.addShape(shapeB);
 *     world.addBody(bodyA);
 *     world.addBody(bodyB);
 *     const localPivotA = new Vec3(1, 0, 0);
 *     const localPivotB = new Vec3(-1, 0, 0);
 *     const constraint = new PointToPointConstraint(bodyA, localPivotA, bodyB, localPivotB);
 *     world.addConstraint(constraint);
 */
class PointToPointConstraint extends Constraint {
  Vec3 pivotA;
  Vec3 pivotB;
  ContactEquation equationX;
  ContactEquation equationY;
  ContactEquation equationZ;

  PointToPointConstraint(Body bodyA,
  {pivotA, Body bodyB, pivotB, maxForce = 1e6})
      : super(bodyA, bodyB) {
    if (pivotA == null) {
      pivotA = Vec3();
    }
    if (pivotB == null) {
      pivotB = Vec3();
    }
    /* super call moved to initializer */;
    this.pivotA = pivotA.clone();
    this.pivotB = pivotB.clone();
    final x = (this.equationX = new ContactEquation(bodyA, bodyB));
    final y = (this.equationY = new ContactEquation(bodyA, bodyB));
    final z = (this.equationZ = new ContactEquation(bodyA, bodyB));
    // Equations to be fed to the solver
    this.equations.add(x);
    this.equations.add(y);
    this.equations.add(z);
    // Make the equations bidirectional
    x.minForce = y.minForce = z.minForce = -maxForce;
    x.maxForce = y.maxForce = z.maxForce = maxForce;
    x.ni.set(1, 0, 0);
    y.ni.set(0, 1, 0);
    z.ni.set(0, 0, 1);
  }

  void update() {
    final bodyA = this.bodyA;
    final bodyB = this.bodyB;
    final x = this.equationX;
    final y = this.equationY;
    final z = this.equationZ;
    // Rotate the pivots to world space
    bodyA.quaternion.vmult(this.pivotA, x.ri);
    bodyB.quaternion.vmult(this.pivotB, x.rj);
    y.ri.copy(x.ri);
    y.rj.copy(x.rj);
    z.ri.copy(x.ri);
    z.rj.copy(x.rj);
  }
}
