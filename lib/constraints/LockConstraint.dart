import "../constraints/PointToPointConstraint.dart" show PointToPointConstraint;
import "../equations/RotationalEquation.dart" show RotationalEquation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import "../equations/RotationalMotorEquation.dart" show RotationalMotorEquation;

/**
 * Lock constraint. Will remove all degrees of freedom between the bodies.
 * @class LockConstraint
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 * @extends PointToPointConstraint
 */
class LockConstraint extends PointToPointConstraint {
  Vec3 xA;

  Vec3 xB;

  Vec3 yA;

  Vec3 yB;

  Vec3 zA;

  Vec3 zB;

  RotationalEquation rotationalEquation1;

  RotationalEquation rotationalEquation2;

  RotationalEquation rotationalEquation3;

  RotationalMotorEquation motorEquation;

  LockConstraint(Body bodyA, Body bodyB,
      [ var options = const {} ])
      : super (bodyA,  pivotA: Vec3(), bodyB: bodyB, pivotB: Vec3(), maxForce: options?.maxForce ?? 1e6) {
    final maxForce = options?.maxForce ?? 1e6;
    // Set pivot point in between
    final pivotA = new Vec3 ();
    final pivotB = new Vec3 ();
    final halfWay = new Vec3 ();
    bodyA.position.vadd(bodyB.position, halfWay);
    halfWay.scale(0.5, halfWay);
    bodyB.pointToLocalFrame(halfWay, pivotB);
    bodyA.pointToLocalFrame(halfWay, pivotA);
    // The point-to-point constraint will keep a point shared between the bodies
    /* super call moved to initializer */;
    // Store initial rotation of the bodies as unit vectors in the local body spaces
    this.xA = bodyA.vectorToLocalFrame(Vec3.UNIT_X);
    this.xB = bodyB.vectorToLocalFrame(Vec3.UNIT_X);
    this.yA = bodyA.vectorToLocalFrame(Vec3.UNIT_Y);
    this.yB = bodyB.vectorToLocalFrame(Vec3.UNIT_Y);
    this.zA = bodyA.vectorToLocalFrame(Vec3.UNIT_Z);
    this.zB = bodyB.vectorToLocalFrame(Vec3.UNIT_Z);
    // ...and the following rotational equations will keep all rotational DOF's in place
    final r1 = (this.rotationalEquation1 =
    new RotationalEquation (bodyA, bodyB, options));
    final r2 = (this.rotationalEquation2 =
    new RotationalEquation (bodyA, bodyB, options));
    final r3 = (this.rotationalEquation3 =
    new RotationalEquation (bodyA, bodyB, options));
    this.equations.add(r1);
    this.equations.add(r2);
    this.equations.add(r3);
  }

  void update() {
    final bodyA = this.bodyA;
    final bodyB = this.bodyB;
    final motor = this.motorEquation;
    final r1 = this.rotationalEquation1;
    final r2 = this.rotationalEquation2;
    final r3 = this.rotationalEquation3;
    final worldAxisA = LockConstraint_update_tmpVec1;
    final worldAxisB = LockConstraint_update_tmpVec2;
    super.update();
    // These vector pairs must be orthogonal
    bodyA.vectorToWorldFrame(this.xA, r1.axisA);
    bodyB.vectorToWorldFrame(this.yB, r1.axisB);
    bodyA.vectorToWorldFrame(this.yA, r2.axisA);
    bodyB.vectorToWorldFrame(this.zB, r2.axisB);
    bodyA.vectorToWorldFrame(this.zA, r3.axisA);
    bodyB.vectorToWorldFrame(this.xB, r3.axisB);
  }
}

final LockConstraint_update_tmpVec1 = new Vec3 ();

final LockConstraint_update_tmpVec2 = new Vec3 ();