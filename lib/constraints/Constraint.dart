import "../equations/Equation.dart" show Equation;
import "../objects/Body.dart" show Body;
import "../utils/Utils.dart" show Utils;

class ConstraintOptions {}

/// Constraint base class
/// @class Constraint
/// @author schteppe
/// @constructor
class Constraint {
  List<Equation> equations;

  Body bodyA;

  Body bodyB;

  num id;

  bool collideConnected;

  static num idCounter = 0;

  Constraint(Body bodyA, Body bodyB, [options = const {}]) {
    options = Utils.defaults(
        options, {"collideConnected": true, "wakeUpBodies": true});
    this.equations = [];
    this.bodyA = bodyA;
    this.bodyB = bodyB;
    this.id = Constraint.idCounter++;
    this.collideConnected = options['collideConnected'];
    if (options.wakeUpBodies) {
      if (bodyA != null) {
        bodyA.wakeUp();
      }
      if (bodyB != null) {
        bodyB.wakeUp();
      }
    }
  }

  /**
   * Update all the equations with data.
   * @method update
   */
  void update() {
    throw ("method update() not implmemented in this Constraint subclass!");
  }

  /**
   * Enables all equations in the constraint.
   * @method enable
   */
  void enable() {
    final eqs = this.equations;
    for (var i = 0; i < eqs.length; i++) {
      eqs[i].enabled = true;
    }
  }

  /**
   * Disables all equations in the constraint.
   * @method disable
   */
  void disable() {
    final eqs = this.equations;
    for (var i = 0; i < eqs.length; i++) {
      eqs[i].enabled = false;
    }
  }
}
