import "../constraints/PointToPointConstraint.dart" show PointToPointConstraint;
import "../equations/ConeEquation.dart" show ConeEquation;
import "../equations/RotationalEquation.dart" show RotationalEquation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

class ConeTwistConstraintOptions {
  num maxForce;
  Vec3 pivotA;
  Vec3 pivotB;
  Vec3 axisA;
  Vec3 axisB;
  bool collideConnected;
  num angle;
  num twistAngle;
}

/**
 * @class ConeTwistConstraint
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 *
 *
 *
 *
 * @extends PointToPointConstraint
 */
class ConeTwistConstraint extends PointToPointConstraint {
  Vec3 axisA;

  Vec3 axisB;

  num angle;

  ConeEquation coneEquation;

  RotationalEquation twistEquation;

  num twistAngle;

  factory ConeTwistConstraint(
      Body bodyA, Body bodyB, ConeTwistConstraintOptions options) {
    options ??= ConeTwistConstraintOptions();
    final maxForce = options.maxForce != 'undefined' ? options.maxForce : 1e6;
    // Set pivot point in between
    final pivotA = options.pivotA != null ? options.pivotA.clone() : new Vec3();
    final pivotB = options.pivotB != null ? options.pivotB.clone() : new Vec3();

    return ConeTwistConstraint._(
        bodyA, pivotA, bodyB, pivotB, maxForce, options);
  }

  ConeTwistConstraint._(bodyA, pivotA, bodyB, pivotB, maxForce, options)
      : super(bodyA,
            pivotA: pivotA, bodyB: bodyB, pivotB: pivotB, maxForce: maxForce) {
    this.axisA = options.axisA ? options.axisA.clone() : new Vec3();
    this.axisB = options.axisB ? options.axisB.clone() : new Vec3();

    this.collideConnected = !!options.collideConnected;

    this.angle = options.angle != 'undefined' ? options.angle : 0;

    final c = (this.coneEquation = new ConeEquation(bodyA, bodyB, options));

    final t =
        (this.twistEquation = new RotationalEquation(bodyA, bodyB, options));
    this.twistAngle =
        options.twistAngle != 'undefined' ? options.twistAngle : 0;

    // Make the cone equation push the bodies toward the cone axis, not outward
    c.maxForce = 0;
    c.minForce = -maxForce;

    // Make the twist equation add torque toward the initial position
    t.maxForce = 0;
    t.minForce = -maxForce;

    this.equations.add(c);
    this.equations.add(t);
  }

  // factory ConeTwistConstraint(Body bodyA, Body bodyB,
  //     ConeTwistConstraintOptions options) {
  //   options ??= {};
  //   final maxForce = !identical(, "undefined") ? options.maxForce : 1e6;
  //   // Set pivot point in between
  //   final pivotA = options.pivotA ? options.pivotA.clone() : new Vec3 ();
  //   final pivotB = options.pivotB
  //       ? options.pivotB.clone()
  //       : new Vec3 (); /* super call moved to initializer */;
  //   var axisA = options.axisA ? options.axisA.clone() : new Vec3 ();
  //   var axisB = options.axisB ? options.axisB.clone() : new Vec3 ();
  //   var collideConnected = ! !options.collideConnected;
  //
  //   ///Todo check everywhere identical(...toSting(), "undefined"),
  //   ///not identical(..., "undefined")
  //   var angle = !identical(options.angle.toString(), "undefined") ? options
  //       .angle : 0;
  //
  //   return ConeTwistConstraint._(
  //       bodyA,
  //       pivotA,
  //       bodyB,
  //       pivotB,
  //       maxForce,
  //       axisA,
  //       axisB,
  //       collideConnected,
  //       angle,
  //       twistEquation,
  //       twistAngle,
  //       equations);
  // }
  //
  // ConeTwistConstraint._(bodyA, pivotA, bodyB, pivotB, maxForce, this.axisA,
  //     this.axisB, this.collideConnected, this.angle, this.twistEquation,
  //     this.twistAngle, this.equations)
  //     : super (bodyA, pivotA, bodyB, pivotB, maxForce) {
  //   final c = (this.coneEquation = new ConeEquation (bodyA, bodyB, options));
  //   final t = (
  //       this.twistEquation = new RotationalEquation (bodyA, bodyB, options));
  //   var twistAngle = !identical(options.twistAngle.toString(), "undefined")
  //       ? options
  //       .twistAngle
  //       : 0;
  //   // Make the cone equation push the bodies toward the cone axis, not outward
  //   c.maxForce = 0;
  //   c.minForce = -maxForce;
  //   // Make the twist equation add torque toward the initial position
  //   t.maxForce = 0;
  //   t.minForce = -maxForce;
  //   this.equations.add(c);
  //   this.equations.add(t);
  // }

  void update() {
    final bodyA = this.bodyA;
    final bodyB = this.bodyB;
    final cone = this.coneEquation;
    final twist = this.twistEquation;
    super.update();
    // Update the axes to the cone constraint
    bodyA.vectorToWorldFrame(this.axisA, cone.axisA);
    bodyB.vectorToWorldFrame(this.axisB, cone.axisB);
    // Update the world axes in the twist constraint
    this.axisA.tangents(twist.axisA, twist.axisA);
    bodyA.vectorToWorldFrame(twist.axisA, twist.axisA);
    this.axisB.tangents(twist.axisB, twist.axisB);
    bodyB.vectorToWorldFrame(twist.axisB, twist.axisB);
    cone.angle = this.angle;
    twist.maxAngle = this.twistAngle;
  }
}

final ConeTwistConstraint_update_tmpVec1 = new Vec3();

final ConeTwistConstraint_update_tmpVec2 = new Vec3();
