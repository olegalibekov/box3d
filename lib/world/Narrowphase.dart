import 'dart:math' as Math;

import "../collision/AABB.dart" show AABB;
import "../collision/Ray.dart" show Ray;
import "../equations/ContactEquation.dart" show ContactEquation;
import "../equations/FrictionEquation.dart" show FrictionEquation;
import "../material/ContactMaterial.dart" show ContactMaterial;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import "../shapes/Box.dart" show Box;
import "../shapes/ConvexPolyhedron.dart"
    show ConvexPolyhedron, ConvexPolyhedronContactPoint;
import "../shapes/Heightfield.dart" show Heightfield;
import "../shapes/Particle.dart" show Particle;
import "../shapes/Plane.dart" show Plane;
import "../shapes/Shape.dart" show Shape, SHAPE_TYPES;
import "../shapes/Sphere.dart" show Sphere;
import "../shapes/Trimesh.dart" show Trimesh;
import "../utils/Vec3Pool.dart" show Vec3Pool;
import "../world/World.dart" show World;

enum COLLISION_TYPES {
  sphereSphere,
  spherePlane,
  boxBox,
  sphereBox,
  planeBox,
  convexConvex,
  sphereConvex,
  planeConvex,
  boxConvex,
  sphereHeightfield,
  boxHeightfield,
  convexHeightfield,
  sphereParticle,
  planeParticle,
  boxParticle,
  convexParticle,
  sphereTrimesh,
  planeTrimesh
}

typedef Resolver(Shape si, Shape sj, Vec3 xi, Vec3 xj, Quaternion qi,
    Quaternion qj, Body bi, Body bj,
    [Shape rsi, Shape rsj, bool justTest]);
//Box si,
//       Heightfield sj,
//       Vec3 xi,
//       Vec3 xj,
//       Quaternion qi,
//       Quaternion qj,
//       Body bi,
//       Body bj,
//       Shape rsi,
//       rsj,
//       bool justTest

/// Helper class for the World. Generates ContactEquations.
/// @class Narrowphase
/// @constructor
/// @todo Sphere-ConvexPolyhedron contacts
/// @todo Contact reduction
/// @todo should move methods to prototype
class Narrowphase {
  get resolvers {
    Map<COLLISION_TYPES, Function> resolvers = {
      COLLISION_TYPES.sphereSphere: sphereSphere,
      COLLISION_TYPES.spherePlane: spherePlane,
      COLLISION_TYPES.boxBox: boxBox,
      COLLISION_TYPES.sphereBox: sphereBox,
      COLLISION_TYPES.planeBox: planeBox,
      COLLISION_TYPES.convexConvex: convexConvex,
      COLLISION_TYPES.sphereConvex: sphereConvex,
      COLLISION_TYPES.planeConvex: planeConvex,
      COLLISION_TYPES.boxConvex: boxConvex,
      COLLISION_TYPES.sphereHeightfield: sphereHeightfield,
      COLLISION_TYPES.boxHeightfield: boxHeightfield,
      COLLISION_TYPES.convexHeightfield: convexHeightfield,
      COLLISION_TYPES.sphereParticle: sphereParticle,
      COLLISION_TYPES.planeParticle: planeParticle,
      COLLISION_TYPES.boxParticle: boxParticle,
      COLLISION_TYPES.convexParticle: convexParticle,
      COLLISION_TYPES.sphereTrimesh: sphereTrimesh,
      COLLISION_TYPES.planeTrimesh: planeTrimesh
    };
    return resolvers;
  }

  resolverIndex(SHAPE_TYPES shapeType1, SHAPE_TYPES shapeType2) {
    checkPair(SHAPE_TYPES shapeType1, SHAPE_TYPES shapeType2) {
      if (shapeType1 == SHAPE_TYPES.SPHERE &&
          shapeType2 == SHAPE_TYPES.SPHERE) {
        return COLLISION_TYPES.sphereSphere;
      }
      if (shapeType1 == SHAPE_TYPES.SPHERE && shapeType2 == SHAPE_TYPES.PLANE) {
        return COLLISION_TYPES.spherePlane;
      }
      if (shapeType1 == SHAPE_TYPES.BOX && shapeType2 == SHAPE_TYPES.BOX) {
        return COLLISION_TYPES.boxBox;
      }
      if (shapeType1 == SHAPE_TYPES.SPHERE && shapeType2 == SHAPE_TYPES.BOX) {
        return COLLISION_TYPES.sphereBox;
      }
      if (shapeType1 == SHAPE_TYPES.PLANE && shapeType2 == SHAPE_TYPES.BOX) {
        return COLLISION_TYPES.planeBox;
      }
      if (shapeType1 == SHAPE_TYPES.CONVEXPOLYHEDRON &&
          shapeType2 == SHAPE_TYPES.CONVEXPOLYHEDRON) {
        return COLLISION_TYPES.convexConvex;
      }
      if (shapeType1 == SHAPE_TYPES.SPHERE &&
          shapeType2 == SHAPE_TYPES.CONVEXPOLYHEDRON) {
        return COLLISION_TYPES.sphereConvex;
      }
      if (shapeType1 == SHAPE_TYPES.PLANE &&
          shapeType2 == SHAPE_TYPES.CONVEXPOLYHEDRON) {
        return COLLISION_TYPES.planeConvex;
      }
      if (shapeType1 == SHAPE_TYPES.BOX &&
          shapeType2 == SHAPE_TYPES.CONVEXPOLYHEDRON) {
        return COLLISION_TYPES.boxConvex;
      }
      if (shapeType1 == SHAPE_TYPES.SPHERE &&
          shapeType2 == SHAPE_TYPES.HEIGHTFIELD) {
        return COLLISION_TYPES.sphereHeightfield;
      }
      if (shapeType1 == SHAPE_TYPES.BOX &&
          shapeType2 == SHAPE_TYPES.HEIGHTFIELD) {
        return COLLISION_TYPES.boxHeightfield;
      }
      if (shapeType1 == SHAPE_TYPES.CONVEXPOLYHEDRON &&
          shapeType2 == SHAPE_TYPES.HEIGHTFIELD) {
        return COLLISION_TYPES.convexHeightfield;
      }
      if (shapeType1 == SHAPE_TYPES.SPHERE &&
          shapeType2 == SHAPE_TYPES.PARTICLE) {
        return COLLISION_TYPES.sphereParticle;
      }
      if (shapeType1 == SHAPE_TYPES.PLANE &&
          shapeType2 == SHAPE_TYPES.PARTICLE) {
        return COLLISION_TYPES.planeParticle;
      }
      if (shapeType1 == SHAPE_TYPES.BOX && shapeType2 == SHAPE_TYPES.PARTICLE) {
        return COLLISION_TYPES.boxParticle;
      }
      if (shapeType1 == SHAPE_TYPES.CONVEXPOLYHEDRON &&
          shapeType2 == SHAPE_TYPES.PARTICLE) {
        return COLLISION_TYPES.convexParticle;
      }
      if (shapeType1 == SHAPE_TYPES.SPHERE &&
          shapeType2 == SHAPE_TYPES.TRIMESH) {
        return COLLISION_TYPES.sphereTrimesh;
      }
      if (shapeType1 == SHAPE_TYPES.PLANE &&
          shapeType2 == SHAPE_TYPES.TRIMESH) {
        return COLLISION_TYPES.planeTrimesh;
      }
      return null;
    }

    var pairOne = checkPair(shapeType1, shapeType2);
    var pairTwo = checkPair(shapeType1, shapeType2);
    return [pairOne ?? pairTwo, pairOne != null];
  }

  List<ContactEquation> contactPointPool;

  List<FrictionEquation> frictionEquationPool;

  List<ContactEquation> result;

  List<FrictionEquation> frictionResult;

  Vec3Pool v3pool;

  World world;

  ContactMaterial currentContactMaterial;

  bool enableFrictionReduction;

  Narrowphase(World world) {
    this.contactPointPool = [];
    this.frictionEquationPool = [];
    this.result = [];
    this.frictionResult = [];
    this.v3pool = new Vec3Pool();
    this.world = world;
    this.currentContactMaterial = world.defaultContactMaterial;
    this.enableFrictionReduction = false;
  }

  /// Make a contact object, by using the internal pool or creating a new one.
  /// @method createContactEquation
  ContactEquation createContactEquation(Body bi, Body bj, Shape si, Shape sj,
      [dynamic /* Shape | null */ overrideShapeA,
      dynamic /* Shape | null */ overrideShapeB]) {
    var c;
    if (this.contactPointPool?.length != null) {
      c = this.contactPointPool.removeLast();
      c.bi = bi;
      c.bj = bj;
    } else {
      c = new ContactEquation(bi, bj);
    }
    c.enabled = bi.collisionResponse &&
        bj.collisionResponse &&
        si.collisionResponse &&
        sj.collisionResponse;
    final cm = this.currentContactMaterial;
    c.restitution = cm.restitution;
    c.setSpookParams(cm.contactEquationStiffness, cm.contactEquationRelaxation,
        this.world.dt);
    final matA = si.material ?? bi.material;
    final matB = sj.material ?? bj.material;
    if (matA && matB && matA.restitution >= 0 && matB.restitution >= 0) {
      c.restitution = matA.restitution * matB.restitution;
    }
    c.si = overrideShapeA ?? si;
    c.sj = overrideShapeB ?? sj;
    return c;
  }

  bool createFrictionEquationsFromContact(
      ContactEquation contactEquation, List<FrictionEquation> outArray) {
    final bodyA = contactEquation.bi;
    final bodyB = contactEquation.bj;
    final shapeA = contactEquation.si;
    final shapeB = contactEquation.sj;
    final world = this.world;
    final cm = this.currentContactMaterial;
    // If friction or restitution were specified in the material, use them
    var friction = cm.friction;
    final matA = shapeA.material ?? bodyA.material;
    final matB = shapeB.material ?? bodyB.material;
    if (matA && matB && matA.friction >= 0 && matB.friction >= 0) {
      friction = matA.friction * matB.friction;
    }
    if (friction > 0) {
      // Create 2 tangent equations
      final mug = friction * world.gravity.length();
      var reducedMass = bodyA.invMass + bodyB.invMass;
      if (reducedMass > 0) {
        reducedMass = 1 / reducedMass;
      }
      final pool = this.frictionEquationPool;
      final c1 = pool.length != null
          ? pool.removeLast()
          : new FrictionEquation(bodyA, bodyB, mug * reducedMass);
      final c2 = pool.length != null
          ? pool.removeLast()
          : new FrictionEquation(bodyA, bodyB, mug * reducedMass);
      c1.bi = c2.bi = bodyA;
      c1.bj = c2.bj = bodyB;
      c1.minForce = c2.minForce = -mug * reducedMass;
      c1.maxForce = c2.maxForce = mug * reducedMass;
      // Copy over the relative vectors
      c1.ri.copy(contactEquation.ri);
      c1.rj.copy(contactEquation.rj);
      c2.ri.copy(contactEquation.ri);
      c2.rj.copy(contactEquation.rj);
      // Construct tangents
      contactEquation.ni.tangents(c1.t, c2.t);
      // Set spook params
      c1.setSpookParams(cm.frictionEquationStiffness,
          cm.frictionEquationRelaxation, world.dt);
      c2.setSpookParams(cm.frictionEquationStiffness,
          cm.frictionEquationRelaxation, world.dt);
      c1.enabled = c2.enabled = contactEquation.enabled;
      outArray.addAll([c1, c2]);
      return true;
    }
    return false;
  }

  // Take the average N latest contact point on the plane.
  void createFrictionFromAverage(num numContacts) {
    // The last contactEquation
    var c = this.result[this.result.length - 1];
    // Create the result: two "average" friction equations
    if (!this.createFrictionEquationsFromContact(c, this.frictionResult) ||
        identical(numContacts, 1)) {
      return;
    }
    final f1 = this.frictionResult[this.frictionResult.length - 2];
    final f2 = this.frictionResult[this.frictionResult.length - 1];
    averageNormal.setZero();
    averageContactPointA.setZero();
    averageContactPointB.setZero();
    final bodyA = c.bi;
    final bodyB = c.bj;
    for (var i = 0; !identical(i, numContacts); i++) {
      c = this.result[this.result.length - 1 - i];
      if (!identical(c.bi, bodyA)) {
        averageNormal.vadd(c.ni, averageNormal);
        averageContactPointA.vadd(c.ri, averageContactPointA);
        averageContactPointB.vadd(c.rj, averageContactPointB);
      } else {
        averageNormal.vsub(c.ni, averageNormal);
        averageContactPointA.vadd(c.rj, averageContactPointA);
        averageContactPointB.vadd(c.ri, averageContactPointB);
      }
    }
    final invNumContacts = 1 / numContacts;
    averageContactPointA.scale(invNumContacts, f1.ri);
    averageContactPointB.scale(invNumContacts, f1.rj);
    f2.ri.copy(f1.ri);
    f2.rj.copy(f1.rj);
    averageNormal.normalize();
    averageNormal.tangents(f1.t, f2.t);
  }

  /// Generate all contacts between a list of body pairs
  /// @method getContacts
  void getContacts(
      List<Body> p1,
      List<Body> p2,
      World world,
      List<ContactEquation> result,
      List<ContactEquation> oldcontacts,
      List<FrictionEquation> frictionResult,
      List<FrictionEquation> frictionPool) {
    // Save old contact objects
    this.contactPointPool = oldcontacts;
    this.frictionEquationPool = frictionPool;
    this.result = result;
    this.frictionResult = frictionResult;
    final qi = tmpQuat1;
    final qj = tmpQuat2;
    final xi = tmpVec1;
    final xj = tmpVec2;
    for (var k = 0, N = p1.length; !identical(k, N); k++) {
      // Get current collision bodies
      final bi = p1[k];
      final bj = p2[k];
      // Get contact material
      var bodyContactMaterial = null;
      if (bi.material != null && bj.material != null) {
        bodyContactMaterial =
            world.getContactMaterial(bi.material, bj.material);
      }

      ///TODO wtf???
      ///((bi.type) & (Body.KINEMATIC as int) &&
      //           (bj.type as int) & (Body.STATIC as int)) ||
      //           ((bi.type as int) & (Body.STATIC as int) &&
      //               (bj.type as int) & (Body.KINEMATIC as int)) ||
      //           ((bi.type as int) & (Body.KINEMATIC as int) &&
      //               (bj.type as int) & (Body.KINEMATIC as int))
      /// [justTest]
      final justTest = true;
      for (var i = 0; i < bi.shapes.length; i++) {
        bi.quaternion.mult(bi.shapeOrientations[i], qi);
        bi.quaternion.vmult(bi.shapeOffsets[i], xi);
        xi.vadd(bi.position, xi);
        final si = bi.shapes[i];
        for (var j = 0; j < bj.shapes.length; j++) {
          // Compute world transform of shapes
          bj.quaternion.mult(bj.shapeOrientations[j], qj);
          bj.quaternion.vmult(bj.shapeOffsets[j], xj);
          xj.vadd(bj.position, xj);
          final sj = bj.shapes[j];

          ///TODO wtf????
          if (!(((si.collisionFilterMask.toInt()) &
                      (sj.collisionFilterGroup.toInt())) !=
                  0 &&
              ((sj.collisionFilterMask.toInt()) &
                      (si.collisionFilterGroup.toInt())) !=
                  0)) {
            continue;
          }
          if (xi.distanceTo(xj) >
              si.boundingSphereRadius + sj.boundingSphereRadius) {
            continue;
          }
          // Get collision material
          var shapeContactMaterial = null;
          if (si.material != null && sj.material != null) {
            shapeContactMaterial =
                world.getContactMaterial(si.material, sj.material);
          }
          this.currentContactMaterial = shapeContactMaterial ??
              bodyContactMaterial ??
              world.defaultContactMaterial;
          // Get contacts
          List answer = resolverIndex(si.type, sj.type);
          final collisionType = answer[0];
          final dynamic resolver = this.resolvers[collisionType];
          if (resolver != null) {
            var retval = false;
            // TO DO: investigate why sphereParticle and convexParticle

            // resolvers expect si and sj shapes to be in reverse order

            // (i.e. larger integer value type first instead of smaller first)
            if (answer[1]) {
              retval =
                  resolver(si, sj, xi, xj, qi, qj, bi, bj, si, sj, justTest);
            } else {
              retval =
                  resolver(sj, si, xj, xi, qj, qi, bj, bi, si, sj, justTest);
            }
            if (retval && justTest) {
              // Register overlap
              world.shapeOverlapKeeper.set(si.id, sj.id);
              world.bodyOverlapKeeper.set(bi.id, bj.id);
            }
          }
        }
      }
    }
  }

  dynamic sphereSphere(Sphere si, Sphere sj, Vec3 xi, Vec3 xj, Quaternion qi,
      Quaternion qj, Body bi, Body bj,
      [Shape rsi, Shape rsj, bool justTest]) {
    if (justTest) {
      return xi.distanceSquared(xj) < Math.pow((si.radius + sj.radius), 2);
    }
    // We will have only one contact in this case
    final contactEq = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
    // Contact normal
    xj.vsub(xi, contactEq.ni);
    contactEq.ni.normalize();
    // Contact point locations
    contactEq.ri.copy(contactEq.ni);
    contactEq.rj.copy(contactEq.ni);
    contactEq.ri.scale(si.radius, contactEq.ri);
    contactEq.rj.scale(-sj.radius, contactEq.rj);
    contactEq.ri.vadd(xi, contactEq.ri);
    contactEq.ri.vsub(bi.position, contactEq.ri);
    contactEq.rj.vadd(xj, contactEq.rj);
    contactEq.rj.vsub(bj.position, contactEq.rj);
    this.result.add(contactEq);
    this.createFrictionEquationsFromContact(contactEq, this.frictionResult);
  }

  spherePlane(Sphere si, Plane sj, Vec3 xi, Vec3 xj, Quaternion qi,
      Quaternion qj, Body bi, Body bj,
      [Shape rsi, Shape rsj, bool justTest]) {
    // We will have one contact in this case
    final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);

    // Contact normal
    r.ni.set(0, 0, 1);
    qj.vmult(r.ni, r.ni);
    r.ni.negate(r.ni); // body i is the sphere, flip normal
    r.ni.normalize(); // Needed?

    // Vector from sphere center to contact point
    r.ni.scale(si.radius, r.ri);

    // Project down sphere on plane
    xi.vsub(xj, point_on_plane_to_sphere);
    r.ni.scale(r.ni.dot(point_on_plane_to_sphere), plane_to_sphere_ortho);
    point_on_plane_to_sphere.vsub(
        plane_to_sphere_ortho, r.rj); // The sphere position projected to plane

    if (-point_on_plane_to_sphere.dot(r.ni) <= si.radius) {
      if (justTest) {
        return true;
      }

      // Make it relative to the body
      final ri = r.ri;
      final rj = r.rj;
      ri.vadd(xi, ri);
      ri.vsub(bi.position, ri);
      rj.vadd(xj, rj);
      rj.vsub(bj.position, rj);

      this.result.add(r);
      this.createFrictionEquationsFromContact(r, this.frictionResult);
    }
  }

  boxBox(Box si, Box sj, Vec3 xi, Vec3 xj, Quaternion qi, Quaternion qj,
      Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    si.convexPolyhedronRepresentation.material = si.material;
    sj.convexPolyhedronRepresentation.material = sj.material;
    si.convexPolyhedronRepresentation.collisionResponse = si.collisionResponse;
    sj.convexPolyhedronRepresentation.collisionResponse = sj.collisionResponse;
    return this.convexConvex(
        si.convexPolyhedronRepresentation,
        sj.convexPolyhedronRepresentation,
        xi,
        xj,
        qi,
        qj,
        bi,
        bj,
        si,
        sj,
        justTest);
  }

  convexConvex(
      ConvexPolyhedron si,
      ConvexPolyhedron sj,
      Vec3 xi,
      Vec3 xj,
      Quaternion qi,
      Quaternion qj,
      Body bi,
      Body bj,
      Shape rsi,
      Shape rsj,
      bool justTest,
      [List<num> faceListA,
      List<num> faceListB]) {
    final sepAxis = convexConvex_sepAxis;

    if (xi.distanceTo(xj) > si.boundingSphereRadius + sj.boundingSphereRadius) {
      return;
    }

    if (si.findSeparatingAxis(
        sj, xi, qi, xj, qj, sepAxis, faceListA, faceListB)) {
      final List<ConvexPolyhedronContactPoint> res = [];
      final q = convexConvex_q;
      si.clipAgainstHull(xi, qi, sj, xj, qj, sepAxis, -100, 100, res);
      var numContacts = 0;
      for (var j = 0; j != res.length; j++) {
        if (justTest) {
          return true;
        }
        final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
        final ri = r.ri;
        final rj = r.rj;
        sepAxis.negate(r.ni);
        res[j].normal.negate(q);
        q.scale(res[j].depth, q);
        res[j].point.vadd(q, ri);
        rj.copy(res[j].point);

        // Contact points are in world coordinates. Transform back to relative
        ri.vsub(xi, ri);
        rj.vsub(xj, rj);

        // Make relative to bodies
        ri.vadd(xi, ri);
        ri.vsub(bi.position, ri);
        rj.vadd(xj, rj);
        rj.vsub(bj.position, rj);

        this.result.add(r);
        numContacts++;
        if (!this.enableFrictionReduction) {
          this.createFrictionEquationsFromContact(r, this.frictionResult);
        }
      }
      if (this.enableFrictionReduction && numContacts != null) {
        this.createFrictionFromAverage(numContacts);
      }
    }
  }

  sphereBox(Sphere si, Box sj, Vec3 xi, Vec3 xj, Quaternion qi, Quaternion qj,
      Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    final v3pool = this.v3pool;

    // we refer to the box as body j
    final sides = sphereBox_sides;
    xi.vsub(xj, box_to_sphere);
    sj.getSideNormals(sides, qj);
    final R = si.radius;
    const penetrating_sides = [];

    // Check side (plane) intersections
    bool found = false;

    // Store the resulting side penetration info
    final side_ns = sphereBox_side_ns;
    final side_ns1 = sphereBox_side_ns1;
    final side_ns2 = sphereBox_side_ns2;
    num side_h = null;
    num side_penetrations = 0;
    num side_dot1 = 0;
    num side_dot2 = 0;
    num side_distance = null;
    for (var idx = 0, nsides = sides.length;
        idx != nsides && found == false;
        idx++) {
      // Get the plane side normal (ns)
      final ns = sphereBox_ns;
      ns.copy(sides[idx]);

      final h = ns.length();
      ns.normalize();

      // The normal/distance dot product tells which side of the plane we are
      final dot = box_to_sphere.dot(ns);

      if (dot < h + R && dot > 0) {
        // Intersects plane. Now check the other two dimensions
        final ns1 = sphereBox_ns1;
        final ns2 = sphereBox_ns2;
        ns1.copy(sides[(idx + 1) % 3]);
        ns2.copy(sides[(idx + 2) % 3]);
        final h1 = ns1.length();
        final h2 = ns2.length();
        ns1.normalize();
        ns2.normalize();
        final dot1 = box_to_sphere.dot(ns1);
        final dot2 = box_to_sphere.dot(ns2);
        if (dot1 < h1 && dot1 > -h1 && dot2 < h2 && dot2 > -h2) {
          final dist = (dot - h - R).abs();
          if (side_distance == null || dist < side_distance) {
            side_distance = dist;
            side_dot1 = dot1;
            side_dot2 = dot2;
            side_h = h;
            side_ns.copy(ns);
            side_ns1.copy(ns1);
            side_ns2.copy(ns2);
            side_penetrations++;

            if (justTest) {
              return true;
            }
          }
        }
      }
    }
    if (side_penetrations != null) {
      found = true;
      final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
      side_ns.scale(-R, r.ri); // Sphere r
      r.ni.copy(side_ns);
      r.ni.negate(r.ni); // Normal should be out of sphere
      side_ns.scale(side_h, side_ns);
      side_ns1.scale(side_dot1, side_ns1);
      side_ns.vadd(side_ns1, side_ns);
      side_ns2.scale(side_dot2, side_ns2);
      side_ns.vadd(side_ns2, r.rj);

      // Make relative to bodies
      r.ri.vadd(xi, r.ri);
      r.ri.vsub(bi.position, r.ri);
      r.rj.vadd(xj, r.rj);
      r.rj.vsub(bj.position, r.rj);

      this.result.add(r);
      this.createFrictionEquationsFromContact(r, this.frictionResult);
    }

    // Check corners
    var rj = v3pool.get();
    final sphere_to_corner = sphereBox_sphere_to_corner;
    for (var j = 0; j != 2 && !found; j++) {
      for (var k = 0; k != 2 && !found; k++) {
        for (var l = 0; l != 2 && !found; l++) {
          rj.set(0, 0, 0);
          if (j != null) {
            rj.vadd(sides[0], rj);
          } else {
            rj.vsub(sides[0], rj);
          }
          if (k != null) {
            rj.vadd(sides[1], rj);
          } else {
            rj.vsub(sides[1], rj);
          }
          if (l != null) {
            rj.vadd(sides[2], rj);
          } else {
            rj.vsub(sides[2], rj);
          }

          // World position of corner
          xj.vadd(rj, sphere_to_corner);
          sphere_to_corner.vsub(xi, sphere_to_corner);

          if (sphere_to_corner.lengthSquared() < R * R) {
            if (justTest) {
              return true;
            }
            found = true;
            final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
            r.ri.copy(sphere_to_corner);
            r.ri.normalize();
            r.ni.copy(r.ri);
            r.ri.scale(R, r.ri);
            r.rj.copy(rj);

            // Make relative to bodies
            r.ri.vadd(xi, r.ri);
            r.ri.vsub(bi.position, r.ri);
            r.rj.vadd(xj, r.rj);
            r.rj.vsub(bj.position, r.rj);

            this.result.add(r);
            this.createFrictionEquationsFromContact(r, this.frictionResult);
          }
        }
      }
    }
    v3pool.release(rj);
    rj = null;

    // Check edges
    final edgeTangent = v3pool.get();
    final edgeCenter = v3pool.get();
    final r = v3pool.get(); // r = edge center to sphere center
    final orthogonal = v3pool.get();
    final dist = v3pool.get();
    final Nsides = sides.length;
    for (var j = 0; j != Nsides && !found; j++) {
      for (var k = 0; k != Nsides && !found; k++) {
        if (j % 3 != k % 3) {
          // Get edge tangent
          sides[k].cross(sides[j], edgeTangent);
          edgeTangent.normalize();
          sides[j].vadd(sides[k], edgeCenter);
          r.copy(xi);
          r.vsub(edgeCenter, r);
          r.vsub(xj, r);
          final orthonorm = r.dot(
              edgeTangent); // distance from edge center to sphere center in the tangent direction
          edgeTangent.scale(orthonorm,
              orthogonal); // Vector from edge center to sphere center in the tangent direction

          // Find the third side orthogonal to this one
          var l = 0;
          while (l == j % 3 || l == k % 3) {
            l++;
          }

          // vec from edge center to sphere projected to the plane orthogonal to the edge tangent
          dist.copy(xi);
          dist.vsub(orthogonal, dist);
          dist.vsub(edgeCenter, dist);
          dist.vsub(xj, dist);

          // Distances in tangent direction and distance in the plane orthogonal to it
          final tdist = (orthonorm);
          final ndist = dist.length();

          if (tdist < sides[l].length() && ndist < R) {
            if (justTest) {
              return true;
            }
            found = true;
            final res = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
            edgeCenter.vadd(orthogonal, res.rj); // box rj
            res.rj.copy(res.rj);
            dist.negate(res.ni);
            res.ni.normalize();

            res.ri.copy(res.rj);
            res.ri.vadd(xj, res.ri);
            res.ri.vsub(xi, res.ri);
            res.ri.normalize();
            res.ri.scale(R, res.ri);

            // Make relative to bodies
            res.ri.vadd(xi, res.ri);
            res.ri.vsub(bi.position, res.ri);
            res.rj.vadd(xj, res.rj);
            res.rj.vsub(bj.position, res.rj);

            this.result.add(res);
            this.createFrictionEquationsFromContact(res, this.frictionResult);
          }
        }
      }
    }
    v3pool.release([edgeTangent, edgeCenter, r, orthogonal, dist]);
  }

  sphereConvex(Sphere si, ConvexPolyhedron sj, Vec3 xi, Vec3 xj, Quaternion qi,
      Quaternion qj, Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    final v3pool = this.v3pool;
    xi.vsub(xj, convex_to_sphere);
    final normals = sj.faceNormals;
    final faces = sj.faces;
    final verts = sj.vertices;
    final R = si.radius;
    final penetrating_sides = [];

    // if(convex_to_sphere.lengthSquared() > si.boundingSphereRadius + sj.boundingSphereRadius){
    //     return;
    // }
    bool found = false;

    // Check corners
    for (var i = 0; i != verts.length; i++) {
      final v = verts[i];

      // World position of corner
      final worldCorner = sphereConvex_worldCorner;
      qj.vmult(v, worldCorner);
      xj.vadd(worldCorner, worldCorner);
      final sphere_to_corner = sphereConvex_sphereToCorner;
      worldCorner.vsub(xi, sphere_to_corner);
      if (sphere_to_corner.lengthSquared() < R * R) {
        if (justTest) {
          return true;
        }
        found = true;
        final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
        r.ri.copy(sphere_to_corner);
        r.ri.normalize();
        r.ni.copy(r.ri);
        r.ri.scale(R, r.ri);
        worldCorner.vsub(xj, r.rj);

        // Should be relative to the body.
        r.ri.vadd(xi, r.ri);
        r.ri.vsub(bi.position, r.ri);

        // Should be relative to the body.
        r.rj.vadd(xj, r.rj);
        r.rj.vsub(bj.position, r.rj);

        this.result.add(r);
        this.createFrictionEquationsFromContact(r, this.frictionResult);
        return;
      }
    }

    // Check side (plane) intersections
    for (var i = 0, nfaces = faces.length; i != nfaces && found == false; i++) {
      final normal = normals[i];
      final face = faces[i];

      // Get world-transformed normal of the face
      final worldNormal = sphereConvex_worldNormal;
      qj.vmult(normal, worldNormal);

      // Get a world vertex from the face
      final worldPoint = sphereConvex_worldPoint;
      qj.vmult(verts[face[0]], worldPoint);
      worldPoint.vadd(xj, worldPoint);

      // Get a point on the sphere, closest to the face normal
      final worldSpherePointClosestToPlane =
          sphereConvex_worldSpherePointClosestToPlane;
      worldNormal.scale(-R, worldSpherePointClosestToPlane);
      xi.vadd(worldSpherePointClosestToPlane, worldSpherePointClosestToPlane);

      // Vector from a face point to the closest point on the sphere
      final penetrationVec = sphereConvex_penetrationVec;
      worldSpherePointClosestToPlane.vsub(worldPoint, penetrationVec);

      // The penetration. Negative value means overlap.
      final penetration = penetrationVec.dot(worldNormal);

      final worldPointToSphere = sphereConvex_sphereToWorldPoint;
      xi.vsub(worldPoint, worldPointToSphere);

      if (penetration < 0 && worldPointToSphere.dot(worldNormal) > 0) {
        // Intersects plane. Now check if the sphere is inside the face polygon
        final faceVerts = []; // Face vertices, in world coords
        for (var j = 0, Nverts = face.length; j != Nverts; j++) {
          final worldVertex = v3pool.get();
          qj.vmult(verts[face[j]], worldVertex);
          xj.vadd(worldVertex, worldVertex);
          faceVerts.add(worldVertex);
        }

        if (pointInPolygon(faceVerts, worldNormal, xi)) {
          // Is the sphere center in the face polygon?
          if (justTest) {
            return true;
          }
          found = true;
          final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);

          worldNormal.scale(
              -R, r.ri); // Contact offset, from sphere center to contact
          worldNormal.negate(r.ni); // Normal pointing out of sphere

          final penetrationVec2 = v3pool.get();
          worldNormal.scale(-penetration, penetrationVec2);
          final penetrationSpherePoint = v3pool.get();
          worldNormal.scale(-R, penetrationSpherePoint);

          //xi.vsub(xj).vadd(penetrationSpherePoint).vadd(penetrationVec2 , r.rj);
          xi.vsub(xj, r.rj);
          r.rj.vadd(penetrationSpherePoint, r.rj);
          r.rj.vadd(penetrationVec2, r.rj);

          // Should be relative to the body.
          r.rj.vadd(xj, r.rj);
          r.rj.vsub(bj.position, r.rj);

          // Should be relative to the body.
          r.ri.vadd(xi, r.ri);
          r.ri.vsub(bi.position, r.ri);

          v3pool.release(penetrationVec2);
          v3pool.release(penetrationSpherePoint);

          this.result.add(r);
          this.createFrictionEquationsFromContact(r, this.frictionResult);

          // Release world vertices
          for (var j = 0, Nfaceverts = faceVerts.length; j != Nfaceverts; j++) {
            v3pool.release(faceVerts[j]);
          }

          return; // We only expect *one* face contact
        } else {
          // Edge?
          for (var j = 0; j != face.length; j++) {
            // Get two world transformed vertices
            final v1 = v3pool.get();
            final v2 = v3pool.get();
            qj.vmult(verts[face[(j + 1) % face.length]], v1);
            qj.vmult(verts[face[(j + 2) % face.length]], v2);
            xj.vadd(v1, v1);
            xj.vadd(v2, v2);

            // Construct edge vector
            final edge = sphereConvex_edge;
            v2.vsub(v1, edge);

            // Construct the same vector, but normalized
            final edgeUnit = sphereConvex_edgeUnit;
            edge.unit(edgeUnit);

            // p is xi projected onto the edge
            final p = v3pool.get();
            final v1_to_xi = v3pool.get();
            xi.vsub(v1, v1_to_xi);
            final dot = v1_to_xi.dot(edgeUnit);
            edgeUnit.scale(dot, p);
            p.vadd(v1, p);

            // Compute a vector from p to the center of the sphere
            final xi_to_p = v3pool.get();
            p.vsub(xi, xi_to_p);

            // Collision if the edge-sphere distance is less than the radius
            // AND if p is in between v1 and v2
            if (dot > 0 &&
                dot * dot < edge.lengthSquared() &&
                xi_to_p.lengthSquared() < R * R) {
              // Collision if the edge-sphere distance is less than the radius
              // Edge contact!
              if (justTest) {
                return true;
              }
              final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
              p.vsub(xj, r.rj);

              p.vsub(xi, r.ni);
              r.ni.normalize();

              r.ni.scale(R, r.ri);

              // Should be relative to the body.
              r.rj.vadd(xj, r.rj);
              r.rj.vsub(bj.position, r.rj);

              // Should be relative to the body.
              r.ri.vadd(xi, r.ri);
              r.ri.vsub(bi.position, r.ri);

              this.result.add(r);
              this.createFrictionEquationsFromContact(r, this.frictionResult);

              // Release world vertices
              for (var j = 0, Nfaceverts = faceVerts.length;
                  j != Nfaceverts;
                  j++) {
                v3pool.release(faceVerts[j]);
              }

              v3pool.release(v1);
              v3pool.release(v2);
              v3pool.release(p);
              v3pool.release(xi_to_p);
              v3pool.release(v1_to_xi);

              return;
            }

            v3pool.release(v1);
            v3pool.release(v2);
            v3pool.release(p);
            v3pool.release(xi_to_p);
            v3pool.release(v1_to_xi);
          }
        }

        // Release world vertices
        for (var j = 0, Nfaceverts = faceVerts.length; j != Nfaceverts; j++) {
          v3pool.release(faceVerts[j]);
        }
      }
    }
  }

  planeConvex(
      Plane planeShape,
      ConvexPolyhedron convexShape,
      Vec3 planePosition,
      Vec3 convexPosition,
      Quaternion planeQuat,
      Quaternion convexQuat,
      Body planeBody,
      Body convexBody,
      Shape si,
      Shape sj,
      bool justTest) {
    // Simply return the points behind the plane.
    final worldVertex = planeConvex_v;

    final worldNormal = planeConvex_normal;
    worldNormal.set(0, 0, 1);
    planeQuat.vmult(
        worldNormal, worldNormal); // Turn normal according to plane orientation

    var numContacts = 0;
    final relpos = planeConvex_relpos;
    for (var i = 0; i != convexShape.vertices.length; i++) {
      // Get world convex vertex
      worldVertex.copy(convexShape.vertices[i]);
      convexQuat.vmult(worldVertex, worldVertex);
      convexPosition.vadd(worldVertex, worldVertex);
      worldVertex.vsub(planePosition, relpos);

      final dot = worldNormal.dot(relpos);
      if (dot <= 0.0) {
        if (justTest) {
          return true;
        }

        final r = this.createContactEquation(
            planeBody, convexBody, planeShape, convexShape, si, sj);

        // Get vertex position projected on plane
        final projected = planeConvex_projected;
        worldNormal.scale(worldNormal.dot(relpos), projected);
        worldVertex.vsub(projected, projected);
        projected.vsub(
            planePosition, r.ri); // From plane to vertex projected on plane

        r.ni.copy(
            worldNormal); // Contact normal is the plane normal out from plane

        // rj is now just the vector from the convex center to the vertex
        worldVertex.vsub(convexPosition, r.rj);

        // Make it relative to the body
        r.ri.vadd(planePosition, r.ri);
        r.ri.vsub(planeBody.position, r.ri);
        r.rj.vadd(convexPosition, r.rj);
        r.rj.vsub(convexBody.position, r.rj);

        this.result.add(r);
        numContacts++;
        if (!this.enableFrictionReduction) {
          this.createFrictionEquationsFromContact(r, this.frictionResult);
        }
      }
    }

    if (this.enableFrictionReduction && numContacts != null) {
      this.createFrictionFromAverage(numContacts);
    }
  }

  boxConvex(Box si, ConvexPolyhedron sj, Vec3 xi, Vec3 xj, Quaternion qi,
      Quaternion qj, Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    si.convexPolyhedronRepresentation.material = si.material;
    si.convexPolyhedronRepresentation.collisionResponse = si.collisionResponse;
    return this.convexConvex(si.convexPolyhedronRepresentation, sj, xi, xj, qi,
        qj, bi, bj, si, sj, justTest);
  }

  sphereHeightfield(
      Sphere sphereShape,
      Heightfield hfShape,
      Vec3 spherePos,
      Vec3 hfPos,
      Quaternion sphereQuat,
      Quaternion hfQuat,
      Body sphereBody,
      Body hfBody,
      Shape rsi,
      Shape rsj,
      bool justTest) {
    final data = hfShape.data;
    final radius = sphereShape.radius;
    final w = hfShape.elementSize;
    final worldPillarOffset = sphereHeightfield_tmp2;

    // Get sphere position to heightfield local!
    final localSpherePos = sphereHeightfield_tmp1;
    Transform.pointToLocalFrame(hfPos, hfQuat, spherePos, localSpherePos);

    // Get the index of the data points to test against
    var iMinX = ((localSpherePos.x - radius) / w).floor() - 1;

    var iMaxX = ((localSpherePos.x + radius) / w).ceil() + 1;
    var iMinY = ((localSpherePos.y - radius) / w).floor() - 1;
    var iMaxY = ((localSpherePos.y + radius) / w).ceil() + 1;

    // Bail out if we are out of the terrain
    if (iMaxX < 0 ||
        iMaxY < 0 ||
        iMinX > data.length ||
        iMinY > data[0].length) {
      return;
    }

    // Clamp index to edges
    if (iMinX < 0) {
      iMinX = 0;
    }
    if (iMaxX < 0) {
      iMaxX = 0;
    }
    if (iMinY < 0) {
      iMinY = 0;
    }
    if (iMaxY < 0) {
      iMaxY = 0;
    }
    if (iMinX >= data.length) {
      iMinX = data.length - 1;
    }
    if (iMaxX >= data.length) {
      iMaxX = data.length - 1;
    }
    if (iMaxY >= data[0].length) {
      iMaxY = data[0].length - 1;
    }
    if (iMinY >= data[0].length) {
      iMinY = data[0].length - 1;
    }

    final minMax = [];
    hfShape.getRectMinMax(iMinX, iMinY, iMaxX, iMaxY, minMax);
    final min = minMax[0];
    final max = minMax[1];

    // Bail out if we can't touch the bounding height box
    if (localSpherePos.z - radius > max || localSpherePos.z + radius < min) {
      return;
    }

    final result = this.result;
    for (var i = iMinX; i < iMaxX; i++) {
      for (var j = iMinY; j < iMaxY; j++) {
        final numContactsBefore = result.length;

        var intersecting = false;

        // Lower triangle
        hfShape.getConvexTrianglePillar(i, j, false);
        Transform.pointToWorldFrame(
            hfPos, hfQuat, hfShape.pillarOffset, worldPillarOffset);
        if (spherePos.distanceTo(worldPillarOffset) <
            hfShape.pillarConvex.boundingSphereRadius +
                sphereShape.boundingSphereRadius) {
          intersecting = this.sphereConvex(
              sphereShape,
              hfShape.pillarConvex,
              spherePos,
              worldPillarOffset,
              sphereQuat,
              hfQuat,
              sphereBody,
              hfBody,
              sphereShape,
              hfShape,
              justTest) as bool;
        }

        if (justTest && intersecting) {
          return true;
        }

        // Upper triangle
        hfShape.getConvexTrianglePillar(i, j, true);
        Transform.pointToWorldFrame(
            hfPos, hfQuat, hfShape.pillarOffset, worldPillarOffset);
        if (spherePos.distanceTo(worldPillarOffset) <
            hfShape.pillarConvex.boundingSphereRadius +
                sphereShape.boundingSphereRadius) {
          intersecting = this.sphereConvex(
              sphereShape,
              hfShape.pillarConvex,
              spherePos,
              worldPillarOffset,
              sphereQuat,
              hfQuat,
              sphereBody,
              hfBody,
              sphereShape,
              hfShape,
              justTest) as bool;
        }

        if (justTest && intersecting) {
          return true;
        }

        final numContacts = result.length - numContactsBefore;

        if (numContacts > 2) {
          return;
        }
        /*
          // Skip all but 1
          for (let k = 0; k < numContacts - 1; k++) {
              result.pop();
          }
        */
      }
    }
  }

  boxHeightfield(Box si, Heightfield sj, Vec3 xi, Vec3 xj, Quaternion qi,
      Quaternion qj, Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    si.convexPolyhedronRepresentation.material = si.material;
    si.convexPolyhedronRepresentation.collisionResponse = si.collisionResponse;
    return this.convexHeightfield(si.convexPolyhedronRepresentation, sj, xi, xj,
        qi, qj, bi, bj, si, sj, justTest);
  }

  convexHeightfield(
      ConvexPolyhedron convexShape,
      Heightfield hfShape,
      Vec3 convexPos,
      Vec3 hfPos,
      Quaternion convexQuat,
      Quaternion hfQuat,
      Body convexBody,
      Body hfBody,
      Shape rsi,
      Shape rsj,
      bool justTest) {
    final data = hfShape.data;
    final w = hfShape.elementSize;
    final radius = convexShape.boundingSphereRadius;
    final worldPillarOffset = convexHeightfield_tmp2;
    final faceList = convexHeightfield_faceList;

    // Get sphere position to heightfield local!
    final localConvexPos = convexHeightfield_tmp1;
    Transform.pointToLocalFrame(hfPos, hfQuat, convexPos, localConvexPos);

    // Get the index of the data points to test against
    var iMinX = ((localConvexPos.x - radius) / w).floor() - 1;

    var iMaxX = ((localConvexPos.x + radius) / w).ceil() + 1;
    var iMinY = ((localConvexPos.y - radius) / w).floor() - 1;
    var iMaxY = ((localConvexPos.y + radius) / w).ceil() + 1;

    // Bail out if we are out of the terrain
    if (iMaxX < 0 ||
        iMaxY < 0 ||
        iMinX > data.length ||
        iMinY > data[0].length) {
      return;
    }

    // Clamp index to edges
    if (iMinX < 0) {
      iMinX = 0;
    }
    if (iMaxX < 0) {
      iMaxX = 0;
    }
    if (iMinY < 0) {
      iMinY = 0;
    }
    if (iMaxY < 0) {
      iMaxY = 0;
    }
    if (iMinX >= data.length) {
      iMinX = data.length - 1;
    }
    if (iMaxX >= data.length) {
      iMaxX = data.length - 1;
    }
    if (iMaxY >= data[0].length) {
      iMaxY = data[0].length - 1;
    }
    if (iMinY >= data[0].length) {
      iMinY = data[0].length - 1;
    }

    final List<num> minMax = [];
    hfShape.getRectMinMax(iMinX, iMinY, iMaxX, iMaxY, minMax);
    final min = minMax[0];
    final max = minMax[1];

    // Bail out if we're cant touch the bounding height box
    if (localConvexPos.z - radius > max || localConvexPos.z + radius < min) {
      return;
    }

    for (var i = iMinX; i < iMaxX; i++) {
      for (var j = iMinY; j < iMaxY; j++) {
        var intersecting = false;

        // Lower triangle
        hfShape.getConvexTrianglePillar(i, j, false);
        Transform.pointToWorldFrame(
            hfPos, hfQuat, hfShape.pillarOffset, worldPillarOffset);
        if (convexPos.distanceTo(worldPillarOffset) <
            hfShape.pillarConvex.boundingSphereRadius +
                convexShape.boundingSphereRadius) {
          intersecting = this.convexConvex(
              convexShape,
              hfShape.pillarConvex,
              convexPos,
              worldPillarOffset,
              convexQuat,
              hfQuat,
              convexBody,
              hfBody,
              null,
              null,
              justTest,
              faceList,
              null) as bool;
        }

        if (justTest && intersecting) {
          return true;
        }

        // Upper triangle
        hfShape.getConvexTrianglePillar(i, j, true);
        Transform.pointToWorldFrame(
            hfPos, hfQuat, hfShape.pillarOffset, worldPillarOffset);
        if (convexPos.distanceTo(worldPillarOffset) <
            hfShape.pillarConvex.boundingSphereRadius +
                convexShape.boundingSphereRadius) {
          intersecting = this.convexConvex(
              convexShape,
              hfShape.pillarConvex,
              convexPos,
              worldPillarOffset,
              convexQuat,
              hfQuat,
              convexBody,
              hfBody,
              null,
              null,
              justTest,
              faceList,
              null) as bool;
        }

        if (justTest && intersecting) {
          return true;
        }
      }
    }
  }

  sphereParticle(Sphere sj, Particle si, Vec3 xj, Vec3 xi, Quaternion qj,
      Quaternion qi, Body bj, Body bi, Shape rsi, Shape rsj, bool justTest) {
    // The normal is the unit vector from sphere center to particle center
    final normal = particleSphere_normal;
    normal.set(0, 0, 1);
    xi.vsub(xj, normal);
    final lengthSquared = normal.lengthSquared();

    if (lengthSquared <= sj.radius * sj.radius) {
      if (justTest) {
        return true;
      }
      final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
      normal.normalize();
      r.rj.copy(normal);
      r.rj.scale(sj.radius, r.rj);
      r.ni.copy(normal); // Contact normal
      r.ni.negate(r.ni);
      r.ri.set(0, 0, 0); // Center of particle
      this.result.add(r);
      this.createFrictionEquationsFromContact(r, this.frictionResult);
    }
  }

  planeParticle(Plane sj, Particle si, Vec3 xj, Vec3 xi, Quaternion qj,
      Quaternion qi, Body bj, Body bi, Shape rsi, Shape rsj, bool justTest) {
    final normal = particlePlane_normal;
    normal.set(0, 0, 1);
    bj.quaternion
        .vmult(normal, normal); // Turn normal according to plane orientation
    final relpos = particlePlane_relpos;
    xi.vsub(bj.position, relpos);
    final dot = normal.dot(relpos);
    if (dot <= 0.0) {
      if (justTest) {
        return true;
      }

      final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
      r.ni.copy(normal); // Contact normal is the plane normal
      r.ni.negate(r.ni);
      r.ri.set(0, 0, 0); // Center of particle

      // Get particle position projected on plane
      final projected = particlePlane_projected;
      normal.scale(normal.dot(xi), projected);
      xi.vsub(projected, projected);
      //projected.vadd(bj.position,projected);

      // rj is now the projected world position minus plane position
      r.rj.copy(projected);
      this.result.add(r);
      this.createFrictionEquationsFromContact(r, this.frictionResult);
    }
  }

  boxParticle(Box si, Particle sj, Vec3 xi, Vec3 xj, Quaternion qi,
      Quaternion qj, Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    si.convexPolyhedronRepresentation.material = si.material;
    si.convexPolyhedronRepresentation.collisionResponse = si.collisionResponse;
    return this.convexParticle(si.convexPolyhedronRepresentation, sj, xi, xj,
        qi, qj, bi, bj, si, sj, justTest);
  }

  convexParticle(
      ConvexPolyhedron sj,
      Particle si,
      Vec3 xj,
      Vec3 xi,
      Quaternion qj,
      Quaternion qi,
      Body bj,
      Body bi,
      Shape rsi,
      Shape rsj,
      bool justTest) {
    var penetratedFaceIndex = -1;
    final penetratedFaceNormal = convexParticle_penetratedFaceNormal;
    final worldPenetrationVec = convexParticle_worldPenetrationVec;
    num minPenetration = null;
    num numDetectedFaces = 0;

    // Convert particle position xi to local coords in the convex
    final local = convexParticle_local;
    local.copy(xi);
    local.vsub(xj, local); // Convert position to relative the convex origin
    qj.conjugate(cqj);
    cqj.vmult(local, local);

    if (sj.pointIsInside(local)) {
      if (sj.worldVerticesNeedsUpdate) {
        sj.computeWorldVertices(xj, qj);
      }
      if (sj.worldFaceNormalsNeedsUpdate) {
        sj.computeWorldFaceNormals(qj);
      }

      // For each world polygon in the polyhedra
      for (var i = 0, nfaces = sj.faces.length; i != nfaces; i++) {
        // Construct world face vertices
        final verts = [sj.worldVertices[sj.faces[i][0]]];
        final normal = sj.worldFaceNormals[i];

        // Check how much the particle penetrates the polygon plane.
        xi.vsub(verts[0], convexParticle_vertexToParticle);
        final penetration = -normal.dot(convexParticle_vertexToParticle);
        if (minPenetration == null ||
            (penetration).abs() < (minPenetration).abs()) {
          if (justTest) {
            return true;
          }

          minPenetration = penetration;
          penetratedFaceIndex = i;
          penetratedFaceNormal.copy(normal);
          numDetectedFaces++;
        }
      }

      if (penetratedFaceIndex != -1) {
        // Setup contact
        final r = this.createContactEquation(bi, bj, si, sj, rsi, rsj);
        penetratedFaceNormal.scale(minPenetration, worldPenetrationVec);

        // rj is the particle position projected to the face
        worldPenetrationVec.vadd(xi, worldPenetrationVec);
        worldPenetrationVec.vsub(xj, worldPenetrationVec);
        r.rj.copy(worldPenetrationVec);
        //final projectedToFace = xi.vsub(xj).vadd(worldPenetrationVec);
        //projectedToFace.copy(r.rj);

        //qj.vmult(r.rj,r.rj);
        penetratedFaceNormal.negate(r.ni); // Contact normal
        r.ri.set(0, 0, 0); // Center of particle

        final ri = r.ri;
        final rj = r.rj;

        // Make relative to bodies
        ri.vadd(xi, ri);
        ri.vsub(bi.position, ri);
        rj.vadd(xj, rj);
        rj.vsub(bj.position, rj);

        this.result.add(r);
        this.createFrictionEquationsFromContact(r, this.frictionResult);
      } else {
        print('Point found inside convex, but did not find penetrating face!');
      }
    }
  }

  sphereTrimesh(
      Sphere sphereShape,
      Trimesh trimeshShape,
      Vec3 spherePos,
      Vec3 trimeshPos,
      Quaternion sphereQuat,
      Quaternion trimeshQuat,
      Body sphereBody,
      Body trimeshBody,
      [Shape rsi,
      Shape rsj,
      bool justTest]) {
    final edgeVertexA = sphereTrimesh_edgeVertexA;
    final edgeVertexB = sphereTrimesh_edgeVertexB;
    final edgeVector = sphereTrimesh_edgeVector;
    final edgeVectorUnit = sphereTrimesh_edgeVectorUnit;
    final localSpherePos = sphereTrimesh_localSpherePos;
    final tmp = sphereTrimesh_tmp;
    final localSphereAABB = sphereTrimesh_localSphereAABB;
    final v2 = sphereTrimesh_v2;
    final relpos = sphereTrimesh_relpos;
    final triangles = sphereTrimesh_triangles;

    // Convert sphere position to local in the trimesh
    Transform.pointToLocalFrame(
        trimeshPos, trimeshQuat, spherePos, localSpherePos);

    // Get the aabb of the sphere locally in the trimesh
    final sphereRadius = sphereShape.radius;
    localSphereAABB.lowerBound.set(localSpherePos.x - sphereRadius,
        localSpherePos.y - sphereRadius, localSpherePos.z - sphereRadius);
    localSphereAABB.upperBound.set(localSpherePos.x + sphereRadius,
        localSpherePos.y + sphereRadius, localSpherePos.z + sphereRadius);

    trimeshShape.getTrianglesInAABB(localSphereAABB, triangles);
    //for (let i = 0; i < trimeshShape.indices.length / 3; i++) triangles.push(i); // All

    // Vertices
    final v = sphereTrimesh_v;
    final radiusSquared = sphereShape.radius * sphereShape.radius;
    for (int i = 0; i < triangles.length; i++) {
      for (int j = 0; j < 3; j++) {
        trimeshShape.getVertex(trimeshShape.indices[triangles[i] * 3 + j], v);

        // Check vertex overlap in sphere
        v.vsub(localSpherePos, relpos);

        if (relpos.lengthSquared() <= radiusSquared) {
          // Safe up
          v2.copy(v);
          Transform.pointToWorldFrame(trimeshPos, trimeshQuat, v2, v);

          v.vsub(spherePos, relpos);

          if (justTest) {
            return true;
          }

          var r = this.createContactEquation(
              sphereBody, trimeshBody, sphereShape, trimeshShape, rsi, rsj);
          r.ni.copy(relpos);
          r.ni.normalize();

          // ri is the vector from sphere center to the sphere surface
          r.ri.copy(r.ni);
          r.ri.scale(sphereShape.radius, r.ri);
          r.ri.vadd(spherePos, r.ri);
          r.ri.vsub(sphereBody.position, r.ri);

          r.rj.copy(v);
          r.rj.vsub(trimeshBody.position, r.rj);

          // Store result
          this.result.add(r);
          this.createFrictionEquationsFromContact(r, this.frictionResult);
        }
      }
    }

    // Check all edges
    for (int i = 0; i < triangles.length; i++) {
      for (int j = 0; j < 3; j++) {
        trimeshShape.getVertex(
            trimeshShape.indices[triangles[i] * 3 + j], edgeVertexA);
        trimeshShape.getVertex(
            trimeshShape.indices[triangles[i] * 3 + ((j + 1) % 3)],
            edgeVertexB);
        edgeVertexB.vsub(edgeVertexA, edgeVector);

        // Project sphere position to the edge
        localSpherePos.vsub(edgeVertexB, tmp);
        final positionAlongEdgeB = tmp.dot(edgeVector);

        localSpherePos.vsub(edgeVertexA, tmp);
        var positionAlongEdgeA = tmp.dot(edgeVector);

        if (positionAlongEdgeA > 0 && positionAlongEdgeB < 0) {
          // Now check the orthogonal distance from edge to sphere center
          localSpherePos.vsub(edgeVertexA, tmp);

          edgeVectorUnit.copy(edgeVector);
          edgeVectorUnit.normalize();
          positionAlongEdgeA = tmp.dot(edgeVectorUnit);

          edgeVectorUnit.scale(positionAlongEdgeA, tmp);
          tmp.vadd(edgeVertexA, tmp);

          // tmp is now the sphere center position projected to the edge, defined locally in the trimesh frame
          final dist = tmp.distanceTo(localSpherePos);
          if (dist < sphereShape.radius) {
            if (justTest) {
              return true;
            }

            final r = this.createContactEquation(
                sphereBody, trimeshBody, sphereShape, trimeshShape, rsi, rsj);

            tmp.vsub(localSpherePos, r.ni);
            r.ni.normalize();
            r.ni.scale(sphereShape.radius, r.ri);
            r.ri.vadd(spherePos, r.ri);
            r.ri.vsub(sphereBody.position, r.ri);

            Transform.pointToWorldFrame(trimeshPos, trimeshQuat, tmp, tmp);
            tmp.vsub(trimeshBody.position, r.rj);

            Transform.vectorToWorldFrameStatic(trimeshQuat, r.ni, r.ni);
            Transform.vectorToWorldFrameStatic(trimeshQuat, r.ri, r.ri);

            this.result.add(r);
            this.createFrictionEquationsFromContact(r, this.frictionResult);
          }
        }
      }
    }

    // Triangle faces
    final va = sphereTrimesh_va;
    final vb = sphereTrimesh_vb;
    final vc = sphereTrimesh_vc;
    final normal = sphereTrimesh_normal;
    for (int i = 0, N = triangles.length; i != N; i++) {
      trimeshShape.getTriangleVertices(triangles[i], va, vb, vc);
      trimeshShape.getNormal(triangles[i], normal);
      localSpherePos.vsub(va, tmp);
      var dist = tmp.dot(normal);
      normal.scale(dist, tmp);
      localSpherePos.vsub(tmp, tmp);

      // tmp is now the sphere position projected to the triangle plane
      dist = tmp.distanceTo(localSpherePos);
      if (Ray.pointInTriangle(tmp, va, vb, vc) && dist < sphereShape.radius) {
        if (justTest) {
          return true;
        }
        var r = this.createContactEquation(
            sphereBody, trimeshBody, sphereShape, trimeshShape, rsi, rsj);

        tmp.vsub(localSpherePos, r.ni);
        r.ni.normalize();
        r.ni.scale(sphereShape.radius, r.ri);
        r.ri.vadd(spherePos, r.ri);
        r.ri.vsub(sphereBody.position, r.ri);

        Transform.pointToWorldFrame(trimeshPos, trimeshQuat, tmp, tmp);
        tmp.vsub(trimeshBody.position, r.rj);

        Transform.vectorToWorldFrameStatic(trimeshQuat, r.ni, r.ni);
        Transform.vectorToWorldFrameStatic(trimeshQuat, r.ri, r.ri);

        this.result.add(r);
        this.createFrictionEquationsFromContact(r, this.frictionResult);
      }
    }

    triangles.length = 0;
  }

  planeTrimesh(
      Plane planeShape,
      Trimesh trimeshShape,
      Vec3 planePos,
      Vec3 trimeshPos,
      Quaternion planeQuat,
      Quaternion trimeshQuat,
      Body planeBody,
      Body trimeshBody,
      [Shape rsi,
      Shape rsj,
      bool justTest]) {
    // Make contacts!
    final v = Vec3();

    final normal = planeTrimesh_normal;
    normal.set(0, 0, 1);
    planeQuat.vmult(normal, normal); // Turn normal according to plane

    for (int i = 0; i < trimeshShape.vertices.length / 3; i++) {
      // Get world vertex from trimesh
      trimeshShape.getVertex(i, v);

      // Safe up
      final v2 = Vec3();
      v2.copy(v);
      Transform.pointToWorldFrame(trimeshPos, trimeshQuat, v2, v);

      // Check plane side
      final relpos = planeTrimesh_relpos;
      v.vsub(planePos, relpos);
      final dot = normal.dot(relpos);

      if (dot <= 0.0) {
        if (justTest) {
          return true;
        }

        final r = this.createContactEquation(
            planeBody, trimeshBody, planeShape, trimeshShape, rsi, rsj);

        r.ni.copy(normal); // Contact normal is the plane normal

        // Get vertex position projected on plane
        final projected = planeTrimesh_projected;
        normal.scale(relpos.dot(normal), projected);
        v.vsub(projected, projected);

        // ri is the projected world position minus plane position
        r.ri.copy(projected);
        r.ri.vsub(planeBody.position, r.ri);

        r.rj.copy(v);
        r.rj.vsub(trimeshBody.position, r.rj);

        // Store result
        this.result.add(r);
        this.createFrictionEquationsFromContact(r, this.frictionResult);
      }
    }
  }

  planeBox(Plane si, Box sj, Vec3 xi, Vec3 xj, Quaternion qi, Quaternion qj,
      Body bi, Body bj, Shape rsi, Shape rsj, bool justTest) {
    sj.convexPolyhedronRepresentation.material = sj.material;
    sj.convexPolyhedronRepresentation.collisionResponse = sj.collisionResponse;
    sj.convexPolyhedronRepresentation.id = sj.id;
    return this.planeConvex(si, sj.convexPolyhedronRepresentation, xi, xj, qi,
        qj, bi, bj, si, sj, justTest);
  }
}

final averageNormal = new Vec3();

final averageContactPointA = new Vec3();

final averageContactPointB = new Vec3();

final tmpVec1 = new Vec3();

final tmpVec2 = new Vec3();

final tmpQuat1 = new Quaternion();

final tmpQuat2 = new Quaternion();

var numWarnings = 0;

final maxWarnings = 10;

void warn(String msg) {
  if (numWarnings > maxWarnings) {
    return;
  }
  numWarnings++;
  print(msg);
}

final planeTrimesh_normal = Vec3();

final planeTrimesh_relpos = new Vec3();

final planeTrimesh_projected = new Vec3();

final sphereTrimesh_normal = new Vec3();

final sphereTrimesh_relpos = new Vec3();

final sphereTrimesh_projected = new Vec3();

final sphereTrimesh_v = new Vec3();

final sphereTrimesh_v2 = new Vec3();

final sphereTrimesh_edgeVertexA = new Vec3();

final sphereTrimesh_edgeVertexB = new Vec3();

final sphereTrimesh_edgeVector = new Vec3();

final sphereTrimesh_edgeVectorUnit = new Vec3();

final sphereTrimesh_localSpherePos = new Vec3();

final sphereTrimesh_tmp = new Vec3();

final sphereTrimesh_va = new Vec3();

final sphereTrimesh_vb = new Vec3();

final sphereTrimesh_vc = new Vec3();

final sphereTrimesh_localSphereAABB = new AABB();

final List<num> sphereTrimesh_triangles = [];

final point_on_plane_to_sphere = new Vec3();

final plane_to_sphere_ortho = new Vec3();

// See http://bulletphysics.com/Bullet/BulletFull/SphereTriangleDetector_8cpp_source.html
final pointInPolygon_edge = new Vec3();

final pointInPolygon_edge_x_normal = new Vec3();

final pointInPolygon_vtp = new Vec3();

bool pointInPolygon(List<Vec3> verts, Vec3 normal, Vec3 p) {
  var positiveResult = null;
  final N = verts.length;
  for (var i = 0; !identical(i, N); i++) {
    final v = verts[i];
    // Get edge to the next vertex
    final edge = pointInPolygon_edge;
    verts[(i + 1) % N].vsub(v, edge);
    // Get cross product between polygon normal and the edge
    final edge_x_normal = pointInPolygon_edge_x_normal;
    //const edge_x_normal = new Vec3();
    edge.cross(normal, edge_x_normal);
    // Get vector between point and current vertex
    final vertex_to_p = pointInPolygon_vtp;
    p.vsub(v, vertex_to_p);
    // This dot product determines which side of the edge the point is
    final r = edge_x_normal.dot(vertex_to_p);
    // If all such dot products have same sign, we are inside the polygon.
    if (identical(positiveResult, null) ||
        (r > 0 && identical(positiveResult, true)) ||
        (r <= 0 && identical(positiveResult, false))) {
      if (identical(positiveResult, null)) {
        positiveResult = r > 0;
      }
      continue;
    } else {
      return false;
    }
  }
  // If we got here, all dot products were of the same sign.
  return true;
}

final box_to_sphere = new Vec3();
final sphereBox_ns = new Vec3();
final sphereBox_ns1 = new Vec3();
final sphereBox_ns2 = new Vec3();
final sphereBox_sides = [
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3()
];
final sphereBox_sphere_to_corner = new Vec3();
final sphereBox_side_ns = new Vec3();
final sphereBox_side_ns1 = new Vec3();
final sphereBox_side_ns2 = new Vec3();

final convex_to_sphere = new Vec3();
final sphereConvex_edge = new Vec3();
final sphereConvex_edgeUnit = new Vec3();
final sphereConvex_sphereToCorner = new Vec3();
final sphereConvex_worldCorner = new Vec3();
final sphereConvex_worldNormal = new Vec3();
final sphereConvex_worldPoint = new Vec3();
final sphereConvex_worldSpherePointClosestToPlane = new Vec3();
final sphereConvex_penetrationVec = new Vec3();
final sphereConvex_sphereToWorldPoint = new Vec3();

final planeBox_normal = new Vec3();
final plane_to_corner = new Vec3();

final planeConvex_v = new Vec3();
final planeConvex_normal = new Vec3();
final planeConvex_relpos = new Vec3();
final planeConvex_projected = new Vec3();

final convexConvex_sepAxis = new Vec3();
final convexConvex_q = new Vec3();

final particlePlane_normal = new Vec3();
final particlePlane_relpos = new Vec3();
final particlePlane_projected = new Vec3();

final particleSphere_normal = new Vec3();

// WIP
final cqj = new Quaternion();
final convexParticle_local = new Vec3();
final convexParticle_normal = new Vec3();
final convexParticle_penetratedFaceNormal = new Vec3();
final convexParticle_vertexToParticle = new Vec3();
final convexParticle_worldPenetrationVec = new Vec3();

final convexHeightfield_tmp1 = Vec3();
final convexHeightfield_tmp2 = Vec3();
final convexHeightfield_faceList = [0];

final sphereHeightfield_tmp1 = Vec3();
final sphereHeightfield_tmp2 = Vec3();
