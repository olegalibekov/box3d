import "../math/Vec3.dart" show Vec3;
import "../math/Quaternion.dart" show Quaternion;
import "../shapes/Heightfield.dart" show Heightfield;
import "Narrowphase.dart" show Narrowphase;
import "../shapes/Sphere.dart" show Sphere;
import "../objects/Body.dart" show Body;
import "../world/World.dart" show World;
import "../equations/ContactEquation.dart" show ContactEquation;

// describe
// ( "Narrowphase" , ( ) { test ( "sphere + sphere contact" , ( ) { final world = new World ( ) ; final narrowPhase = new Narrowphase ( world ) ; final List < ContactEquation > result = [ ] ; final sphereShape = new Sphere ( 1 ) ; final bodyA = new Body ( mass : 1 ) ; final bodyB = new Body ( mass : 1 ) ; bodyA . addShape ( sphereShape ) ; bodyB . addShape ( sphereShape ) ;
// // Assumption: using World.defaultContactMaterial will be the same
//
// // as using `new ContactMaterial()` for narrowPhase.currentContactMaterial.
// narrowPhase . result = result ; narrowPhase . sphereSphere ( sphereShape , sphereShape , new Vec3 ( 0.5 , 0 , 0 ) , new Vec3 ( - 0.5 , 0 , 0 ) , new Quaternion ( ) , new Quaternion ( ) , bodyA , bodyB ) ; expect ( result . length ) . toBe ( 1 ) ; } ) ; test ( "sphere + heightfield contact" , ( ) { final world = new World ( ) ; final narrowPhase = new Narrowphase ( world ) ; final List < ContactEquation > result = [ ] ; final hfShape = createHeightfield ( ) ; final sphereShape = new Sphere ( 0.1 ) ; final bodyA = new Body ( mass : 1 ) ; final bodyB = new Body ( mass : 1 ) ; bodyA . addShape ( hfShape ) ; bodyB . addShape ( sphereShape ) ; narrowPhase . result = result ; narrowPhase . sphereHeightfield ( sphereShape , hfShape , new Vec3 ( 0.25 , 0.25 , 0.05 ) , new Vec3 ( 0 , 0 , 0 ) , new Quaternion ( ) , new Quaternion ( ) , bodyA , bodyB ) ; expect ( result . length ) . toBe ( 1 ) ; } ) ; } );
//
// createHeightfield() {
//   final List <List <num>> matrix = [];
//   const size = 20;
//   for (var i = 0; i < size; i ++) {
//     matrix.add([]);
//     for (var j = 0; j < size; j ++) {
//       matrix [ i ].add(0);
//     }
//   }
//   final hfShape = new Heightfield (matrix, elementSize: 1);
//   return hfShape;
// }