import 'dart:async';
import "dart:math" as Math;

import "../collision/AABB.dart" show AABB;
import "../collision/ArrayCollisionMatrix.dart" show ArrayCollisionMatrix;
import "../collision/Broadphase.dart" show Broadphase;
import "../collision/NaiveBroadphase.dart" show NaiveBroadphase;
import "../collision/OverlapKeeper.dart" show OverlapKeeper;
import "../collision/Ray.dart" show Ray, RAY_MODES;
import "../collision/Ray.dart" show RayOptions, RaycastCallback;
import "../collision/RaycastResult.dart" show RaycastResult;
import "../constraints/Constraint.dart" show Constraint;
import "../equations/ContactEquation.dart" show ContactEquation;
import "../equations/FrictionEquation.dart" show FrictionEquation;
import "../material/ContactMaterial.dart" show ContactMaterial;
import "../material/Material.dart" show Material;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart"
    show Body, BODY_TYPES, BODY_SLEEP_STATES, BodyEvents;
import '../shapes/Shape.dart' show Shape;
import "../solver/GSSolver.dart" show GSSolver;
import "../solver/Solver.dart" show Solver;
import '../utils/EventContainer.dart' show EventContainer;
import "../utils/TupleDictionary.dart" show TupleDictionary;
import "../world/Narrowphase.dart" show Narrowphase;

class WorldOptions {
  Vec3 gravity;
  bool allowSleep = true;
  Broadphase broadphase;
  Solver solver;
  bool quatNormalizeFast;
  num quatNormalizeSkip;
}

enum WorldEvents { addBody, removeBody, preStep, postStep }

/// The physics world
/// @class World
/// @constructor
/// @extends EventTarget
class World {
  final worldStreamsController = StreamController<EventContainer>();
  Map<dynamic, List<dynamic>> _subscriptions = {};

  hasAnyEventListener(eventType) {
    return _subscriptions.containsKey(eventType);
  }

  addEventListener(eventType, Function listener(event)) {
    if (!_subscriptions.containsKey(eventType))
      _subscriptions[eventType] = List();
    _subscriptions[eventType].add(worldEvents.listen(listener));
  }

  removeEventListener(eventType, [Function listener(event)]) {
    if (_subscriptions.containsKey(eventType)) {
      if (!_subscriptions[eventType].remove(listener)) {
        _subscriptions[eventType].removeAt(0);
      }
    }
  }

  dispatchEvent(EventContainer eventContainer) =>
      worldStreamsController.add(eventContainer);
  Stream<EventContainer> worldEvents;

  dispose() {
    worldStreamsController.close();
  }

  num dt;

  bool allowSleep;

  List<ContactEquation> contacts;

  List<FrictionEquation> frictionEquations;

  num quatNormalizeSkip;

  bool quatNormalizeFast;

  num time;

  num stepnumber;

  num default_dt;

  num nextId;

  Vec3 gravity;

  Broadphase broadphase;

  List<Body> bodies;

  bool hasActiveBodies;

  Solver solver;

  List<Constraint> constraints;

  Narrowphase narrowphase;

  ArrayCollisionMatrix collisionMatrix;

  ArrayCollisionMatrix collisionMatrixPrevious;

  OverlapKeeper bodyOverlapKeeper;

  OverlapKeeper shapeOverlapKeeper;

  List<Material> materials;

  List<ContactMaterial> contactmaterials;

  TupleDictionary contactMaterialTable;

  Material defaultMaterial;

  ContactMaterial defaultContactMaterial;

  bool doProfiling;

  dynamic profile;

  num accumulator;

  List<dynamic> subsystems;

  Map<num, Body> idToBodyMap;

  EmitContactEventsCallback emitContactEvents;

  World([WorldOptions options]) : super() {
    worldEvents = worldStreamsController.stream.asBroadcastStream();

    /// ToDo was options ??= {};
    options ??= WorldOptions();
    this.dt ??= -1;
    this.allowSleep ??= options.allowSleep;
    this.contacts ??= [];
    this.frictionEquations ??= [];
    this.quatNormalizeSkip =
        (options.quatNormalizeSkip != null ? options.quatNormalizeSkip : 0);
    this.quatNormalizeFast =
        (options.quatNormalizeFast != null ? options.quatNormalizeFast : false);
    this.time ??= 0.0;
    this.stepnumber ??= 0;
    this.default_dt ??= 1 / 60;
    this.nextId ??= 0;
    this.gravity ??= new Vec3();

    if (options.gravity != null) {
      this.gravity.copy(options.gravity);
    }

    this.broadphase ??= (options.broadphase != null
        ? options.broadphase
        : new NaiveBroadphase());
    this.bodies ??= [];
    this.hasActiveBodies ??= false;
    this.solver ??= (options.solver != null ? options.solver : new GSSolver());
    this.constraints ??= [];
    this.narrowphase ??= new Narrowphase(this);
    this.collisionMatrix ??= new ArrayCollisionMatrix();
    this.collisionMatrixPrevious ??= new ArrayCollisionMatrix();
    this.bodyOverlapKeeper ??= new OverlapKeeper();
    this.shapeOverlapKeeper ??= new OverlapKeeper();
    this.materials ??= [];
    this.contactmaterials ??= [];
    this.contactMaterialTable ??= new TupleDictionary();
    this.defaultMaterial ??= new Material('default');
    this.defaultContactMaterial ??= new ContactMaterial(this.defaultMaterial,
        this.defaultMaterial, {"friction": 0.3, "restitution": 0.0});
    this.doProfiling ??= false;
    this.profile ??= {
      "solve": 0,
      "makeContactConstraints": 0,
      "broadphase": 0,
      "integrate": 0,
      "narrowphase": 0,
    };

    this.accumulator ??= 0;
    this.subsystems ??= [];
    this.idToBodyMap ??= {};
    this.broadphase.setWorld(this);
    emitContactEvents = EmitContactEventsCallback(this);
  }

  /**
   * Get the contact material between materials m1 and m2
   * @method getContactMaterial
   * @param {Material} m1
   * @param {Material} m2
   * @return {ContactMaterial} The contact material if it was found.
   */
  ContactMaterial getContactMaterial(Material m1, Material m2) {
    return this.contactMaterialTable.get(m1.id, m2.id);
  }

  /**
   * Get number of objects in the world.
   * @method numObjects
   * @return {Number}
   * @deprecated
   */
  num numObjects() {
    return this.bodies.length;
  }

  /**
   * Store old collision state info
   * @method collisionMatrixTick
   */
  void collisionMatrixTick() {
    final temp = this.collisionMatrixPrevious;
    this.collisionMatrixPrevious = this.collisionMatrix;
    this.collisionMatrix = temp;
    this.collisionMatrix.reset();

    this.bodyOverlapKeeper.tick();
    this.shapeOverlapKeeper.tick();
  }

  /**
   * Add a constraint to the simulation.
   * @method addConstraint
   * @param {Constraint} c
   */
  addConstraint(Constraint c) {
    this.constraints.add(c);
  }

  /**
   * Removes a constraint
   * @method removeConstraint
   * @param {Constraint} c
   */
  removeConstraint(Constraint c) {
    final idx = this.constraints.indexOf(c);
    if (idx != -1) {
      this.constraints.removeRange(idx, 1);
    }
  }

  /**
   * Raycast test
   * @method rayTest
   * @param {Vec3} from
   * @param {Vec3} to
   * @param {RaycastResult} result
   * @deprecated Use .raycastAll, .raycastClosest or .raycastAny instead.
   */
  rayTest(Vec3 from, Vec3 to, dynamic result) {
    //ToDo RayOptions was {}
    RayOptions rayOptions = RayOptions();
    rayOptions.skipBackfaces = true;
    if (result is RaycastResult) {
      // Do raycastClosest
      this.raycastClosest(from, to, rayOptions, result);
    } else if (result is RaycastCallback) {
      // Do raycastAll
      this.raycastAll(from, to, rayOptions, result);
    }
  }

  /**
   * Ray cast against all bodies. The provided callback will be executed for each hit with a RaycastResult as single argument.
   * @method raycastAll
   * @param  {Vec3} from
   * @param  {Vec3} to
   * @param  {Object} options
   * @param  {number} [options.collisionFilterMask=-1]
   * @param  {number} [options.collisionFilterGroup=-1]
   * @param  {boolean} [options.skipBackfaces=false]
   * @param  {boolean} [options.checkCollisionResponse=true]
   * @param  {Function} callback
   * @return {boolean} True if any body was hit.
   */
  bool raycastAll(
      Vec3 from, Vec3 to, RayOptions options, RaycastCallback callback) {
    ///ToDO was {}, not RayOptions();
    options ??= RayOptions();
    options.mode = RAY_MODES.ALL;
    options.from = from;
    options.to = to;
    options.callback = callback;
    return tmpRay.intersectWorld(this, options);
  }

  /**
   * Ray cast, and stop at the first result. Note that the order is random - but the method is fast.
   * @method raycastAny
   * @param  {Vec3} from
   * @param  {Vec3} to
   * @param  {Object} options
   * @param  {number} [options.collisionFilterMask=-1]
   * @param  {number} [options.collisionFilterGroup=-1]
   * @param  {boolean} [options.skipBackfaces=false]
   * @param  {boolean} [options.checkCollisionResponse=true]
   * @param  {RaycastResult} result
   * @return {boolean} True if any body was hit.
   */
  bool raycastAny(
      Vec3 from, Vec3 to, RayOptions options, RaycastResult result) {
    options ??= RayOptions();
    options.mode = RAY_MODES.ANY;
    options.from = from;
    options.to = to;
    options.result = result;
    return tmpRay.intersectWorld(this, options);
  }

  /**
   * Ray cast, and return information of the closest hit.
   * @method raycastClosest
   * @param  {Vec3} from
   * @param  {Vec3} to
   * @param  {Object} options
   * @param  {number} [options.collisionFilterMask=-1]
   * @param  {number} [options.collisionFilterGroup=-1]
   * @param  {boolean} [options.skipBackfaces=false]
   * @param  {boolean} [options.checkCollisionResponse=true]
   * @param  {RaycastResult} result
   * @return {boolean} True if any body was hit.
   */
  bool raycastClosest(
      Vec3 from, Vec3 to, RayOptions options, RaycastResult result) {
    options.mode = RAY_MODES.CLOSEST;
    options.from = from;
    options.to = to;
    options.result = result;
    return tmpRay.intersectWorld(this, options);
  }

  /**
   * Add a rigid body to the simulation.
   * @method add
   * @param {Body} body
   * @todo If the simulation has not yet started, why recrete and copy arrays for each body? Accumulate in dynamic arrays in this case.
   * @todo Adding an array of bodies should be possible. This would save some loops too
   */
  addBody(Body body) {
    if (this.bodies.contains(body)) {
      return;
    }
    body.index = this.bodies.length;
    this.bodies.add(body);
    body.world = this;
    body.initPosition.copy(body.position);
    body.initVelocity.copy(body.velocity);
    body.timeLastSleepy = this.time;
    if (body is Body) {
      body.initAngularVelocity.copy(body.angularVelocity);
      body.initQuaternion.copy(body.quaternion);
    }
    this.collisionMatrix.setNumObjects(this.bodies.length);
    this.idToBodyMap[body.id] = body;
    this.worldStreamsController.add(EventContainer(
        WorldEvents.addBody, body)); //TODO change events to stream
  }

  /**
   * Remove a rigid body from the simulation.
   * @method remove
   * @param {Body} body
   */
  removeBody(Body body) {
    body.world = null;
    final n = this.bodies.length - 1;
    final bodies = this.bodies;
    final idx = bodies.indexOf(body);

    ///ToDo == and === are different. Check === and change to identical
    if (identical(idx, -1)) {
      bodies.removeRange(idx, 1); // Todo: should use a garbage free method
      // Recompute index
      for (int i = 0; i != bodies.length; i++) {
        bodies[i].index = i;
      }

      this.collisionMatrix.setNumObjects(n);

      /// Was below
      // delete this.idToBodyMap[body.id];
      this.idToBodyMap.remove(body.id);
      this.dispatchEvent(EventContainer(WorldEvents.removeBody, body));
    }
  }

  Body getBodyById(num id) {
    return this.idToBodyMap[id];
  }

// TODO Make a faster map
  getShapeById(num id) {
    final bodies = this.bodies;
    for (int i = 0, bl = bodies.length; i < bl; i++) {
      final shapes = bodies[i].shapes;
      for (int j = 0, sl = shapes.length; j < sl; j++) {
        final shape = shapes[j];
        if (identical(shape.id, id)) {
          return shape;
        }
      }
    }
  }

  /**
   * Adds a material to the World.
   * @method addMaterial
   * @param {Material} m
   * @todo Necessary?
   */
  addMaterial(Material m) {
    this.materials.add(m);
  }

  /**
   * Adds a contact material to the World
   * @method addContactMaterial
   * @param {ContactMaterial} cmat
   */
  addContactMaterial(ContactMaterial cmat) {
    // Add contact material
    this.contactmaterials.add(cmat);

    // Add current contact material to the material table
    this
        .contactMaterialTable
        .set(cmat.materials[0].id, cmat.materials[1].id, cmat);
  }

  /**
   * Step the physics world forward in time.
   *
   * There are two modes. The simple mode is fixed timestepping without interpolation. In this case you only use the first argument. The second case uses interpolation. In that you also provide the time since the function was last used, as well as the maximum fixed timesteps to take.
   *
   * @method step
   * @param {Number} dt                       The fixed time step size to use.
   * @param {Number} [timeSinceLastCalled]    The time elapsed since the function was last called.
   * @param {Number} [maxSubSteps=10]         Maximum number of fixed steps to take per function call.
   *
   * @example
   *     // fixed timestepping without interpolation
   *     world.step(1/60);
   *
   * @see http://bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_The_World
   */
  step(num dt, num timeSinceLastCalled, maxSubSteps) {
    maxSubSteps ??= 10;
    if (timeSinceLastCalled == null) {
      // Fixed, simple stepping

      this.internalStep(dt);

      // Increment time
      this.time += dt;
    } else {
      this.accumulator += timeSinceLastCalled;

      final t0 = performance.now();
      var substeps = 0;
      while (this.accumulator >= dt && substeps < maxSubSteps) {
        // Do fixed steps to catch up
        this.internalStep(dt);
        this.accumulator -= dt;
        substeps++;
        if (performance.now() - t0 > dt * 2 * 1000) {
          // The framerate is not interactive anymore.
          // We are at half of the target framerate.
          // Better bail out.
          break;
        }
      }

      // Remove the excess accumulator, since we may not
      // have had enough substeps available to catch up
      this.accumulator = this.accumulator % dt;

      final t = this.accumulator / dt;
      for (int j = 0; j != this.bodies.length; j++) {
        final b = this.bodies[j];
        b.previousPosition.lerp(b.position, t, b.interpolatedPosition);
        b.previousQuaternion.slerp(b.quaternion, t, b.interpolatedQuaternion);
        b.previousQuaternion.normalize();
      }
      this.time += timeSinceLastCalled;
    }
  }

  internalStep(num dt) {
    this.dt = dt;

    final wfinalorld = this;
    final that = this;
    final contacts = this.contacts;
    final p1 = World_step_p1;
    final p2 = World_step_p2;
    final N = this.numObjects();
    final bodies = this.bodies;
    final solver = this.solver;
    final gravity = this.gravity;
    final doProfiling = this.doProfiling;
    final profile = this.profile;
    var profilingStart = -double.infinity;
    final constraints = this.constraints;
    final frictionEquationPool = World_step_frictionEquationPool;
    final gnorm = gravity.length();
    final gx = gravity.x;
    final gy = gravity.y;
    final gz = gravity.z;
    var i = 0;

    if (doProfiling) {
      profilingStart = performance.now();
    }

    // Add gravity to all objects
    for (i = 0; i != N; i++) {
      final bi = bodies[i];
      if (bi.type == BODY_TYPES.DYNAMIC) {
        // Only for dynamic bodies
        final f = bi.force;

        final m = bi.mass;
        f.x += m * gx;
        f.y += m * gy;
        f.z += m * gz;
      }
    }

    // Update subsystems
    for (int i = 0, Nsubsystems = this.subsystems.length;
        i != Nsubsystems;
        i++) {
      this.subsystems[i].update();
    }

    // Collision detection
    if (doProfiling) {
      profilingStart = performance.now();
    }
    p1.length = 0; // Clean up pair arrays from last step
    p2.length = 0;
    this.broadphase.collisionPairs(this, p1, p2);
    if (doProfiling) {
      profile.broadphase = performance.now() - profilingStart;
    }

    // Remove constrained pairs with collideConnected == false
    var Nconstraints = constraints.length;
    for (i = 0; i != Nconstraints; i++) {
      final c = constraints[i];
      if (!c.collideConnected) {
        for (int j = p1.length - 1; j >= 0; j -= 1) {
          if ((c.bodyA == p1[j] && c.bodyB == p2[j]) ||
              (c.bodyB == p1[j] && c.bodyA == p2[j])) {
            p1.removeRange(j, 1);
            p2.removeRange(j, 1);
          }
        }
      }
    }

    this.collisionMatrixTick();

    // Generate contacts
    if (doProfiling) {
      profilingStart = performance.now();
    }
    final oldcontacts = World_step_oldContacts;
    final NoldContacts = contacts.length;

    for (i = 0; i != NoldContacts; i++) {
      oldcontacts.add(contacts[i]);
    }
    contacts.length = 0;

    // Transfer FrictionEquation from current list to the pool for reuse
    final NoldFrictionEquations = this.frictionEquations.length;
    for (i = 0; i != NoldFrictionEquations; i++) {
      frictionEquationPool.add(this.frictionEquations[i]);
    }
    this.frictionEquations.length = 0;

    this.narrowphase.getContacts(
        p1,
        p2,
        this,
        contacts,
        oldcontacts,
        // To be reused
        this.frictionEquations,
        frictionEquationPool);

    if (doProfiling) {
      profile.narrowphase = performance.now() - profilingStart;
    }

    // Loop over all collisions
    if (doProfiling) {
      profilingStart = performance.now();
    }

    // Add all friction eqs
    for (i = 0; i < this.frictionEquations.length; i++) {
      solver.addEquation(this.frictionEquations[i]);
    }

    final ncontacts = contacts.length;
    for (int k = 0; k != ncontacts; k++) {
      // Current contact
      final c = contacts[k];

      // Get current collision indeces
      final bi = c.bi;

      final bj = c.bj;
      final si = c.si;
      final sj = c.sj;

      // Get collision properties
      var cm;
      if (bi.material && bj.material) {
        ContactMaterial contactMaterial =
            this.getContactMaterial(bi.material, bj.material);
        if (contactMaterial != null &&
            contactMaterial != false &&
            contactMaterial != 0) {
          cm = this.getContactMaterial(bi.material, bj.material);
        } else {
          cm = this.defaultContactMaterial;
        }
      } else {
        cm = this.defaultContactMaterial;
      }

      // c.enabled = bi.collisionResponse && bj.collisionResponse && si.collisionResponse && sj.collisionResponse;

      var mu = cm.friction;
      // c.restitution = cm.restitution;

      // If friction or restitution were specified in the material, use them
      if (bi.material && bj.material) {
        if (bi.material.friction >= 0 && bj.material.friction >= 0) {
          mu = bi.material.friction * bj.material.friction;
        }

        if (bi.material.restitution >= 0 && bj.material.restitution >= 0) {
          c.restitution = bi.material.restitution * bj.material.restitution;
        }
      }

      // c.setSpookParams(
      //           cm.contactEquationStiffness,
      //           cm.contactEquationRelaxation,
      //           dt
      //       );

      solver.addEquation(c);

      // // Add friction constraint equation
      // if(mu > 0){

      // 	// Create 2 tangent equations
      // 	const mug = mu * gnorm;
      // 	const reducedMass = (bi.invMass + bj.invMass);
      // 	if(reducedMass > 0){
      // 		reducedMass = 1/reducedMass;
      // 	}
      // 	const pool = frictionEquationPool;
      // 	const c1 = pool.length ? pool.pop() : new FrictionEquation(bi,bj,mug*reducedMass);
      // 	const c2 = pool.length ? pool.pop() : new FrictionEquation(bi,bj,mug*reducedMass);
      // 	this.frictionEquations.push(c1, c2);

      // 	c1.bi = c2.bi = bi;
      // 	c1.bj = c2.bj = bj;
      // 	c1.minForce = c2.minForce = -mug*reducedMass;
      // 	c1.maxForce = c2.maxForce = mug*reducedMass;

      // 	// Copy over the relative vectors
      // 	c1.ri.copy(c.ri);
      // 	c1.rj.copy(c.rj);
      // 	c2.ri.copy(c.ri);
      // 	c2.rj.copy(c.rj);

      // 	// Construct tangents
      // 	c.ni.tangents(c1.t, c2.t);

      //           // Set spook params
      //           c1.setSpookParams(cm.frictionEquationStiffness, cm.frictionEquationRelaxation, dt);
      //           c2.setSpookParams(cm.frictionEquationStiffness, cm.frictionEquationRelaxation, dt);

      //           c1.enabled = c2.enabled = c.enabled;

      // 	// Add equations to solver
      // 	solver.addEquation(c1);
      // 	solver.addEquation(c2);
      // }

      if (bi.allowSleep &&
          bi.type == BODY_TYPES.DYNAMIC &&
          bi.sleepState == BODY_SLEEP_STATES.SLEEPING &&
          bj.sleepState == BODY_SLEEP_STATES.AWAKE &&
          bj.type != BODY_TYPES.STATIC) {
        final speedSquaredB =
            bj.velocity.lengthSquared() + bj.angularVelocity.lengthSquared();
        final speedLimitSquaredB = Math.pow(bj.sleepSpeedLimit, 2);

        if (speedSquaredB >= speedLimitSquaredB * 2) {
          bi.wakeUpAfterNarrowphase = true;
        }
      }

      if (bj.allowSleep &&
          bj.type == BODY_TYPES.DYNAMIC &&
          bj.sleepState == BODY_SLEEP_STATES.SLEEPING &&
          bi.sleepState == BODY_SLEEP_STATES.AWAKE &&
          bi.type != BODY_TYPES.STATIC) {
        final speedSquaredA =
            bi.velocity.lengthSquared() + bi.angularVelocity.lengthSquared();
        final speedLimitSquaredA = Math.pow(bi.sleepSpeedLimit, 2);
        if (speedSquaredA >= speedLimitSquaredA * 2) {
          bj.wakeUpAfterNarrowphase = true;
        }
      }

      // Now we know that i and j are in contact. Set collision matrix state
      this.collisionMatrix.set(bi, bj, true);

      if (this.collisionMatrixPrevious.get(bi, bj) != false) {
        // First contact!
        // We reuse the collideEvent object, otherwise we will end up creating new objects for each new contact, even if there's no event listener attached.
        WorldStepCollide worldStepCollide = WorldStepCollide(bj, c);
        this.dispatchEvent(
            EventContainer(BodyEvents.collide, worldStepCollide));

        this.dispatchEvent(EventContainer(
            BodyEvents.collide, worldStepCollide.copyWith(body: bi)));
      }

      this.bodyOverlapKeeper.set(bi.id, bj.id);
      this.shapeOverlapKeeper.set(si.id, sj.id);
    }

    this.emitContactEvents.emitContactEvents();

    if (doProfiling) {
      profile.makeContactConstraints = performance.now() - profilingStart;
      profilingStart = performance.now();
    }

    // Wake up bodies
    for (i = 0; i != N; i++) {
      final bi = bodies[i];
      if (bi.wakeUpAfterNarrowphase) {
        bi.wakeUp();
        bi.wakeUpAfterNarrowphase = false;
      }
    }

    // Add user-added constraints
    Nconstraints = constraints.length;
    for (i = 0; i != Nconstraints; i++) {
      final c = constraints[i];
      c.update();
      for (int j = 0, Neq = c.equations.length; j != Neq; j++) {
        final eq = c.equations[j];
        solver.addEquation(eq);
      }
    }

    // Solve the constrained system
    solver.solve(dt, this);

    if (doProfiling) {
      profile.solve = performance.now() - profilingStart;
    }

    // Remove all contacts from solver
    solver.removeAllEquations();

    // Apply damping, see http://code.google.com/p/bullet/issues/detail?id=74 for details
    final pow = Math.pow;
    for (i = 0; i != N; i++) {
      final bi = bodies[i];
      if (bi.type == BODY_TYPES.DYNAMIC) {
        // Only for dynamic bodies
        final ld = pow(1.0 - bi.linearDamping, dt);
        final v = bi.velocity;
        v.scale(ld, v);
        final av = bi.angularVelocity;
        if (av != null) {
          final ad = pow(1.0 - bi.angularDamping, dt);
          av.scale(ad, av);
        }
      }
    }

    this.dispatchEvent(EventContainer(WorldEvents.preStep, null));

    // Invoke pre-step callbacks
    for (i = 0; i != N; i++) {
      final bi = bodies[i];
      if (bi != null) {
        this.dispatchEvent(EventContainer(WorldEvents.preStep, bi));
      }
    }

    // Leap frog
    // vnew = v + h*f/m
    // xnew = x + h*vnew
    if (doProfiling) {
      profilingStart = performance.now();
    }
    final stepnumber = this.stepnumber;
    final quatNormalize = stepnumber % (this.quatNormalizeSkip + 1) == 0;
    final quatNormalizeFast = this.quatNormalizeFast;

    for (i = 0; i != N; i++) {
      bodies[i].integrate(dt, quatNormalize, quatNormalizeFast);
    }
    this.clearForces();

    this.broadphase.dirty = true;

    if (doProfiling) {
      profile.integrate = performance.now() - profilingStart;
    }

    // Update world time
    this.time += dt;
    this.stepnumber += 1;

    this.dispatchEvent(EventContainer(WorldEvents.postStep));

    // Invoke post-step callbacks
    for (i = 0; i != N; i++) {
      final bi = bodies[i];
      if (bi != null) {
        this.dispatchEvent(EventContainer(WorldEvents.postStep, bi));
      }
    }

    // Sleeping update
    var hasActiveBodies = true;
    if (this.allowSleep) {
      hasActiveBodies = false;
      for (i = 0; i != N; i++) {
        final bi = bodies[i];
        bi.sleepTick(this.time);

        if (bi.sleepState != BODY_SLEEP_STATES.SLEEPING) {
          hasActiveBodies = true;
        }
      }
    }
    this.hasActiveBodies = hasActiveBodies;
  }

  /**
   * Sets all body forces in the world to zero.
   * @method clearForces
   */
  clearForces() {
    final bodies = this.bodies;
    final N = bodies.length;
    for (int i = 0; i != N; i++) {
      final b = bodies[i];
      final force = b.force;
      final tau = b.torque;
      b.force.set(0, 0, 0);
      b.torque.set(0, 0, 0);
    }
  }
}

// Temp stuff
final tmpAABB1 = new AABB();

final tmpArray1 = [];

final tmpRay = new Ray();

//ToDo

final performance = Performance();

//TODO Was below
// if (!performance.now) {
// var nowOffset = Date.now();
// if (performance.timing && performance.timing.navigationStart) {
// nowOffset = performance.timing.navigationStart;
// }
// performance.now = () => Date.now() - nowOffset;
// }

// performance.now() fallback on Date.now()
// final performance = (globalThis.performance || {}) as Performance;
// if(globalThis.performance != null && globalThis.performance != 0 && globalThis.performance != false) {

final step_tmp1 = new Vec3();

// Dispatched after the world has stepped forward in time.
// Reusable event objects to save memory.
final World_step_postStepEvent = {"type": 'postStep'};

// Dispatched before the world steps forward in time.
final World_step_preStepEvent = {"type": 'preStep'};

// final World_step_collideEvent: {
// type: typeof Body.COLLIDE_EVENT_NAME
// body: Body | null
// contact: ContactEquation | null
// } = { type: Body.COLLIDE_EVENT_NAME, body: null, contact: null }

// final Map<String, dynamic> World_step_collideEvent = {
//   "type": Body.COLLIDE_EVENT_NAME,
//   "body": null,
//   "contact": null
// };
class WorldStepCollide {
  final Body body;
  final ContactEquation contact;

  WorldStepCollide(this.body, this.contact);

  copyWith({Body body, ContactEquation contact}) =>
      WorldStepCollide(body ?? this.body, contact ?? this.contact);
}

// Pools for unused objects
final List<ContactEquation> World_step_oldContacts = [];
final List<FrictionEquation> World_step_frictionEquationPool = [];

// Reusable arrays for collision pairs
final List<Body> World_step_p1 = [];
final List<Body> World_step_p2 = [];

class Performance {
  double now() {
    return DateTime.now().millisecondsSinceEpoch.toDouble();
  }
}

class Contact {
  final Body bodyA, bodyB;

  Contact(this.bodyA, this.bodyB);
}

class ShapeContact extends Contact {
  final Shape shapeA, shapeB;

  ShapeContact(Body bodyA, Body bodyB, this.shapeA, this.shapeB)
      : super(bodyA, bodyB);
}

enum ContactEvents {
  beginContact,
  endContact,
  beginShapeContact,
  endShapeContact
}

//TODO check below
// var emitContactEvents = emitContactEvents();
class EmitContactEventsCallback {
  final List<num> additions = [];
  final List<num> removals = [];

  final World world;

  EmitContactEventsCallback(this.world);

  void emitContactEvents() {
    final hasBeginContact =
        world.hasAnyEventListener(ContactEvents.beginContact);
    final hasEndContact = world.hasAnyEventListener(ContactEvents.endContact);

    if (hasBeginContact || hasEndContact) {
      world.bodyOverlapKeeper.getDiff(additions, removals);
    }
    if (hasBeginContact) {
      for (int i = 0, l = additions.length; i < l; i += 2) {
        Contact beginContactEvent = Contact(world.getBodyById(additions[i]),
            world.getBodyById(additions[i + 1]));

        world.dispatchEvent(
            EventContainer(ContactEvents.beginContact, beginContactEvent));
      }
    }

    if (hasEndContact) {
      for (int i = 0, l = removals.length; i < l; i += 2) {
        Contact endContactEvent = Contact(
            world.getBodyById(removals[i]), world.getBodyById(removals[i + 1]));
        world.dispatchEvent(
            EventContainer(ContactEvents.endContact, endContactEvent));
      }
    }

    additions.length = removals.length = 0;

    final hasBeginShapeContact =
        world.hasAnyEventListener(ContactEvents.beginShapeContact);
    final hasEndShapeContact =
        world.hasAnyEventListener(ContactEvents.endShapeContact);

    if ((hasBeginShapeContact == true) || hasEndShapeContact) {
      world.shapeOverlapKeeper.getDiff(additions, removals);
    }

    if (hasBeginShapeContact) {
      for (int i = 0, l = additions.length; i < l; i += 2) {
        final shapeA = world.getShapeById(additions[i]);
        final shapeB = world.getShapeById(additions[i + 1]);
        ShapeContact beginShapeContactEvent =
            ShapeContact(shapeA.body, shapeB.body, shapeA, shapeB);
        world.dispatchEvent(EventContainer(
            ContactEvents.beginShapeContact, beginShapeContactEvent));
      }
    }

    if (hasEndShapeContact) {
      for (int i = 0, l = removals.length; i < l; i += 2) {
        final shapeA = world.getShapeById(removals[i]);
        final shapeB = world.getShapeById(removals[i + 1]);
        ShapeContact endShapeContactEvent =
            ShapeContact(shapeA.body, shapeB.body, shapeA, shapeB);
        world.dispatchEvent(EventContainer(
            ContactEvents.endShapeContact, endShapeContactEvent));
      }
    }
  }
}
