import 'dart:math' as Math;

import "../collision/Ray.dart" show Ray;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;

/**
 * Axis aligned bounding box class.
 * @class AABB
 * @constructor
 *
 *
 *
 */
class AABB {
  Vec3 lowerBound;

  Vec3 upperBound;

  AABB({this.lowerBound, this.upperBound}) {
    this.lowerBound = new Vec3();
    this.upperBound = new Vec3();
    if (lowerBound != null) {
      this.lowerBound.copy(lowerBound);
    }
    if (upperBound != null) {
      this.upperBound.copy(upperBound);
    }
  }

  /**
   * Set the AABB bounds from a set of points.
   * @method setFromPoints
   *
   *
   *
   *
   *
   */
  AABB setFromPoints(List<Vec3> points,
      [Vec3 position, Quaternion quaternion, num skinSize]) {
    final l = this.lowerBound;
    final u = this.upperBound;
    final q = quaternion;
    // Set to the first point
    l.copy(points[0]);
    if (q != null) {
      q.vmult(l, l);
    }
    u.copy(l);
    for (var i = 1; i < points.length; i++) {
      var p = points[i];
      if (q != null) {
        q.vmult(p, tmp);
        p = tmp;
      }
      if (p.x > u.x) {
        u.x = p.x;
      }
      if (p.x < l.x) {
        l.x = p.x;
      }
      if (p.y > u.y) {
        u.y = p.y;
      }
      if (p.y < l.y) {
        l.y = p.y;
      }
      if (p.z > u.z) {
        u.z = p.z;
      }
      if (p.z < l.z) {
        l.z = p.z;
      }
    }
    // Add offset
    if (position != null) {
      position.vadd(l, l);
      position.vadd(u, u);
    }
    if (skinSize != null) {
      l.x -= skinSize;
      l.y -= skinSize;
      l.z -= skinSize;
      u.x += skinSize;
      u.y += skinSize;
      u.z += skinSize;
    }
    return this;
  }

  /**
   * Copy bounds from an AABB to this AABB
   * @method copy
   *
   *
   */
  AABB copy(AABB aabb) {
    this.lowerBound.copy(aabb.lowerBound);
    this.upperBound.copy(aabb.upperBound);
    return this;
  }

  /**
   * Clone an AABB
   * @method clone
   */
  AABB clone() {
    return new AABB().copy(this);
  }

  /**
   * Extend this AABB so that it covers the given AABB too.
   * @method extend
   *
   */
  void extend(AABB aabb) {
    this.lowerBound.x = Math.min(this.lowerBound.x, aabb.lowerBound.x);
    this.upperBound.x = Math.max(this.upperBound.x, aabb.upperBound.x);
    this.lowerBound.y = Math.min(this.lowerBound.y, aabb.lowerBound.y);
    this.upperBound.y = Math.max(this.upperBound.y, aabb.upperBound.y);
    this.lowerBound.z = Math.min(this.lowerBound.z, aabb.lowerBound.z);
    this.upperBound.z = Math.max(this.upperBound.z, aabb.upperBound.z);
  }

  /**
   * Returns true if the given AABB overlaps this AABB.
   * @method overlaps
   *
   *
   */
  bool overlaps(AABB aabb) {
    final l1 = this.lowerBound;
    final u1 = this.upperBound;
    final l2 = aabb.lowerBound;
    final u2 = aabb.upperBound;
    //      l2        u2

    //      |---------|

    // |--------|

    // l1       u1
    final overlapsX =
        (l2.x <= u1.x && u1.x <= u2.x) || (l1.x <= u2.x && u2.x <= u1.x);
    final overlapsY =
        (l2.y <= u1.y && u1.y <= u2.y) || (l1.y <= u2.y && u2.y <= u1.y);
    final overlapsZ =
        (l2.z <= u1.z && u1.z <= u2.z) || (l1.z <= u2.z && u2.z <= u1.z);
    return overlapsX && overlapsY && overlapsZ;
  }

  // Mostly for debugging
  num volume() {
    final l = this.lowerBound;
    final u = this.upperBound;
    return (u.x - l.x) * (u.y - l.y) * (u.z - l.z);
  }

  /**
   * Returns true if the given AABB is fully contained in this AABB.
   * @method contains
   *
   *
   */
  bool contains(AABB aabb) {
    final l1 = this.lowerBound;
    final u1 = this.upperBound;
    final l2 = aabb.lowerBound;
    final u2 = aabb.upperBound;
    //      l2        u2

    //      |---------|

    // |---------------|

    // l1              u1
    return l1.x <= l2.x &&
        u1.x >= u2.x &&
        l1.y <= l2.y &&
        u1.y >= u2.y &&
        l1.z <= l2.z &&
        u1.z >= u2.z;
  }

  /**
   * @method getCorners
   *
   *
   *
   *
   *
   *
   *
   *
   */
  void getCorners(
      Vec3 a, Vec3 b, Vec3 c, Vec3 d, Vec3 e, Vec3 f, Vec3 g, Vec3 h) {
    final l = this.lowerBound;
    final u = this.upperBound;
    a.copy(l);
    b.set(u.x, l.y, l.z);
    c.set(u.x, u.y, l.z);
    d.set(l.x, u.y, u.z);
    e.set(u.x, l.y, u.z);
    f.set(l.x, u.y, l.z);
    g.set(l.x, l.y, u.z);
    h.copy(u);
  }

  /**
   * Get the representation of an AABB in another frame.
   * @method toLocalFrame
   *
   *
   *
   */
  AABB toLocalFrame(Transform frame, AABB target) {
    final corners = transformIntoFrame_corners;
    final a = corners[0];
    final b = corners[1];
    final c = corners[2];
    final d = corners[3];
    final e = corners[4];
    final f = corners[5];
    final g = corners[6];
    final h = corners[7];
    // Get corners in current frame
    this.getCorners(a, b, c, d, e, f, g, h);
    // Transform them to new local frame
    for (var i = 0; !identical(i, 8); i++) {
      final corner = corners[i];
      frame.pointToLocal(corner, corner);
    }
    return target.setFromPoints(corners);
  }

  /**
   * Get the representation of an AABB in the global frame.
   * @method toWorldFrame
   *
   *
   *
   */
  AABB toWorldFrame(Transform frame, AABB target) {
    final corners = transformIntoFrame_corners;
    final a = corners[0];
    final b = corners[1];
    final c = corners[2];
    final d = corners[3];
    final e = corners[4];
    final f = corners[5];
    final g = corners[6];
    final h = corners[7];
    // Get corners in current frame
    this.getCorners(a, b, c, d, e, f, g, h);
    // Transform them to new local frame
    for (var i = 0; !identical(i, 8); i++) {
      final corner = corners[i];
      frame.pointToWorld(corner, corner);
    }
    return target.setFromPoints(corners);
  }

  /**
   * Check if the AABB is hit by a ray.
   *
   *
   */
  bool overlapsRay(Ray ray) {
    final direction = ray.direction, from = ray.from;
    const t = 0;
    // ray.direction is unit direction vector of ray
    final dirFracX = 1 / direction.x;
    final dirFracY = 1 / direction.y;
    final dirFracZ = 1 / direction.z;
    // this.lowerBound is the corner of AABB with minimal coordinates - left bottom, rt is maximal corner
    final t1 = (this.lowerBound.x - from.x) * dirFracX;
    final t2 = (this.upperBound.x - from.x) * dirFracX;
    final t3 = (this.lowerBound.y - from.y) * dirFracY;
    final t4 = (this.upperBound.y - from.y) * dirFracY;
    final t5 = (this.lowerBound.z - from.z) * dirFracZ;
    final t6 = (this.upperBound.z - from.z) * dirFracZ;
    // const tmin = Math.max(Math.max(Math.min(t1, t2), Math.min(t3, t4)));

    // const tmax = Math.min(Math.min(Math.max(t1, t2), Math.max(t3, t4)));
    final tmin = Math.max(
        Math.max(Math.min(t1, t2), Math.min(t3, t4)), Math.min(t5, t6));
    final tmax = Math.min(
        Math.min(Math.max(t1, t2), Math.max(t3, t4)), Math.max(t5, t6));
    // if tmax < 0, ray (line) is intersecting AABB, but whole AABB is behing us
    if (tmax < 0) {
      //t = tmax;
      return false;
    }
    // if tmin > tmax, ray doesn't intersect AABB
    if (tmin > tmax) {
      //t = tmax;
      return false;
    }
    return true;
  }
}

final tmp = new Vec3();

final transformIntoFrame_corners = [
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3()
];
