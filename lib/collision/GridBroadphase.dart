import 'dart:math';

import '../collision/Broadphase.dart' show Broadphase;
import '../math/Vec3.dart' show Vec3;
import '../objects/Body.dart' show Body;
import '../shapes/Plane.dart' show Plane;
import '../shapes/Shape.dart' show Shape, SHAPE_TYPES;
import '../shapes/Sphere.dart' show Sphere;
import '../world/World.dart' show World;

/// Axis aligned uniform grid broadphase.
/// @class GridBroadphase
/// @constructor
/// @extends Broadphase
/// @todo Needs support for more than just planes and spheres.
class GridBroadphase extends Broadphase {
  num nx;

  num ny;

  num nz;

  Vec3 aabbMin;

  Vec3 aabbMax;

  List<List<Body>> bins;

  List<num> binLengths;

  GridBroadphase([aabbMin, aabbMax, nx = 10, ny = 10, nz = 10]) : super() {
    aabbMin ??= Vec3(100, 100, 100);
    aabbMax ??= Vec3(-100, -100, -100);
    /* super call moved to initializer */;
    this.nx = nx;
    this.ny = ny;
    this.nz = nz;
    this.aabbMin = aabbMin;
    this.aabbMax = aabbMax;
    final nbins = this.nx * this.ny * this.nz;
    if (nbins <= 0) {
      throw "GridBroadphase: Each dimension's n must be >0";
    }
    this.bins = [];
    this.binLengths = [];
    this.bins.length = nbins;
    this.binLengths.length = nbins;
    for (var i = 0; i < nbins; i++) {
      this.bins[i] = [];
      this.binLengths[i] = 0;
    }
  }

  void collisionPairs(World world, List<Body> pairs1, List<Body> pairs2) {
    final N = world.numObjects();
    final bodies = world.bodies;
    final max = this.aabbMax;
    final min = this.aabbMin;
    final nx = this.nx;
    final ny = this.ny;
    final nz = this.nz;
    final xstep = ny * nz;
    final ystep = nz;
    const zstep = 1;
    final xmax = max.x;
    final ymax = max.y;
    final zmax = max.z;
    final xmin = min.x;
    final ymin = min.y;
    final zmin = min.z;
    final xmult = nx / (xmax - xmin);
    final ymult = ny / (ymax - ymin);
    final zmult = nz / (zmax - zmin);
    final binsizeX = (xmax - xmin) / nx;
    final binsizeY = (ymax - ymin) / ny;
    final binsizeZ = (zmax - zmin) / nz;
    final binRadius =
        sqrt(binsizeX * binsizeX + binsizeY * binsizeY + binsizeZ * binsizeZ) *
            0.5;
    final bins = this.bins;
    final binLengths = this.binLengths;
    final Nbins = this.bins.length;
    // Reset bins
    for (var i = 0; !identical(i, Nbins); i++) {
      binLengths[i] = 0;
    }
    void addBoxToBins(num x0, num y0, num z0, num x1, num y1, num z1, Body bi) {
      var xoff0 = (((x0 - xmin) * xmult) as int) | 0;
      var yoff0 = (((y0 - ymin) * ymult) as int) | 0;
      var zoff0 = (((z0 - zmin) * zmult) as int) | 0;
      var xoff1 = ((x1 - xmin) * xmult).ceil();
      var yoff1 = ((y1 - ymin) * ymult).ceil();
      var zoff1 = ((z1 - zmin) * zmult).ceil();
      if (xoff0 < 0) {
        xoff0 = 0;
      } else if (xoff0 >= nx) {
        xoff0 = nx - 1;
      }
      if (yoff0 < 0) {
        yoff0 = 0;
      } else if (yoff0 >= ny) {
        yoff0 = ny - 1;
      }
      if (zoff0 < 0) {
        zoff0 = 0;
      } else if (zoff0 >= nz) {
        zoff0 = nz - 1;
      }
      if (xoff1 < 0) {
        xoff1 = 0;
      } else if (xoff1 >= nx) {
        xoff1 = nx - 1;
      }
      if (yoff1 < 0) {
        yoff1 = 0;
      } else if (yoff1 >= ny) {
        yoff1 = ny - 1;
      }
      if (zoff1 < 0) {
        zoff1 = 0;
      } else if (zoff1 >= nz) {
        zoff1 = nz - 1;
      }
      xoff0 *= xstep;
      yoff0 *= ystep;
      zoff0 *= zstep;
      xoff1 *= xstep;
      yoff1 *= ystep;
      zoff1 *= zstep;
      for (var xoff = xoff0; xoff <= xoff1; xoff += xstep) {
        for (var yoff = yoff0; yoff <= yoff1; yoff += ystep) {
          for (var zoff = zoff0; zoff <= zoff1; zoff += zstep) {
            final idx = xoff + yoff + zoff;
            bins[idx][binLengths[idx]++] = bi;
          }
        }
      }
    }

    // Put all bodies into the bins
    for (var i = 0; !identical(i, N); i++) {
      final bi = bodies[i];
      final si = bi.shapes[0];
      switch (si.type) {
        case SHAPE_TYPES.SPHERE:
          {
            final shape = si as Sphere;
            // Put in bin

            // check if overlap with other bins
            final x = bi.position.x;
            final y = bi.position.y;
            final z = bi.position.z;
            final r = shape.radius;
            addBoxToBins(x - r, y - r, z - r, x + r, y + r, z + r, bi);
            break;
          }
        case SHAPE_TYPES.PLANE:
          {
            final shape = si as Plane;
            if (shape.worldNormalNeedsUpdate) {
              shape.computeWorldNormal(bi.quaternion);
            }
            final planeNormal = shape.worldNormal;
            //Relative position from origin of plane object to the first bin

            //Incremented as we iterate through the bins
            final xreset = xmin + binsizeX * 0.5 - bi.position.x;
            final yreset = ymin + binsizeY * 0.5 - bi.position.y;
            final zreset = zmin + binsizeZ * 0.5 - bi.position.z;
            final d = GridBroadphase_collisionPairs_d;
            d.set(xreset, yreset, zreset);
            for (var xi = 0, xoff = 0;
                !identical(xi, nx);
                xi++, xoff += xstep, d.y = yreset, d.x += binsizeX) {
              for (var yi = 0, yoff = 0;
                  !identical(yi, ny);
                  yi++, yoff += ystep, d.z = zreset, d.y += binsizeY) {
                for (var zi = 0, zoff = 0;
                    !identical(zi, nz);
                    zi++, zoff += zstep, d.z += binsizeZ) {
                  if (d.dot(planeNormal) < binRadius) {
                    final idx = xoff + yoff + zoff;
                    bins[idx][binLengths[idx]++] = bi;
                  }
                }
              }
            }
            break;
          }
        default:
          {
            if (bi.aabbNeedsUpdate) {
              bi.computeAABB();
            }
            addBoxToBins(
                bi.aabb.lowerBound.x,
                bi.aabb.lowerBound.y,
                bi.aabb.lowerBound.z,
                bi.aabb.upperBound.x,
                bi.aabb.upperBound.y,
                bi.aabb.upperBound.z,
                bi);
            break;
          }
      }
    }
    // Check each bin
    for (var i = 0; !identical(i, Nbins); i++) {
      final binLength = binLengths[i];
      //Skip bins with no potential collisions
      if (binLength > 1) {
        final bin = bins[i];
        // Do N^2 broadphase inside
        for (var xi = 0; !identical(xi, binLength); xi++) {
          final bi = bin[xi];
          for (var yi = 0; !identical(yi, xi); yi++) {
            final bj = bin[yi];
            if (this.needBroadphaseCollision(bi, bj)) {
              this.intersectionTest(bi, bj, pairs1, pairs2);
            }
          }
        }
      }
    }
    //	for (let zi = 0, zoff=0; zi < nz; zi++, zoff+= zstep) {

    //		console.log("layer "+zi);

    //		for (let yi = 0, yoff=0; yi < ny; yi++, yoff += ystep) {

    //			const row = '';

    //			for (let xi = 0, xoff=0; xi < nx; xi++, xoff += xstep) {

    //				const idx = xoff + yoff + zoff;

    //				row += ' ' + binLengths[idx];

    //			}

    //			console.log(row);

    //		}

    //	}
    this.makePairsUnique(pairs1, pairs2);
  }
}

/**
 * Get all the collision pairs in the physics world
 * @method collisionPairs
 *
 *
 *
 */
final GridBroadphase_collisionPairs_d = new Vec3();

final GridBroadphase_collisionPairs_binPos = new Vec3();
