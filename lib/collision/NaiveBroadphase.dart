import "../collision/Broadphase.dart" show Broadphase;
import "../collision/AABB.dart" show AABB;
import "../objects/Body.dart" show Body;
import "../world/World.dart" show World;

/**
 * Naive broadphase implementation, used in lack of better ones.
 * @class NaiveBroadphase
 * @constructor
 *  The naive broadphase looks at all possible pairs without restriction, therefore it has complexity N^2 (which is bad)
 * @extends Broadphase
 */
class NaiveBroadphase extends Broadphase {
  NaiveBroadphase() : super() {
    /* super call moved to initializer */;
  }
  /**
   * Get all the collision pairs in the physics world
   * @method collisionPairs
   * 
   * 
   * 
   */
  void collisionPairs(World world, List<Body> pairs1, List<Body> pairs2) {
    final bodies = world.bodies;
    final n = bodies.length;
    var bi;
    var bj;
    // Naive N^2 ftw!
    for (var i = 0; !identical(i, n); i++) {
      for (var j = 0; !identical(j, i); j++) {
        bi = bodies[i];
        bj = bodies[j];
        if (!this.needBroadphaseCollision(bi, bj)) {
          continue;
        }
        this.intersectionTest(bi, bj, pairs1, pairs2);
      }
    }
  }

  /**
   * Returns all the bodies within an AABB.
   * @method aabbQuery
   * 
   * 
   * 
   * 
   */
  List<Body> aabbQuery(World world, AABB aabb, [List<Body> result = const []]) {
    for (var i = 0; i < world.bodies.length; i++) {
      final b = world.bodies[i];
      if (b.aabbNeedsUpdate) {
        b.computeAABB();
      }
      // Ugly hack until Body gets aabb
      if (b.aabb.overlaps(aabb)) {
        result.add(b);
      }
    }
    return result;
  }
}
