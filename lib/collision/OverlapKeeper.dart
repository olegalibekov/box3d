/**
 * @class OverlapKeeper
 * @constructor
 */
class OverlapKeeper {
  List<num> current;
  List<num> previous;

  OverlapKeeper() {
    this.current = [];
    this.previous = [];
  }

  num getKey(num i, num j) {
    if (j < i) {
      final temp = j;
      j = i;
      i = temp;
    }
    return (((i as int) << 16) as int) | (j as int);
  }

  /**
   * @method set
   *
   *
   */
  void set(num i, num j) {
    // Insertion sort. This way the diff will have linear complexity.
    final key = this.getKey(i, j);
    final current = this.current;
    var index = 0;
    while (key > current[index]) {
      index++;
    }
    if (identical(key, current[index])) {
      return;
    }
    for (var j = current.length - 1; j >= index; j--) {
      current[j + 1] = current[j];
    }
    current[index] = key;
  }

  /**
   * @method tick
   */
  void tick() {
    final tmp = this.current;
    this.current = this.previous;
    this.previous = tmp;
    this.current.length = 0;
  }

  /**
   * @method getDiff
   *
   *
   */
  void getDiff(List<num> additions, List<num> removals) {
    final a = this.current;
    final b = this.previous;
    final al = a.length;
    final bl = b.length;
    var j = 0;
    for (var i = 0; i < al; i++) {
      var found = false;
      final keyA = a[i];
      while (keyA > b[j]) {
        j++;
      }
      found = identical(keyA, b[j]);
      if (!found) {
        unpackAndPush(additions, keyA);
      }
    }
    j = 0;
    for (var i = 0; i < bl; i++) {
      var found = false;
      final keyB = b[i];
      while (keyB > a[j]) {
        j++;
      }
      found = identical(a[j], keyB);
      if (!found) {
        unpackAndPush(removals, keyB);
      }
    }
  }
}

void unpackAndPush(List<num> array, num key) {
  array.add((((key as int) & 0xffff0000) as int) >> 16);
  array.add((key as int) & 0x0000ffff);
}
