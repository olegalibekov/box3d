import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import "../shapes/Shape.dart" show Shape;

/**
 * Storage for Ray casting data.
 * @class RaycastResult
 * @constructor
 */
class RaycastResult {
  Vec3 rayFromWorld;
  Vec3 rayToWorld;
  Vec3 hitNormalWorld;
  Vec3 hitPointWorld;
  bool hasHit;
  dynamic /* Shape | null */ shape;
  dynamic /* Body | null */ body;
  num hitFaceIndex;
  num distance;
  bool shouldStop;
  RaycastResult() {
    this.rayFromWorld = new Vec3();
    this.rayToWorld = new Vec3();
    this.hitNormalWorld = new Vec3();
    this.hitPointWorld = new Vec3();
    this.hasHit = false;
    this.shape = null;
    this.body = null;
    this.hitFaceIndex = -1;
    this.distance = -1;
    this.shouldStop = false;
  }
  /**
   * Reset all result data.
   * @method reset
   */
  void reset() {
    this.rayFromWorld.setZero();
    this.rayToWorld.setZero();
    this.hitNormalWorld.setZero();
    this.hitPointWorld.setZero();
    this.hasHit = false;
    this.shape = null;
    this.body = null;
    this.hitFaceIndex = -1;
    this.distance = -1;
    this.shouldStop = false;
  }

  /**
   * @method abort
   */
  void abort() {
    this.shouldStop = true;
  }

  /**
   * @method set
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   */
  void set(Vec3 rayFromWorld, Vec3 rayToWorld, Vec3 hitNormalWorld,
      Vec3 hitPointWorld, Shape shape, Body body, num distance) {
    this.rayFromWorld.copy(rayFromWorld);
    this.rayToWorld.copy(rayToWorld);
    this.hitNormalWorld.copy(hitNormalWorld);
    this.hitPointWorld.copy(hitPointWorld);
    this.shape = shape;
    this.body = body;
    this.distance = distance;
  }
}
