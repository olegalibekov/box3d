import "../objects/Body.dart" show Body;

/**
 * Collision "matrix". It's actually a triangular-shaped array of whether two bodies are touching this step, for reference next step
 * @class ArrayCollisionMatrix
 * @constructor
 */
class ArrayCollisionMatrix {
  List<num> matrix;

  ArrayCollisionMatrix() {
    this.matrix = [];
  }

  /**
   * Get an element
   * @method get
   *
   *
   *
   */
  num get(Body bi, Body bj) {
    var i = bi.index;
    var j = bj.index;
    if (j > i) {
      final temp = j;
      j = i;
      i = temp;
    }
    return this.matrix[(((i * (i + 1)) as int) >> 1) + j - 1];
  }

  /**
   * Set an element
   * @method set
   *
   *
   *
   */
  void set(Body bi, Body bj, bool value) {
    var i = bi.index;
    var j = bj.index;
    if (j > i) {
      final temp = j;
      j = i;
      i = temp;
    }
    this.matrix[(((i * (i + 1)) as int) >> 1) + j - 1] = value ? 1 : 0;
  }

  /**
   * Sets all elements to zero
   * @method reset
   */
  void reset() {
    for (var i = 0, l = this.matrix.length; !identical(i, l); i++) {
      this.matrix[i] = 0;
    }
  }

  /**
   * Sets the max number of objects
   * @method setNumObjects
   *
   */
  void setNumObjects(num n) {
    this.matrix.length = ((n * (n - 1)) as int) >> 1;
  }
}
