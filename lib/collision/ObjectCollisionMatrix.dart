import "../objects/Body.dart" show Body;

/**
 * Maps what objects are colliding with each other
 * @class ObjectCollisionMatrix
 * @constructor
 */
class ObjectCollisionMatrix {
  Map<String, bool> matrix;

  ObjectCollisionMatrix() {
    this.matrix = {};
  }

  /**
   * @method get
   *
   *
   *
   */
  bool get(Body bi, Body bj) {
    var i = bi.id;
    var j = bj.id;
    if (j > i) {
      final temp = j;
      j = i;
      i = temp;
    }
    return this.matrix["${i}-${j}"];
  }

  /**
   * @method set
   *
   *
   *
   */
  void set(Body bi, Body bj, bool value) {
    var i = bi.id;
    var j = bj.id;
    if (j > i) {
      final temp = j;
      j = i;
      i = temp;
    }
    if (value) {
      this.matrix["${i}-${j}"] = true;
    } else {
      this.matrix.remove("${i}-${j}");
    }
  }

  /**
   * Empty the matrix
   * @method reset
   */
  void reset() {
    this.matrix = {};
  }

  /**
   * Set max number of objects
   * @method setNumObjects
   *
   */
  void setNumObjects(num n) {}
}
