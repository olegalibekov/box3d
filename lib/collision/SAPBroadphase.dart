import "../collision/AABB.dart" show AABB;
import "../collision/Broadphase.dart" show Broadphase;
import "../objects/Body.dart" show Body;
import "../world/World.dart" show World;

/**
 * Sweep and prune broadphase along one axis.
 *
 * @class SAPBroadphase
 * @constructor
 *
 * @extends Broadphase
 */
class SAPBroadphase extends Broadphase {
  List<Body> axisList;

  World world;

  num axisIndex = 0 | 1 | 2;

  dynamic /* (event: { body: Body }) => void */ _addBodyHandler;

  dynamic /* (event: { body: Body }) => void */ _removeBodyHandler;

  /// Check if the bounds of two bodies overlap, along the given SAP axis.
  /// @static
  /// @method checkBounds
  static bool checkBounds(Body bi, Body bj, [axisIndex = (0 | 1) | 2]) {
    num biPos;
    num bjPos;
    if (identical(axisIndex, 0)) {
      biPos = bi.position.x;
      bjPos = bj.position.x;
    } else if (identical(axisIndex, 1)) {
      biPos = bi.position.y;
      bjPos = bj.position.y;
    } else if (identical(axisIndex, 2)) {
      biPos = bi.position.z;
      bjPos = bj.position.z;
    }
    final ri = bi.boundingRadius,
        rj = bj.boundingRadius,
// boundA1 = biPos - ri,
        boundA2 = biPos + ri,
        boundB1 = bjPos - rj;
// boundB2 = bjPos + rj;
    return boundB1 < boundA2;
  }

  /// @static
  /// @method insertionSortY
  static List<Body> insertionSortY(List<Body> a) {
    for (var i = 1, l = a.length; i < l; i++) {
      final v = a[i];
      num j;
      for (j = i - 1; j >= 0; j--) {
        if (a[j].aabb.lowerBound.y <= v.aabb.lowerBound.y) {
          break;
        }
        a[j + 1] = a[j];
      }
      a[j + 1] = v;
    }
    return a;
  }

  /// @static
  /// @method insertionSortX
  static List<Body> insertionSortX(List<Body> a) {
    for (var i = 1, l = a.length; i < l; i++) {
      final v = a[i];
      num j;
      for (j = i - 1; j >= 0; j--) {
        if (a[j].aabb.lowerBound.x <= v.aabb.lowerBound.x) {
          break;
        }
        a[j + 1] = a[j];
      }
      a[j + 1] = v;
    }
    return a;
  }

  /// @static
  /// @method insertionSortZ
  static List<Body> insertionSortZ(List<Body> a) {
    for (var i = 1, l = a.length; i < l; i++) {
      final v = a[i];
      num j;
      for (j = i - 1; j >= 0; j--) {
        if (a[j].aabb.lowerBound.z <= v.aabb.lowerBound.z) {
          break;
        }
        a[j + 1] = a[j];
      }
      a[j + 1] = v;
    }
    return a;
  }

  SAPBroadphase(World world) : super() {
    /* super call moved to initializer */;
    this.axisList = [];
    this.world = null;
    this.axisIndex = 0;
    final axisList = this.axisList;
    this._addBodyHandler = (dynamic event) {
      axisList.add(event.body);
    };
    this._removeBodyHandler = (dynamic event) {
      final idx = axisList.indexOf(event.body);
      if (!identical(idx, -1)) {
        axisList.removeRange(idx, 1);
      }
    };
    if (world != null) {
      this.setWorld(world);
    }
  }

  /**
   * Change the world
   * @method setWorld
   *
   */
  void setWorld(World world) {
    // Clear the old axis array
    this.axisList.length = 0;
    // Add all bodies from the new world
    for (var i = 0; i < world.bodies.length; i++) {
      this.axisList.add(world.bodies[i]);
    }
    // Remove old handlers, if any
    world.removeEventListener("addBody", this._addBodyHandler);
    world.removeEventListener("removeBody", this._removeBodyHandler);
    // Add handlers to update the list of bodies.
    world.addEventListener("addBody", this._addBodyHandler);
    world.addEventListener("removeBody", this._removeBodyHandler);
    this.world = world;
    this.dirty = true;
  }

  /**
   * Collect all collision pairs
   * @method collisionPairs
   *
   *
   *
   */
  void collisionPairs(World world, List<Body> p1, List<Body> p2) {
    final bodies = this.axisList;
    final N = bodies.length;
    final axisIndex = this.axisIndex;
    var i;
    var j;
    if (this.dirty) {
      this.sortList();
      this.dirty = false;
    }
    // Look through the list
    for (i = 0; !identical(i, N); i++) {
      final bi = bodies[i];
      for (j = i + 1; j < N; j++) {
        final bj = bodies[j];
        if (!this.needBroadphaseCollision(bi, bj)) {
          continue;
        }
        if (!SAPBroadphase.checkBounds(bi, bj, axisIndex)) {
          break;
        }
        this.intersectionTest(bi, bj, p1, p2);
      }
    }
  }

  void sortList() {
    final axisList = this.axisList;
    final axisIndex = this.axisIndex;
    final N = axisList.length;
    // Update AABBs
    for (var i = 0; !identical(i, N); i++) {
      final bi = axisList[i];
      if (bi.aabbNeedsUpdate) {
        bi.computeAABB();
      }
    }
    // Sort the list
    if (identical(axisIndex, 0)) {
      SAPBroadphase.insertionSortX(axisList);
    } else if (identical(axisIndex, 1)) {
      SAPBroadphase.insertionSortY(axisList);
    } else if (identical(axisIndex, 2)) {
      SAPBroadphase.insertionSortZ(axisList);
    }
  }

  /**
   * Computes the variance of the body positions and estimates the best
   * axis to use. Will automatically set property .axisIndex.
   * @method autoDetectAxis
   */
  void autoDetectAxis() {
    var sumX = 0;
    var sumX2 = 0;
    var sumY = 0;
    var sumY2 = 0;
    var sumZ = 0;
    var sumZ2 = 0;
    final bodies = this.axisList;
    final N = bodies.length;
    final invN = 1 / N;
    for (var i = 0; !identical(i, N); i++) {
      final b = bodies[i];
      final centerX = b.position.x;
      sumX += centerX;
      sumX2 += centerX * centerX;
      final centerY = b.position.y;
      sumY += centerY;
      sumY2 += centerY * centerY;
      final centerZ = b.position.z;
      sumZ += centerZ;
      sumZ2 += centerZ * centerZ;
    }
    final varianceX = sumX2 - sumX * sumX * invN;
    final varianceY = sumY2 - sumY * sumY * invN;
    final varianceZ = sumZ2 - sumZ * sumZ * invN;
    if (varianceX > varianceY) {
      if (varianceX > varianceZ) {
        this.axisIndex = 0;
      } else {
        this.axisIndex = 2;
      }
    } else if (varianceY > varianceZ) {
      this.axisIndex = 1;
    } else {
      this.axisIndex = 2;
    }
  }

  /// Returns all the bodies within an AABB.
  /// @method aabbQuery
  List<Body> aabbQuery(World world, AABB aabb, [List<Body> result = const []]) {
    if (this.dirty) {
      this.sortList();
      this.dirty = false;
    }
    final axisIndex = this.axisIndex;
    var lower = aabb.lowerBound.x;
    var upper = aabb.upperBound.x;
    if (identical(axisIndex, 1)) {
      lower = aabb.lowerBound.y;
      upper = aabb.upperBound.y;
    }
    if (identical(axisIndex, 2)) {
      lower = aabb.lowerBound.z;
      upper = aabb.upperBound.z;
    }
    final axisList = this.axisList;
    for (var i = 0; i < axisList.length; i++) {
      final b = axisList[i];
      if (b.aabbNeedsUpdate) {
        b.computeAABB();
      }
      if (b.aabb.overlaps(aabb)) {
        result.add(b);
      }
    }
    return result;
  }
}
