import 'dart:math' as Math;

import "../collision/AABB.dart" show AABB;
import "../collision/RaycastResult.dart" show RaycastResult;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import "../shapes/Box.dart" show Box;
import "../shapes/ConvexPolyhedron.dart" show ConvexPolyhedron;
import "../shapes/Heightfield.dart" show Heightfield;
import "../shapes/Plane.dart" show Plane;
import "../shapes/Shape.dart" show Shape;
import '../shapes/Shape.dart';
import "../shapes/Sphere.dart" show Sphere;
import "../shapes/Trimesh.dart" show Trimesh;
import "../world/World.dart" show World;

enum RAY_MODES { CLOSEST, ANY, ALL }

typedef void RaycastCallback(RaycastResult result);

class RayMode {}

class RayOptions {
  Vec3 from;
  Vec3 to;
  RAY_MODES mode;
  RaycastResult result;
  bool skipBackfaces;
  num collisionFilterMask;
  num collisionFilterGroup;
  bool checkCollisionResponse;
  RaycastCallback callback;
}

/**
 * A line in 3D space that intersects bodies and return points.
 * @class Ray
 * @constructor
 *
 *
 */
class Ray {
  Vec3 from;

  Vec3 to;

  Vec3 direction;

  num precision;

  bool checkCollisionResponse;

  bool skipBackfaces;

  num collisionFilterMask;

  num collisionFilterGroup;

  RAY_MODES mode;

  RaycastResult result;

  bool hasHit;

  RaycastCallback callback;

  static bool pointInTriangle(Vec3 p, Vec3 a, Vec3 b, Vec3 c) {
    c.vsub(a, v0);
    b.vsub(a, v1);
    p.vsub(a, v2);
    final dot00 = v0.dot(v0);
    final dot01 = v0.dot(v1);
    final dot02 = v0.dot(v2);
    final dot11 = v1.dot(v1);
    final dot12 = v1.dot(v2);
    var u;
    var v;
    return ((u = dot11 * dot02 - dot01 * dot12) >= 0 &&
        (v = dot00 * dot12 - dot01 * dot02) >= 0 &&
        u + v < dot00 * dot11 - dot01 * dot01);
  }

  Ray([from, to]) {
    if (from == null) from = Vec3();
    if (to == null) to = Vec3();
    this.from = from.clone();
    this.to = to.clone();
    this.direction = new Vec3();
    this.precision = 0.0001;
    this.checkCollisionResponse = true;
    this.skipBackfaces = false;
    this.collisionFilterMask = -1;
    this.collisionFilterGroup = -1;
    this.mode = RAY_MODES.ANY;
    this.result = new RaycastResult();
    this.hasHit = false;
    this.callback = (result) {};
  }

  /**
   * Do itersection against all bodies in the given World.
   * @method intersectWorld
   *
   *
   *
   */
  bool intersectWorld(World world, RayOptions options) {
    this.mode = options.mode ?? RAY_MODES.ANY;
    this.result = options.result ?? new RaycastResult();
    this.skipBackfaces = !!options.skipBackfaces;
    this.collisionFilterMask = options.collisionFilterMask ?? -1;
    this.collisionFilterGroup = options.collisionFilterGroup ?? -1;
    this.checkCollisionResponse = options.checkCollisionResponse ?? true;
    if (options.from != null) {
      this.from.copy(options.from);
    }
    if (options.to != null) {
      this.to.copy(options.to);
    }
    this.callback = options.callback ?? (() {});
    this.hasHit = false;
    this.result.reset();
    this.updateDirection();
    this.getAABB(tmpAABB);
    tmpArray.length = 0;
    world.broadphase.aabbQuery(world, tmpAABB, tmpArray);
    this.intersectBodies(tmpArray);
    return this.hasHit;
  }

  /**
   * Shoot a ray at a body, get back information about the hit.
   *
   *
   */
  void intersectBody(Body body, [RaycastResult result]) {
    if (result != null) {
      this.result = result;
      this.updateDirection();
    }
    final checkCollisionResponse = this.checkCollisionResponse;
    if (checkCollisionResponse && !body.collisionResponse) {
      return;
    }
    if (identical(
            ((this.collisionFilterGroup as int) &
                (body.collisionFilterMask as int)),
            0) ||
        identical(
            ((body.collisionFilterGroup as int) &
                (this.collisionFilterMask as int)),
            0)) {
      return;
    }
    final xi = intersectBody_xi;
    final qi = intersectBody_qi;
    for (var i = 0, N = body.shapes.length; i < N; i++) {
      final shape = body.shapes[i];
      if (checkCollisionResponse && !shape.collisionResponse) {
        continue;
      }
      body.quaternion.mult(body.shapeOrientations[i], qi);
      body.quaternion.vmult(body.shapeOffsets[i], xi);
      xi.vadd(body.position, xi);
      this.intersectShape(shape, qi, xi, body);
      if (this.result.shouldStop) {
        break;
      }
    }
  }

  /**
   * @method intersectBodies
   *
   *
   */
  void intersectBodies(List<Body> bodies, [RaycastResult result]) {
    if (result != null) {
      this.result = result;
      this.updateDirection();
    }
    for (var i = 0, l = bodies.length; !this.result.shouldStop && i < l; i++) {
      this.intersectBody(bodies[i]);
    }
  }

  /**
   * Updates the direction vector.
   */
  void updateDirection() {
    this.to.vsub(this.from, this.direction);
    this.direction.normalize();
  }

  void intersectShape(Shape shape, Quaternion quat, Vec3 position, Body body) {
    final from = this.from;
    // Checking boundingSphere
    final distance = distanceFromIntersection(from, this.direction, position);
    if (distance > shape.boundingSphereRadius) {
      return;
    }
    Function chooseIntersectionMethod(SHAPE_TYPES shapes) {
      switch (shapes) {
        case SHAPE_TYPES.SPHERE:
          return _intersectSphere;
          break;
        case SHAPE_TYPES.PLANE:
          return _intersectPlane;
          break;

        case SHAPE_TYPES.HEIGHTFIELD:
          return _intersectHeightfield;
          break;
        case SHAPE_TYPES.TRIMESH:
          return _intersectTrimesh;
          break;
        case SHAPE_TYPES.COMPOUND:
        case SHAPE_TYPES.CONVEXPOLYHEDRON:
        case SHAPE_TYPES.PARTICLE:
        case SHAPE_TYPES.CYLINDER:
        case SHAPE_TYPES.BOX:
          return _intersectBox;
          break;
      }
    }

    final intersectMethod = chooseIntersectionMethod(shape.type);
    if (intersectMethod != null) {
      intersectMethod(shape, quat, position, body, shape);
    }
  }

  void _intersectBox(
      Box box, Quaternion quat, Vec3 position, Body body, Shape reportedShape) {
    return this._intersectConvex(box.convexPolyhedronRepresentation, quat,
        position, body, reportedShape);
  }

  void _intersectPlane(Plane shape, Quaternion quat, Vec3 position, Body body,
      Shape reportedShape) {
    final from = this.from;
    final to = this.to;
    final direction = this.direction;
    // Get plane normal
    final worldNormal = new Vec3(0, 0, 1);
    quat.vmult(worldNormal, worldNormal);
    final len = new Vec3();
    from.vsub(position, len);
    final planeToFrom = len.dot(worldNormal);
    to.vsub(position, len);
    final planeToTo = len.dot(worldNormal);
    if (planeToFrom * planeToTo > 0) {
      // "from" and "to" are on the same side of the plane... bail out
      return;
    }
    if (from.distanceTo(to) < planeToFrom) {
      return;
    }
    final n_dot_dir = worldNormal.dot(direction);
    if ((n_dot_dir).abs() < this.precision) {
      // No intersection
      return;
    }
    final planePointToFrom = new Vec3();
    final dir_scaled_with_t = new Vec3();
    final hitPointWorld = new Vec3();
    from.vsub(position, planePointToFrom);
    final t = -worldNormal.dot(planePointToFrom) / n_dot_dir;
    direction.scale(t, dir_scaled_with_t);
    from.vadd(dir_scaled_with_t, hitPointWorld);
    this.reportIntersection(
        worldNormal, hitPointWorld, reportedShape, body, -1);
  }

  /// Get the world AABB of the ray.
  void getAABB(AABB aabb) {
    final lowerBound = aabb.lowerBound;
    final upperBound = aabb.upperBound;

    final to = this.to;
    final from = this.from;
    lowerBound.x = Math.min(to.x, from.x);
    lowerBound.y = Math.min(to.y, from.y);
    lowerBound.z = Math.min(to.z, from.z);
    upperBound.x = Math.max(to.x, from.x);
    upperBound.y = Math.max(to.y, from.y);
    upperBound.z = Math.max(to.z, from.z);
  }

  void _intersectHeightfield(Heightfield shape, Quaternion quat, Vec3 position,
      Body body, Shape reportedShape) {
    final data = shape.data;
    final w = shape.elementSize;
    // Convert the ray to local heightfield coordinates
    final localRay = intersectHeightfield_localRay;
    localRay.from.copy(this.from);
    localRay.to.copy(this.to);
    Transform.pointToLocalFrame(position, quat, localRay.from, localRay.from);
    Transform.pointToLocalFrame(position, quat, localRay.to, localRay.to);
    localRay.updateDirection();
    // Get the index of the data points to test against
    final index = intersectHeightfield_index;
    num iMinX;
    num iMinY;
    num iMaxX;
    num iMaxY;
    // Set to max
    iMinX = iMinY = 0;
    iMaxX = iMaxY = shape.data.length - 1;
    final aabb = new AABB();
    localRay.getAABB(aabb);
    shape.getIndexOfPosition(aabb.lowerBound.x, aabb.lowerBound.y, index, true);
    iMinX = Math.max(iMinX, index[0]);
    iMinY = Math.max(iMinY, index[1]);
    shape.getIndexOfPosition(aabb.upperBound.x, aabb.upperBound.y, index, true);
    iMaxX = Math.min(iMaxX, index[0] + 1);
    iMaxY = Math.min(iMaxY, index[1] + 1);
    for (var i = iMinX; i < iMaxX; i++) {
      for (var j = iMinY; j < iMaxY; j++) {
        if (this.result.shouldStop) {
          return;
        }
        shape.getAabbAtIndex(i, j,
            lowerBound: aabb.lowerBound, upperBound: aabb.upperBound);
        if (!aabb.overlapsRay(localRay)) {
          continue;
        }
        // Lower triangle
        shape.getConvexTrianglePillar(i, j, false);
        Transform.pointToWorldFrame(
            position, quat, shape.pillarOffset, worldPillarOffset);
        this._intersectConvex(shape.pillarConvex, quat, worldPillarOffset, body,
            reportedShape, intersectConvexOptions);
        if (this.result.shouldStop) {
          return;
        }
        // Upper triangle
        shape.getConvexTrianglePillar(i, j, true);
        Transform.pointToWorldFrame(
            position, quat, shape.pillarOffset, worldPillarOffset);
        this._intersectConvex(shape.pillarConvex, quat, worldPillarOffset, body,
            reportedShape, intersectConvexOptions);
      }
    }
  }

  void _intersectSphere(Sphere sphere, Quaternion quat, Vec3 position,
      Body body, Shape reportedShape) {
    final from = this.from;
    final to = this.to;
    final r = sphere.radius;
    final a = Math.pow((to.x - from.x), 2) +
        Math.pow((to.y - from.y), 2) +
        Math.pow((to.z - from.z), 2);
    final b = 2 *
        ((to.x - from.x) * (from.x - position.x) +
            (to.y - from.y) * (from.y - position.y) +
            (to.z - from.z) * (from.z - position.z));
    final c = Math.pow((from.x - position.x), 2) +
        Math.pow((from.y - position.y), 2) +
        Math.pow((from.z - position.z), 2) -
        r * r;
    final delta = b * b - 4 * a * c;
    final intersectionPoint = Ray_intersectSphere_intersectionPoint;
    final normal = Ray_intersectSphere_normal;
    if (delta < 0) {
      // No intersection
      return;
    } else if (identical(delta, 0)) {
      // single intersection point
      from.lerp(to, delta, intersectionPoint);
      intersectionPoint.vsub(position, normal);
      normal.normalize();
      this.reportIntersection(
          normal, intersectionPoint, reportedShape, body, -1);
    } else {
      final d1 = (-b - Math.sqrt(delta)) / (2 * a);
      final d2 = (-b + Math.sqrt(delta)) / (2 * a);
      if (d1 >= 0 && d1 <= 1) {
        from.lerp(to, d1, intersectionPoint);
        intersectionPoint.vsub(position, normal);
        normal.normalize();
        this.reportIntersection(
            normal, intersectionPoint, reportedShape, body, -1);
      }
      if (this.result.shouldStop) {
        return;
      }
      if (d2 >= 0 && d2 <= 1) {
        from.lerp(to, d2, intersectionPoint);
        intersectionPoint.vsub(position, normal);
        normal.normalize();
        this.reportIntersection(
            normal, intersectionPoint, reportedShape, body, -1);
      }
    }
  }

  void _intersectConvex(ConvexPolyhedron shape, Quaternion quat, Vec3 position,
      Body body, Shape reportedShape,
      [List<num> _faceList]) {
    final minDistNormal = intersectConvex_minDistNormal;
    final normal = intersectConvex_normal;
    final vector = intersectConvex_vector;
    final minDistIntersect = intersectConvex_minDistIntersect;
    final faceList = _faceList ?? [];
    // Checking faces
    final faces = shape.faces;
    final vertices = shape.vertices;
    final normals = shape.faceNormals;
    final direction = this.direction;
    final from = this.from;
    final to = this.to;
    final fromToDistance = from.distanceTo(to);
    final minDist = -1;
    final Nfaces = faceList != null ? faceList.length : faces.length;
    final result = this.result;
    for (var j = 0; !result.shouldStop && j < Nfaces; j++) {
      final fi = faceList != null ? faceList[j] : j;
      final face = faces[fi];
      final faceNormal = normals[fi];
      final q = quat;
      final x = position;
      // determine if ray intersects the plane of the face

      // note: this works regardless of the direction of the face normal

      // Get plane point in world coordinates...
      vector.copy(vertices[face[0]]);
      q.vmult(vector, vector);
      vector.vadd(x, vector);
      // ...but make it relative to the ray from. We'll fix this later.
      vector.vsub(from, vector);
      // Get plane normal
      q.vmult(faceNormal, normal);
      // If this dot product is negative, we have something interesting
      final dot = direction.dot(normal);
      // Bail out if ray and plane are parallel
      if ((dot).abs() < this.precision) {
        continue;
      }
      // calc distance to plane
      final scalar = normal.dot(vector) / dot;
      // if negative distance, then plane is behind ray
      if (scalar < 0) {
        continue;
      }
      // if (dot < 0) {

      // Intersection point is from + direction * scalar
      direction.scale(scalar, intersectPoint);
      intersectPoint.vadd(from, intersectPoint);
      // a is the point we compare points b and c with.
      a.copy(vertices[face[0]]);
      q.vmult(a, a);
      x.vadd(a, a);
      for (var i = 1; !result.shouldStop && i < face.length - 1; i++) {
        // Transform 3 vertices to world coords
        b.copy(vertices[face[i]]);
        c.copy(vertices[face[i + 1]]);
        q.vmult(b, b);
        q.vmult(c, c);
        x.vadd(b, b);
        x.vadd(c, c);
        final distance = intersectPoint.distanceTo(from);
        if (!(pointInTriangle(intersectPoint, a, b, c) ||
                pointInTriangle(intersectPoint, b, a, c)) ||
            distance > fromToDistance) {
          continue;
        }
        this.reportIntersection(
            normal, intersectPoint, reportedShape, body, fi);
      }
    }
  }

  /**
   * @todo Optimize by transforming the world to local space first.
   * @todo Use Octree lookup
   */
  void _intersectTrimesh(Trimesh mesh, Quaternion quat, Vec3 position,
      Body body, Shape reportedShape,
      [dynamic options]) {
    final normal = intersectTrimesh_normal;
    final triangles = intersectTrimesh_triangles;
    final treeTransform = intersectTrimesh_treeTransform;
    final minDistNormal = intersectConvex_minDistNormal;
    final vector = intersectConvex_vector;
    final minDistIntersect = intersectConvex_minDistIntersect;
    final localAABB = intersectTrimesh_localAABB;
    final localDirection = intersectTrimesh_localDirection;
    final localFrom = intersectTrimesh_localFrom;
    final localTo = intersectTrimesh_localTo;
    final worldIntersectPoint = intersectTrimesh_worldIntersectPoint;
    final worldNormal = intersectTrimesh_worldNormal;
    final faceList = (options && options.faceList) || null;
    // Checking faces
    final indices = mesh.indices;
    final vertices = mesh.vertices;
    // const normals = mesh.faceNormals
    final from = this.from;
    final to = this.to;
    final direction = this.direction;
    final minDist = -1;
    treeTransform.position.copy(position);
    treeTransform.quaternion.copy(quat);
    // Transform ray to local space!
    Transform.vectorToLocalFrame(position, quat, direction, localDirection);
    Transform.pointToLocalFrame(position, quat, from, localFrom);
    Transform.pointToLocalFrame(position, quat, to, localTo);
    localTo.x *= mesh.scale.x;
    localTo.y *= mesh.scale.y;
    localTo.z *= mesh.scale.z;
    localFrom.x *= mesh.scale.x;
    localFrom.y *= mesh.scale.y;
    localFrom.z *= mesh.scale.z;
    localTo.vsub(localFrom, localDirection);
    localDirection.normalize();
    final fromToDistanceSquared = localFrom.distanceSquared(localTo);
    mesh.tree.rayQuery(this, treeTransform, triangles);
    for (var i = 0, N = triangles.length;
        !this.result.shouldStop && !identical(i, N);
        i++) {
      final trianglesIndex = triangles[i];
      mesh.getNormal(trianglesIndex, normal);
      // determine if ray intersects the plane of the face

      // note: this works regardless of the direction of the face normal

      // Get plane point in world coordinates...
      mesh.getVertex(indices[trianglesIndex * 3], a);
      // ...but make it relative to the ray from. We'll fix this later.
      a.vsub(localFrom, vector);
      // If this dot product is negative, we have something interesting
      final dot = localDirection.dot(normal);
      // Bail out if ray and plane are parallel

      // if (Math.abs( dot ) < this.precision){

      //     continue;

      // }

      // calc distance to plane
      final scalar = normal.dot(vector) / dot;
      // if negative distance, then plane is behind ray
      if (scalar < 0) {
        continue;
      }
      // Intersection point is from + direction * scalar
      localDirection.scale(scalar, intersectPoint);
      intersectPoint.vadd(localFrom, intersectPoint);
      // Get triangle vertices
      mesh.getVertex(indices[trianglesIndex * 3 + 1], b);
      mesh.getVertex(indices[trianglesIndex * 3 + 2], c);
      final squaredDistance = intersectPoint.distanceSquared(localFrom);
      if (!(pointInTriangle(intersectPoint, b, a, c) ||
              pointInTriangle(intersectPoint, a, b, c)) ||
          squaredDistance > fromToDistanceSquared) {
        continue;
      }
      // transform intersectpoint and normal to world
      Transform.vectorToWorldFrameStatic(quat, normal, worldNormal);
      Transform.pointToWorldFrame(
          position, quat, intersectPoint, worldIntersectPoint);
      this.reportIntersection(worldNormal, worldIntersectPoint, reportedShape,
          body, trianglesIndex);
    }
    triangles.length = 0;
  }

  /**
   *
   */
  void reportIntersection(Vec3 normal, Vec3 hitPointWorld, Shape shape,
      Body body, num hitFaceIndex) {
    final from = this.from;
    final to = this.to;
    final distance = from.distanceTo(hitPointWorld);
    final result = this.result;
    // Skip back faces?
    if (this.skipBackfaces && normal.dot(this.direction) > 0) {
      return;
    }
    result.hitFaceIndex = hitFaceIndex ?? -1;
    switch (this.mode) {
      case RAY_MODES.ALL:
        this.hasHit = true;
        result.set(from, to, normal, hitPointWorld, shape, body, distance);
        result.hasHit = true;
        this.callback(result);
        break;
      case RAY_MODES.CLOSEST:
        // Store if closer than current closest
        if (distance < result.distance || !result.hasHit) {
          this.hasHit = true;
          result.hasHit = true;
          result.set(from, to, normal, hitPointWorld, shape, body, distance);
        }
        break;
      case RAY_MODES.ANY:
        // Report and stop.
        this.hasHit = true;
        result.hasHit = true;
        result.set(from, to, normal, hitPointWorld, shape, body, distance);
        result.shouldStop = true;
        break;
    }
  }
}

final tmpAABB = new AABB();

final List<Body> tmpArray = [];

final v1 = new Vec3();

final v2 = new Vec3();
/*
 * As per "Barycentric Technique" as named here http://www.blackpawn.com/texts/pointinpoly/default.html But without the division
 */

final intersectBody_xi = new Vec3();

final intersectBody_qi = new Quaternion();

final vector = new Vec3();

final normal = new Vec3();

final intersectPoint = new Vec3();

final a = new Vec3();

final b = new Vec3();

final c = new Vec3();

final d = new Vec3();

final tmpRaycastResult = new RaycastResult();

final intersectConvexOptions = [0];

final worldPillarOffset = new Vec3();

final intersectHeightfield_localRay = new Ray();

final List<num> intersectHeightfield_index = [];

final intersectHeightfield_minMax = [];

final Ray_intersectSphere_intersectionPoint = new Vec3();

final Ray_intersectSphere_normal = new Vec3();

final intersectConvex_normal = new Vec3();

final intersectConvex_minDistNormal = new Vec3();

final intersectConvex_minDistIntersect = new Vec3();

final intersectConvex_vector = new Vec3();

final intersectTrimesh_normal = new Vec3();

final intersectTrimesh_localDirection = new Vec3();

final intersectTrimesh_localFrom = new Vec3();

final intersectTrimesh_localTo = new Vec3();

final intersectTrimesh_worldNormal = new Vec3();

final intersectTrimesh_worldIntersectPoint = new Vec3();

final intersectTrimesh_localAABB = new AABB();

final List<num> intersectTrimesh_triangles = [];

final intersectTrimesh_treeTransform = new Transform();

final v0 = new Vec3();

final intersect = new Vec3();

num distanceFromIntersection(Vec3 from, Vec3 direction, Vec3 position) {
  // v0 is vector from from to position
  position.vsub(from, v0);
  final dot = v0.dot(direction);
  // intersect = direction*dot + from
  direction.scale(dot, intersect);
  intersect.vadd(from, intersect);
  final distance = position.distanceTo(intersect);
  return distance;
}
