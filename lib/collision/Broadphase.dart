import 'dart:math';

import "../collision/AABB.dart" show AABB;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body, BODY_TYPES, BODY_SLEEP_STATES;
import "../world/World.dart" show World;

/**
 * Base class for broadphase implementations
 * @class Broadphase
 * @constructor
 * @author schteppe
 */
class Broadphase {
  World world;

  bool useBoundingBoxes;

  bool dirty;

  static bool boundingSphereCheck(bodyA, bodyB) {
    final dist = new Vec3();
    bodyA.position.vsub(bodyB.position, dist);
    final sa = bodyA.shapes[0];
    final sb = bodyB.shapes[0];
    return pow(sa.boundingSphereRadius + sb.boundingSphereRadius, 2) >
        dist.lengthSquared();
  }

  Broadphase() {
    this.world = null;
    this.useBoundingBoxes = false;
    this.dirty = true;
  }

  /**
   * Get the collision pairs from the world
   * @method collisionPairs
   *
   *
   *
   */
  void collisionPairs(World world, List<Body> p1, List<Body> p2) {
    throw ("collisionPairs not implemented for this BroadPhase class!");
  }

  /**
   * Check if a body pair needs to be intersection tested at all.
   * @method needBroadphaseCollision
   *
   *
   *
   */
  bool needBroadphaseCollision(Body bodyA, Body bodyB) {
    // Check collision filter masks
    if (identical(
            ((bodyA.collisionFilterGroup as int) &
                (bodyB.collisionFilterMask as int)),
            0) ||
        identical(
            ((bodyB.collisionFilterGroup as int) &
                (bodyA.collisionFilterMask as int)),
            0)) {
      return false;
    }
    // Check types

    if (((bodyA.type == BODY_TYPES.STATIC) ||
        (bodyA.sleepState == BODY_SLEEP_STATES.SLEEPING)) &&
        ((bodyB.type == BODY_TYPES.STATIC) ||
            (bodyB.sleepState == BODY_SLEEP_STATES.SLEEPING))) {
      // Both bodies are static or sleeping. Skip.
      return false;
    }
    return true;
  }

  /**
   * Check if the bounding volumes of two bodies intersect.
   * @method intersectionTest
   *
   *
   *
   *
   */
  void intersectionTest(
      Body bodyA, Body bodyB, List<Body> pairs1, List<Body> pairs2) {
    if (this.useBoundingBoxes) {
      this.doBoundingBoxBroadphase(bodyA, bodyB, pairs1, pairs2);
    } else {
      this.doBoundingSphereBroadphase(bodyA, bodyB, pairs1, pairs2);
    }
  }

  void doBoundingSphereBroadphase(
      Body bodyA, Body bodyB, List<Body> pairs1, List<Body> pairs2) {
    final r = Broadphase_collisionPairs_r;
    bodyB.position.vsub(bodyA.position, r);
    final boundingRadiusSum2 =
        pow((bodyA.boundingRadius + bodyB.boundingRadius), 2);
    final norm2 = r.lengthSquared();
    if (norm2 < boundingRadiusSum2) {
      pairs1.add(bodyA);
      pairs2.add(bodyB);
    }
  }

  /**
   * Check if the bounding boxes of two bodies are intersecting.
   * @method doBoundingBoxBroadphase
   *
   *
   *
   *
   */
  void doBoundingBoxBroadphase(
      Body bodyA, Body bodyB, List<Body> pairs1, List<Body> pairs2) {
    if (bodyA.aabbNeedsUpdate) {
      bodyA.computeAABB();
    }
    if (bodyB.aabbNeedsUpdate) {
      bodyB.computeAABB();
    }
    // Check AABB / AABB
    if (bodyA.aabb.overlaps(bodyB.aabb)) {
      pairs1.add(bodyA);
      pairs2.add(bodyB);
    }
  }

  void makePairsUnique(List<Body> pairs1, List<Body> pairs2) {
    final t = Broadphase_makePairsUnique_temp;
    final p1 = Broadphase_makePairsUnique_p1;
    final p2 = Broadphase_makePairsUnique_p2;
    final N = pairs1.length;
    for (var i = 0; !identical(i, N); i++) {
      p1[i] = pairs1[i];
      p2[i] = pairs2[i];
    }
    pairs1.length = 0;
    pairs2.length = 0;
    for (var i = 0; !identical(i, N); i++) {
      final id1 = p1[i].id;
      final id2 = p2[i].id;
      final key = id1 < id2 ? '''${id1},${id2}''' : '''${id2},${id1}''';
      t[key] = i;
      t[key] = null;
    }
    for (var i = 0; !identical(i, t.keys.length); i++) {
      final key = t.keys.last;
      t.remove(key);
      final pairIndex = t[key];
      pairs1.add(p1[pairIndex]);
      pairs2.add(p2[pairIndex]);
      ;
    }
  }

  /**
   * To be implemented by subcasses
   * @method setWorld
   *
   */
  void setWorld(World world) {}

  /**
   * Returns all the bodies within the AABB.
   * @method aabbQuery
   *
   *
   *
   *
   */
  List<Body> aabbQuery(World world, AABB aabb, List<Body> result) {
    print(".aabbQuery is not implemented in this Broadphase subclass.");
    return [];
  }
}

/**
 * Check if the bounding spheres of two bodies are intersecting.
 * @method doBoundingSphereBroadphase
 *
 *
 *
 *
 */
final Broadphase_collisionPairs_r = new Vec3();

final Broadphase_collisionPairs_normal = new Vec3();

final Broadphase_collisionPairs_quat = new Quaternion();

final Broadphase_collisionPairs_relpos = new Vec3();
/**
 * Removes duplicate pairs from the pair arrays.
 * @method makePairsUnique
 *
 *
 */
final Map<String, dynamic> Broadphase_makePairsUnique_temp = {"keys": []};

final List<Body> Broadphase_makePairsUnique_p1 = [];

final List<Body> Broadphase_makePairsUnique_p2 = [];
/**
 * Check if the bounding spheres of two bodies overlap.
 * @method boundingSphereCheck
 *
 *
 *
 */
final bsc_dist = new Vec3();
