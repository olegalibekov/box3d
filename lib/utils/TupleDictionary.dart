/// @class TupleDictionary
/// @constructor
class TupleDictionary {
  Map<String, dynamic> data;

  TupleDictionary();

  /// @method get
  dynamic get(num i, num j) {
    if (i > j) {
      // swap
      final temp = j;
      j = i;
      i = temp;
    }
    return this.data["$i-$j"];
  }

  /// @method set
  void set(num i, num j, dynamic value) {
    if (i > j) {
      final temp = j;
      j = i;
      i = temp;
    }
    final key = '$i-$j';

    this.data[key] = value;
  }

  /**
   * @method reset
   */
  void reset() {
    data.clear();
  }
}
