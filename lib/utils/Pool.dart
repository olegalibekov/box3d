/**
 * For pooling objects that can be reused.
 * @class Pool
 * @constructor
 */
class Pool {
  List<dynamic> objects;
  dynamic type;

  Pool() {
    this.objects = [];
    this.type = Object;
  }

  /**
   * Release an object after use
   * @method release
   *
   */
  Pool release(List<dynamic> args) {
    final Nargs = args.length;
    for (var i = 0; !identical(i, Nargs); i++) {
      this.objects.add(args[i]);
    }
    return this;
  }

  /**
   * Get an object
   * @method get
   *
   */
  dynamic get() {
    if (identical(this.objects.length, 0)) {
      return this.constructObject();
    } else {
      return this.objects.removeLast();
    }
  }

  /**
   * Construct an object. Should be implemented in each subclass.
   * @method constructObject
   *
   */
  dynamic constructObject() {
    throw ("constructObject() not implemented in this Pool subclass yet!");
  }

  /**
   * @method resize
   *
   *
   */
  Pool resize(num size) {
    final objects = this.objects;
    while (objects.length > size) {
      objects.removeLast();
    }
    while (objects.length < size) {
      objects.add(this.constructObject());
    }
    return this;
  }
}
