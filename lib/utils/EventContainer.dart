class EventContainer<EventType, EventValue> {
  EventType eventType;

  EventValue eventValue;

  factory EventContainer(EventType key, [EventValue value]) =>
      EventContainer<EventType, EventValue>._(key, value ?? null);

  EventContainer._(this.eventType, this.eventValue);

  String toString() =>
      "EventContainer(${eventType.toString()}: ${eventValue.toString()})";
}
