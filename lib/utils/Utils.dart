class Utils {
  call() {}

  /// Extend an options object with default values.
  /// @static
  /// @method defaults
  static Map<String, dynamic> defaults(
      [Map<String, dynamic> options, Map<String, dynamic> defaults]) {
    options ??= {};
    options = Map<String, dynamic>.from(options);
    defaults = Map<String, dynamic>.from(defaults);
    for (var key in defaults.keys) {
      if (!options.containsKey(key)) {
        options[key] = defaults[key];
      }
    }
    return options;
  }
}
