/**
 * Base class for objects that dispatches events.
 * @class EventTarget
 * @constructor
 */
// class EventTarget {
//   Map _listeners;
//   EventTarget() {}
//   /**
//    * Add an event listener
//    * @method addEventListener
//    *
//    *
//    *
//    */
//   EventTarget addEventListener(String type, Function listener) {
//     if (identical(this._listeners, null)) {
//       this._listeners = {};
//     }
//     final listeners = this._listeners;
//     if (identical(listeners[type], null)) {
//       listeners[type] = [];
//     }
//     if (!listeners[type].includes(listener)) {
//       listeners[type].push(listener);
//     }
//     return this;
//   }
//
//   /**
//    * Check if an event listener is added
//    * @method hasEventListener
//    *
//    *
//    *
//    */
//   bool hasEventListener(String type, Function listener) {
//     if (identical(this._listeners, null)) {
//       return false;
//     }
//     final listeners = this._listeners;
//     if (!identical(listeners[type], null) &&
//         listeners[type].includes(listener)) {
//       return true;
//     }
//     return false;
//   }
//
//   /**
//    * Check if any event listener of the given type is added
//    * @method hasAnyEventListener
//    *
//    *
//    */
//   bool hasAnyEventListener(String type) {
//     if (identical(this._listeners, null)) {
//       return false;
//     }
//     final listeners = this._listeners;
//     return !identical(listeners[type], null);
//   }
//
//   /**
//    * Remove an event listener
//    * @method removeEventListener
//    *
//    *
//    *
//    */
//   EventTarget removeEventListener(String type, Function listener) {
//     if (identical(this._listeners, null)) {
//       return this;
//     }
//     final listeners = this._listeners;
//     if (identical(listeners[type], null)) {
//       return this;
//     }
//     final index = listeners[type].indexOf(listener);
//     if (!identical(index, -1)) {
//       listeners[type].splice(index, 1);
//     }
//     return this;
//   }
//
//   /**
//    * Emit an event.
//    * @method dispatchEvent
//    *
//    *
//    *
//    */
//   EventTarget dispatchEvent(dynamic event) {
//     if (identical(this._listeners, null)) {
//       return this;
//     }
//     final listeners = this._listeners;
//     final listenerArray = listeners[event.type];
//     if (!identical(listenerArray, null)) {
//       event.target = this;
//       for (var i = 0, l = listenerArray.length; i < l; i++) {
//         listenerArray[i].call(this, event);
//       }
//     }
//     return this;
//   }
// }
