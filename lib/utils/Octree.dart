import "../collision/AABB.dart" show AABB;
import "../collision/Ray.dart" show Ray;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;

/**
 * @class OctreeNode
 * @constructor
 *
 *
 *
 */
class OctreeNode {
  OctreeNode root;
  num maxDepth;
  AABB aabb;

  List<num> data;

  List<OctreeNode> children;

  OctreeNode({this.aabb, this.data, this.children, this.root}) {
    this.root = root;
    this.aabb = aabb?.clone() ?? new AABB();
    this.data = [];
    this.children = [];
  }

  void reset() {
    this.children.length = this.data.length = 0;
  }

  /// Insert data into this node
  /// @method insert

  bool insert(AABB aabb, num elementData, [level = 0]) {
    final nodeData = this.data;
    // Ignore objects that do not belong in this node
    if (!this.aabb.contains(aabb)) {
      return false;
    }
    final children = this.children;
    final maxDepth = this.maxDepth ?? root.maxDepth;
    if (level < maxDepth) {
      // Subdivide if there are no children yet
      var subdivided = false;
      if (children.isNotEmpty) {
        this.subdivide();
        subdivided = true;
      }
      // add to whichever node will accept it
      for (var i = 0; !identical(i, 8); i++) {
        if (children[i].insert(aabb, elementData, level + 1)) {
          return true;
        }
      }
      if (subdivided) {
        // No children accepted! Might as well just remove em since they contain none
        children.length = 0;
      }
    }
    // Too deep, or children didnt want it. add it in current node
    nodeData.add(elementData);
    return true;
  }

  /**
   * Create 8 equally sized children nodes and put them in the .children array.
   * @method subdivide
   */
  void subdivide() {
    final aabb = this.aabb;
    final l = aabb.lowerBound;
    final u = aabb.upperBound;
    final children = this.children;
    children.addAll([
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(0, 0, 0))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(1, 0, 0))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(1, 1, 0))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(1, 1, 1))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(0, 1, 1))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(0, 0, 1))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(1, 0, 1))),
      new OctreeNode(aabb: new AABB(lowerBound: new Vec3(0, 1, 0)))
    ]);
    u.vsub(l, halfDiagonal);
    halfDiagonal.scale(0.5, halfDiagonal);
    final root = this.root ?? this;
    for (var i = 0; !identical(i, 8); i++) {
      final child = children[i];
      // Set current node as root
      child.root = root;
      // Compute bounds
      final lowerBound = child.aabb.lowerBound;
      lowerBound.x *= halfDiagonal.x;
      lowerBound.y *= halfDiagonal.y;
      lowerBound.z *= halfDiagonal.z;
      lowerBound.vadd(l, lowerBound);
      // Upper bound is always lower bound + halfDiagonal
      lowerBound.vadd(halfDiagonal, child.aabb.upperBound);
    }
  }

  /**
   * Get all data, potentially within an AABB
   * @method aabbQuery
   *
   *
   *
   */
  List<num> aabbQuery(AABB aabb, List<num> result) {
    final nodeData = this.data;
    // abort if the range does not intersect this node

    // if (!this.aabb.overlaps(aabb)){

    //     return result;

    // }

    // Add objects at this level

    // Array.prototype.push.apply(result, nodeData);

    // Add child data

    // @todo unwrap recursion into a queue / loop, that's faster in JS
    final children = this.children;
    // for (let i = 0, N = this.children.length; i !== N; i++) {

    //     children[i].aabbQuery(aabb, result);

    // }
    final queue = [this];
    while (queue.isNotEmpty) {
      final OctreeNode node = queue.removeLast();
      if (node.aabb.overlaps(aabb)) {
        result.addAll(node.data);
      }
      queue.addAll(node.children);
    }
    return result;
  }

  /**
   * Get all data, potentially intersected by a ray.
   * @method rayQuery
   *
   *
   *
   *
   */
  List<num> rayQuery(Ray ray, Transform treeTransform, List<num> result) {
    // Use aabb query for now.

    // @todo implement real ray query which needs less lookups
    ray.getAABB(tmpAABB);
    tmpAABB.toLocalFrame(treeTransform, tmpAABB);
    this.aabbQuery(tmpAABB, result);
    return result;
  }

  /**
   * @method removeEmptyNodes
   */
  void removeEmptyNodes() {
    for (var i = this.children.length - 1; i >= 0; i--) {
      this.children[i].removeEmptyNodes();
      if (this.children[i].children.isEmpty && this.children[i].data == null) {
        this.children.removeRange(i, 1);
      }
    }
  }
}

/**
 * @class Octree
 *
 *
 *
 * @extends OctreeNode
 */
class Octree extends OctreeNode {
  num maxDepth;

  Octree({this.maxDepth, AABB aabb}) : super(root: null, aabb: aabb) {
    /* super call moved to initializer */;
    this.maxDepth ??= 8;
  }
}

final halfDiagonal = new Vec3();

final tmpAABB = new AABB();
