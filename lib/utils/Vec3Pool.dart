import "../math/Vec3.dart" show Vec3;
import "../utils/Pool.dart" show Pool;

/**
 * @class Vec3Pool
 * @constructor
 * @extends Pool
 */
class Vec3Pool extends Pool {
  dynamic type;

  Vec3Pool() : super() {
    /* super call moved to initializer */;
    this.type = Vec3();
  }

  /**
   * Construct a vector
   * @method constructObject
   *
   */
  Vec3 constructObject() {
    return new Vec3();
  }
}
