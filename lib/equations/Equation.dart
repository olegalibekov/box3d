import "../math/JacobianElement.dart" show JacobianElement;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import "../shapes/Shape.dart" show Shape;

/**
 * Equation base class
 * @class Equation
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 */
class Equation {
  num id;

  num minForce;

  num maxForce;

  Body bi;

  Body bj;

  Shape si;

  Shape sj;

  num a;

  num b;

  num eps;

  JacobianElement jacobianElementA;

  JacobianElement jacobianElementB;

  bool enabled;

  num multiplier;

  static num sId = 0;

  Equation(Body bi, Body bj, [minForce = -1e6, maxForce = 1e6]) {
    this.id = Equation.sId++;
    this.minForce = minForce;
    this.maxForce = maxForce;
    this.bi = bi;
    this.bj = bj;
    this.a = 0.0;
    this.b = 0.0;
    this.eps = 0.0;
    this.jacobianElementA = new JacobianElement();
    this.jacobianElementB = new JacobianElement();
    this.enabled = true;
    this.multiplier = 0;
    this.setSpookParams(1e7, 4, 1 / 60);
  }

  /**
   * Recalculates a,b,eps.
   * @method setSpookParams
   */
  void setSpookParams(num stiffness, num relaxation, num timeStep) {
    final d = relaxation;
    final k = stiffness;
    final h = timeStep;
    this.a = 4.0 / (h * (1 + 4 * d));
    this.b = (4.0 * d) / (1 + 4 * d);
    this.eps = 4.0 / (h * h * k * (1 + 4 * d));
  }

  /**
   * Computes the right hand side of the SPOOK equation
   * @method computeB
   *
   */
  num computeB(num a, [num b, num h]) {
    ///ToDo [num b, num h] and b = 1; h = 1; weren't. Check original file
    b = 1;
    h = 1;

    final GW = this.computeGW();
    final Gq = this.computeGq();
    final GiMf = this.computeGiMf();
    return -Gq * a - GW * b - GiMf * h;
  }

  /**
   * Computes G*q, where q are the generalized body coordinates
   * @method computeGq
   *
   */
  num computeGq() {
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final bi = this.bi;
    final bj = this.bj;
    final xi = bi.position;
    final xj = bj.position;
    return GA.spatial.dot(xi) + GB.spatial.dot(xj);
  }

  /**
   * Computes G*W, where W are the body velocities
   * @method computeGW
   *
   */
  num computeGW() {
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final bi = this.bi;
    final bj = this.bj;
    final vi = bi.velocity;
    final vj = bj.velocity;
    final wi = bi.angularVelocity;
    final wj = bj.angularVelocity;
    return GA.multiplyVectors(vi, wi) + GB.multiplyVectors(vj, wj);
  }

  /**
   * Computes G*Wlambda, where W are the body velocities
   * @method computeGWlambda
   *
   */
  num computeGWlambda() {
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final bi = this.bi;
    final bj = this.bj;
    final vi = bi.vlambda;
    final vj = bj.vlambda;
    final wi = bi.wlambda;
    final wj = bj.wlambda;
    return GA.multiplyVectors(vi, wi) + GB.multiplyVectors(vj, wj);
  }

  num computeGiMf() {
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final bi = this.bi;
    final bj = this.bj;
    final fi = bi.force;
    final ti = bi.torque;
    final fj = bj.force;
    final tj = bj.torque;
    final invMassi = bi.invMassSolve;
    final invMassj = bj.invMassSolve;
    fi.scale(invMassi, iMfi);
    fj.scale(invMassj, iMfj);
    bi.invInertiaWorldSolve.vmult(ti, invIi_vmult_taui);
    bj.invInertiaWorldSolve.vmult(tj, invIj_vmult_tauj);
    return GA.multiplyVectors(iMfi, invIi_vmult_taui) +
        GB.multiplyVectors(iMfj, invIj_vmult_tauj);
  }

  num computeGiMGt() {
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final bi = this.bi;
    final bj = this.bj;
    final invMassi = bi.invMassSolve;
    final invMassj = bj.invMassSolve;
    final invIi = bi.invInertiaWorldSolve;
    final invIj = bj.invInertiaWorldSolve;
    var result = invMassi + invMassj;
    invIi.vmult(GA.rotational, tmp);
    result += tmp.dot(GA.rotational);
    invIj.vmult(GB.rotational, tmp);
    result += tmp.dot(GB.rotational);
    return result;
  }

  /**
   * Add constraint velocity to the bodies.
   * @method addToWlambda
   *
   */
  void addToWlambda(num deltalambda) {
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final bi = this.bi;
    final bj = this.bj;
    final temp = addToWlambda_temp;
    // Add to linear velocity

    // v_lambda += inv(M) * delta_lamba * G
    bi.vlambda
        .addScaledVector(bi.invMassSolve * deltalambda, GA.spatial, bi.vlambda);
    bj.vlambda
        .addScaledVector(bj.invMassSolve * deltalambda, GB.spatial, bj.vlambda);
    // Add to angular velocity
    bi.invInertiaWorldSolve.vmult(GA.rotational, temp);
    bi.wlambda.addScaledVector(deltalambda, temp, bi.wlambda);
    bj.invInertiaWorldSolve.vmult(GB.rotational, temp);
    bj.wlambda.addScaledVector(deltalambda, temp, bj.wlambda);
  }

  /**
   * Compute the denominator part of the SPOOK equation: C = G*inv(M)*G' + eps
   * @method computeInvC
   *
   *
   */
  num computeC() {
    return this.computeGiMGt() + this.eps;
  }
}

/**
 * Computes G*inv(M)*f, where M is the mass matrix with diagonal blocks for each body, and f are the forces on the bodies.
 * @method computeGiMf
 *
 */
final iMfi = new Vec3();

final iMfj = new Vec3();

final invIi_vmult_taui = new Vec3();

final invIj_vmult_tauj = new Vec3();
/**
 * Computes G*inv(M)*G'
 * @method computeGiMGt
 *
 */
final tmp = new Vec3();

final addToWlambda_temp = new Vec3();
