import "../equations/Equation.dart" show Equation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

/**
 * Constrains the slipping in a contact along a tangent
 * @class FrictionEquation
 * @constructor
 * @author schteppe
 *
 *
 *
 * @extends Equation
 */
class FrictionEquation extends Equation {
  Vec3 ri;
  Vec3 rj;
  Vec3 t;

  FrictionEquation(Body bodyA, Body bodyB, num slipForce)
      : super(bodyA, bodyB, -slipForce, slipForce) {
    /* super call moved to initializer */;
    this.ri = new Vec3();
    this.rj = new Vec3();
    this.t = new Vec3();
  }

  ///ToDO was num computeB(num h) {
  num computeB(num h, [num temp1, num temp2]) {
    final a = this.a;
    final b = this.b;
    final bi = this.bi;
    final bj = this.bj;
    final ri = this.ri;
    final rj = this.rj;
    final rixt = FrictionEquation_computeB_temp1;
    final rjxt = FrictionEquation_computeB_temp2;
    final t = this.t;
    // Caluclate cross products
    ri.cross(t, rixt);
    rj.cross(t, rjxt);
    // G = [-t -rixt t rjxt]

    // And remember, this is a pure velocity constraint, g is always zero!
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    t.negate(GA.spatial);
    rixt.negate(GA.rotational);
    GB.spatial.copy(t);
    GB.rotational.copy(rjxt);
    final GW = this.computeGW();
    final GiMf = this.computeGiMf();
    final B = -GW * b - h * GiMf;
    return B;
  }
}

final FrictionEquation_computeB_temp1 = new Vec3();
final FrictionEquation_computeB_temp2 = new Vec3();
