import 'dart:math';

import "../math/Vec3.dart" show Vec3;
import "../equations/Equation.dart" show Equation;
import "../objects/Body.dart" show Body;

class ConeEquationOptions {
  num maxForce;
  Vec3 axisA;
  Vec3 axisB;
  num angle;
}

/**
 * Cone equation. Works to keep the given body world vectors aligned, or tilted within a given angle from each other.
 * @class ConeEquation
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 *
 *
 * @extends Equation
 */
class ConeEquation extends Equation {
  Vec3 axisA;

  Vec3 axisB;

  num angle;

  factory ConeEquation(Body bodyA, Body bodyB, ConeEquationOptions options) {
    if (options == null) options = ConeEquationOptions();
    final maxForce =
        !identical(options.maxForce, "undefined") ? options.maxForce : 1e6;
    /* super call moved to initializer */
    var axisA = options.axisA != null ? options.axisA.clone() : new Vec3(1, 0, 0);
    var axisB = options.axisB != null ? options.axisB.clone() : new Vec3(0, 1, 0);
    var angle = !identical(options.angle, "undefined") ? options.angle : 0;

    return ConeEquation._(
        bodyA, bodyB, -maxForce, maxForce, axisA, axisB, angle);
  }

  ConeEquation._(bodyA, bodyB, maxForceFirst, maxForceSecond, this.axisA,
      this.axisB, this.angle)
      : super(bodyA, bodyB, maxForceFirst, maxForceSecond);

  //ToDo was "num computeB(num h) {"
  num computeB(num h, [num test1, num test2]) {
    final a = this.a;
    final b = this.b;
    final ni = this.axisA;
    final nj = this.axisB;
    final nixnj = tmpVec1;
    final njxni = tmpVec2;
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    // Caluclate cross products
    ni.cross(nj, nixnj);
    nj.cross(ni, njxni);
    // The angle between two vector is:

    // cos(theta) = a * b / (length(a) * length(b) = { len(a) = len(b) = 1 } = a * b

    // g = a * b

    // gdot = (b x a) * wi + (a x b) * wj

    // G = [0 bxa 0 axb]

    // W = [vi wi vj wj]
    GA.rotational.copy(njxni);
    GB.rotational.copy(nixnj);
    final g = cos(this.angle) - ni.dot(nj);
    final GW = this.computeGW();
    final GiMf = this.computeGiMf();
    final B = -g * a - GW * b - h * GiMf;
    return B;
  }
}

final tmpVec1 = new Vec3();

final tmpVec2 = new Vec3();
