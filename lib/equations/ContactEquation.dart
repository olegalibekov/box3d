import "../equations/Equation.dart" show Equation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

/**
 * Contact/non-penetration constraint equation
 * @class ContactEquation
 * @constructor
 * @author schteppe
 * 
 * 
 * @extends Equation
 */
class ContactEquation extends Equation {
  num restitution;
  Vec3 ri;
  Vec3 rj;
  Vec3 ni;
  ContactEquation(Body bodyA, Body bodyB, [maxForce = 1e6])
      : super(bodyA, bodyB, 0, maxForce) {
    /* super call moved to initializer */;
    this.restitution = 0.0;
    this.ri = new Vec3();
    this.rj = new Vec3();
    this.ni = new Vec3();
  }

  //ToDo was "num computeB(num h) {"
  num computeB(num h, [num temp1, num temp2]) {
    final a = this.a;
    final b = this.b;
    final bi = this.bi;
    final bj = this.bj;
    final ri = this.ri;
    final rj = this.rj;
    final rixn = ContactEquation_computeB_temp1;
    final rjxn = ContactEquation_computeB_temp2;
    final vi = bi.velocity;
    final wi = bi.angularVelocity;
    final fi = bi.force;
    final taui = bi.torque;
    final vj = bj.velocity;
    final wj = bj.angularVelocity;
    final fj = bj.force;
    final tauj = bj.torque;
    final penetrationVec = ContactEquation_computeB_temp3;
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    final n = this.ni;
    // Caluclate cross products
    ri.cross(n, rixn);
    rj.cross(n, rjxn);
    // g = xj+rj -(xi+ri)

    // G = [ -ni  -rixn  ni  rjxn ]
    n.negate(GA.spatial);
    rixn.negate(GA.rotational);
    GB.spatial.copy(n);
    GB.rotational.copy(rjxn);
    // Calculate the penetration vector
    penetrationVec.copy(bj.position);
    penetrationVec.vadd(rj, penetrationVec);
    penetrationVec.vsub(bi.position, penetrationVec);
    penetrationVec.vsub(ri, penetrationVec);
    final g = n.dot(penetrationVec);
    // Compute iteration
    final ePlusOne = this.restitution + 1;
    final GW = ePlusOne * vj.dot(n) -
        ePlusOne * vi.dot(n) +
        wj.dot(rjxn) -
        wi.dot(rixn);
    final GiMf = this.computeGiMf();
    final B = -g * a - GW * b - h * GiMf;
    return B;
  }

  /**
   * Get the current relative velocity in the contact point.
   * @method getImpactVelocityAlongNormal
   * 
   */
  num getImpactVelocityAlongNormal() {
    final vi = ContactEquation_getImpactVelocityAlongNormal_vi;
    final vj = ContactEquation_getImpactVelocityAlongNormal_vj;
    final xi = ContactEquation_getImpactVelocityAlongNormal_xi;
    final xj = ContactEquation_getImpactVelocityAlongNormal_xj;
    final relVel = ContactEquation_getImpactVelocityAlongNormal_relVel;
    this.bi.position.vadd(this.ri, xi);
    this.bj.position.vadd(this.rj, xj);
    this.bi.getVelocityAtWorldPoint(xi, vi);
    this.bj.getVelocityAtWorldPoint(xj, vj);
    vi.vsub(vj, relVel);
    return this.ni.dot(relVel);
  }
}

final ContactEquation_computeB_temp1 = new Vec3();
final ContactEquation_computeB_temp2 = new Vec3();
final ContactEquation_computeB_temp3 = new Vec3();
final ContactEquation_getImpactVelocityAlongNormal_vi = new Vec3();
final ContactEquation_getImpactVelocityAlongNormal_vj = new Vec3();
final ContactEquation_getImpactVelocityAlongNormal_xi = new Vec3();
final ContactEquation_getImpactVelocityAlongNormal_xj = new Vec3();
final ContactEquation_getImpactVelocityAlongNormal_relVel = new Vec3();
