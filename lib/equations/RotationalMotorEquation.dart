import "../equations/Equation.dart" show Equation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

/**
 * Rotational motor constraint. Tries to keep the relative angular velocity of the bodies to a given value.
 * @class RotationalMotorEquation
 * @constructor
 * @author schteppe
 * 
 * 
 * 
 * @extends Equation
 */
class RotationalMotorEquation extends Equation {
  Vec3 axisA;
  Vec3 axisB;
  num targetVelocity;
  RotationalMotorEquation(Body bodyA, Body bodyB, [maxForce = 1e6])
      : super(bodyA, bodyB, -maxForce, maxForce) {
    /* super call moved to initializer */;
    this.axisA = new Vec3();
    this.axisB = new Vec3();
    this.targetVelocity = 0;
  }
  /// ToDo was num computeB(num h) {
  num computeB(num h, [num temp1, num temp2]) {
    final a = this.a;
    final b = this.b;
    final bi = this.bi;
    final bj = this.bj;
    final axisA = this.axisA;
    final axisB = this.axisB;
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    // g = 0

    // gdot = axisA * wi - axisB * wj

    // gdot = G * W = G * [vi wi vj wj]

    // =>

    // G = [0 axisA 0 -axisB]
    GA.rotational.copy(axisA);
    axisB.negate(GB.rotational);
    final GW = this.computeGW() - this.targetVelocity;
    final GiMf = this.computeGiMf();
    final B = -GW * b - h * GiMf;
    return B;
  }
}
