import 'dart:math';

import "../equations/Equation.dart" show Equation;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

class RotationalEquationOptions {
  num maxForce;
  Vec3 axisA;
  Vec3 axisB;
  num maxAngle;
}
/**
 * Rotational constraint. Works to keep the local vectors orthogonal to each other in world space.
 * @class RotationalEquation
 * @constructor
 * @author schteppe
 *
 *
 *
 *
 *
 * @extends Equation
 */
class RotationalEquation extends Equation {
  Vec3 axisA;

  Vec3 axisB;

  num maxAngle;

  factory RotationalEquation(Body bodyA, Body bodyB,
      RotationalEquationOptions options) {
    options ??= RotationalEquationOptions();
    final maxForce = !identical(options.maxForce, "undefined")
        ? options.maxForce
        : 1e6; /* super call moved to initializer */;
    return RotationalEquation._(bodyA, bodyB, -maxForce, maxForce, options);
  }

  RotationalEquation._(bodyA, bodyB, maxForceFirst, maxForceSecond, options)
      : super(bodyA, bodyB, maxForceFirst, maxForceSecond) {
    this.axisA = options.axisA ? options.axisA.clone() : new Vec3(1, 0, 0);
    this.axisB = options.axisB ? options.axisB.clone() : new Vec3(0, 1, 0);
    this.maxAngle = pi / 2;
  }


  ///ToDO was num computeB(num h) {
  num computeB(num h, [num temp1, num temp2]) {
    final a = this.a;
    final b = this.b;
    final ni = this.axisA;
    final nj = this.axisB;
    final nixnj = tmpVec1;
    final njxni = tmpVec2;
    final GA = this.jacobianElementA;
    final GB = this.jacobianElementB;
    // Caluclate cross products
    ni.cross(nj, nixnj);
    nj.cross(ni, njxni);
    // g = ni * nj

    // gdot = (nj x ni) * wi + (ni x nj) * wj

    // G = [0 njxni 0 nixnj]

    // W = [vi wi vj wj]
    GA.rotational.copy(njxni);
    GB.rotational.copy(nixnj);
    final g = cos(this.maxAngle) - ni.dot(nj);
    final GW = this.computeGW();
    final GiMf = this.computeGiMf();
    final B = -g * a - GW * b - h * GiMf;
    return B;
  }
}

final tmpVec1 = new Vec3 ();

final tmpVec2 = new Vec3 ();