import 'dart:math';

import "../constraints/HingeConstraint.dart" show HingeConstraint;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body, BodyOptions;
import "../shapes/Box.dart" show Box;
import "../shapes/Sphere.dart" show Sphere;
import "../world/World.dart" show World;

/**
 * Simple vehicle helper class with spherical rigid body wheels.
 * @class RigidVehicle
 * @constructor
 *
 */
class RigidVehicle {
  List<Body> wheelBodies;

  Vec3 coordinateSystem;

  Body chassisBody;

  List<HingeConstraint> constraints;

  List<Vec3> wheelAxes;

  List<num> wheelForces;

  RigidVehicle([Vec3 coordinateSystem, Body chassisBody]) {
    this.wheelBodies = [];
    this.coordinateSystem = coordinateSystem?.clone() ?? new Vec3(1, 2, 3);
    // No chassis body given. Create it!
    this.chassisBody =
        chassisBody ?? Body(BodyOptions(mass: 1, shape: Box(Vec3(5, 2, 0.5))));
    this.constraints = [];
    this.wheelAxes = [];
    this.wheelForces = [];
  }

  /**
   * Add a wheel
   * @method addWheel
   *
   *
   *
   *
   *
   *
   */
  num addWheel({Body body, Vec3 position, Vec3 axis}) {
    Body wheelBody;
    if (body != null) {
      wheelBody = body;
    } else {
      // No wheel body given. Create it!
      wheelBody = new Body(BodyOptions(mass: 1, shape: new Sphere(1.2)));
    }
    this.wheelBodies.add(wheelBody);
    this.wheelForces.add(0);
    // Position constrain wheels
    final zero = new Vec3();
    position = position?.clone() ?? new Vec3();
    // Set position locally to the chassis
    final worldPosition = new Vec3();
    this.chassisBody.pointToWorldFrame(position, worldPosition);
    wheelBody.position.set(worldPosition.x, worldPosition.y, worldPosition.z);
    // Constrain wheel
    axis = axis?.clone() ?? new Vec3(0, 1, 0);
    this.wheelAxes.add(axis);
    final hingeConstraint = new HingeConstraint(this.chassisBody, wheelBody, {
      "pivotA": position,
      "axisA": axis,
      "pivotB": Vec3.ZERO,
      "axisB": axis,
      "collideConnected": false
    });
    this.constraints.add(hingeConstraint);
    return this.wheelBodies.length - 1;
  }

  /**
   * Set the steering value of a wheel.
   * @method setSteeringValue
   *
   *
   * @todo check coordinateSystem
   */
  void setSteeringValue(num value, num wheelIndex) {
    // Set angle of the hinge axis
    final axis = this.wheelAxes[wheelIndex];
    final c = cos(value);
    final s = sin(value);
    final x = axis.x;
    final y = axis.y;
    this.constraints[wheelIndex].axisA.set(c * x - s * y, s * x + c * y, 0);
  }

  /**
   * Set the target rotational speed of the hinge constraint.
   * @method setMotorSpeed
   *
   *
   */
  void setMotorSpeed(num value, num wheelIndex) {
    final hingeConstraint = this.constraints[wheelIndex];
    hingeConstraint.enableMotor();
    hingeConstraint.motorTargetVelocity = value;
  }

  /**
   * Set the target rotational speed of the hinge constraint.
   * @method disableMotor
   *
   *
   */
  void disableMotor(num wheelIndex) {
    final hingeConstraint = this.constraints[wheelIndex];
    hingeConstraint.disableMotor();
  }

  /**
   * Set the wheel force to apply on one of the wheels each time step
   * @method setWheelForce
   *
   *
   */
  void setWheelForce(num value, num wheelIndex) {
    this.wheelForces[wheelIndex] = value;
  }

  /**
   * Apply a torque on one of the wheels.
   * @method applyWheelForce
   *
   *
   */
  void applyWheelForce(num value, num wheelIndex) {
    final axis = this.wheelAxes[wheelIndex];
    final wheelBody = this.wheelBodies[wheelIndex];
    final bodyTorque = wheelBody.torque;
    axis.scale(value, torque);
    wheelBody.vectorToWorldFrame(torque, torque);
    bodyTorque.vadd(torque, bodyTorque);
  }

  /**
   * Add the vehicle including its constraints to the world.
   * @method addToWorld
   *
   */
  void addToWorld(World world) {
    final constraints = this.constraints;
    final bodies = this.wheelBodies..addAll([this.chassisBody]);
    for (var i = 0; i < bodies.length; i++) {
      world.addBody(bodies[i]);
    }
    for (var i = 0; i < constraints.length; i++) {
      world.addConstraint(constraints[i]);
    }
    world.addEventListener("preStep", this._update);
  }

  void _update(event) {
    final wheelForces = this.wheelForces;
    for (var i = 0; i < wheelForces.length; i++) {
      this.applyWheelForce(wheelForces[i], i);
    }
  }

  /**
   * Remove the vehicle including its constraints from the world.
   * @method removeFromWorld
   *
   */
  void removeFromWorld(World world) {
    final constraints = this.constraints;
    final bodies = this.wheelBodies..addAll([this.chassisBody]);
    for (var i = 0; i < bodies.length; i++) {
      world.removeBody(bodies[i]);
    }
    for (var i = 0; i < constraints.length; i++) {
      world.removeConstraint(constraints[i]);
    }
  }

  /**
   * Get current rotational velocity of a wheel
   * @method getWheelSpeed
   *
   */
  num getWheelSpeed(num wheelIndex) {
    final axis = this.wheelAxes[wheelIndex];
    final wheelBody = this.wheelBodies[wheelIndex];
    final w = wheelBody.angularVelocity;
    this.chassisBody.vectorToWorldFrame(axis, worldAxis);
    return w.dot(worldAxis);
  }
}

final torque = new Vec3();

final worldAxis = new Vec3();
