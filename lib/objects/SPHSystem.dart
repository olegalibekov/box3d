import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import 'dart:math';

/**
 * Smoothed-particle hydrodynamics system
 * @class SPHSystem
 * @constructor
 */
class SPHSystem {
  List <Body> particles;

  num density;

  num smoothingRadius;

  num speedOfSound;

  num viscosity;

  num eps;

  List <num> pressures;

  List <num> densities;

  List <List <Body>> neighbors;

  SPHSystem() {
    this.particles = [];
    this.density = 1;
    this.smoothingRadius = 1;
    this.speedOfSound = 1;
    this.viscosity = 0.01;
    this.eps = 0.000001;
    // Stuff Computed per particle
    this.pressures = [];
    this.densities = [];
    this.neighbors = [];
  }

  /**
   * Add a particle to the system.
   * @method add
   *
   */
  void add(Body particle) {
    this.particles.add(particle);
    if (this.neighbors.length < this.particles.length) {
      this.neighbors.add([]);
    }
  }

  /**
   * Remove a particle from the system.
   * @method remove
   *
   */
  void remove(Body particle) {
    final idx = this.particles.indexOf(particle);
    if (!identical(idx, -1)) {
      this.particles.removeRange(idx, 1);
      if (this.neighbors.length > this.particles.length) {
        this.neighbors.removeLast();
      }
    }
  }

  void getNeighbors(Body particle, List <Body> neighbors) {
    final N = this.particles.length;
    final id = particle.id;
    final R2 = this.smoothingRadius * this.smoothingRadius;
    final dist = SPHSystem_getNeighbors_dist;
    for (var i = 0; !identical(i, N); i ++) {
      final p = this.particles [ i ];
      p.position.vsub(particle.position, dist);
      if (!identical(id, p.id) && dist.lengthSquared() < R2) {
        neighbors.add(p);
      }
    }
  }

  void update() {
    final N = this.particles.length;
    final dist = SPHSystem_update_dist;
    final cs = this.speedOfSound;
    final eps = this.eps;
    for (var i = 0; !identical(i, N); i ++) {
      final p = this.particles [ i ];
      final neighbors = this.neighbors [ i ];
      // Get neighbors
      neighbors.length = 0;
      this.getNeighbors(p, neighbors);
      neighbors.add(this.particles [ i ]);
      final numNeighbors = neighbors.length;
      // Accumulate density for the particle
      var sum = 0.0;
      for (var j = 0; !identical(j, numNeighbors); j ++) {
        //printf("Current particle has position %f %f %f\n",objects[id].pos.x(),objects[id].pos.y(),objects[id].pos.z());
        p.position.vsub(neighbors [ j ].position, dist);
        final len = dist.length();
        final weight = this.w(len);
        sum += neighbors [ j ].mass * weight;
      }
      // Save
      this.densities [ i ] = sum;
      this.pressures [ i ] = cs * cs * (this.densities [ i ] - this.density);
    }
    // Add forces

    // Sum to these accelerations
    final a_pressure = SPHSystem_update_a_pressure;
    final a_visc = SPHSystem_update_a_visc;
    final gradW = SPHSystem_update_gradW;
    final r_vec = SPHSystem_update_r_vec;
    final u = SPHSystem_update_u;
    for (var i = 0; !identical(i, N); i ++) {
      final particle = this.particles [ i ];
      a_pressure.set(0, 0, 0);
      a_visc.set(0, 0, 0);
      // Init vars
      var Pij;
      var nabla;
      var Vij;
      // Sum up for all other neighbors
      final neighbors = this.neighbors [ i ];
      final numNeighbors = neighbors.length;
      //printf("Neighbors: ");
      for (var j = 0; !identical(j, numNeighbors); j ++) {
        final neighbor = neighbors [ j ];
        //printf("%d ",nj);

        // Get r once for all..
        particle.position.vsub(neighbor.position, r_vec);
        final r = r_vec.length();
        // Pressure contribution
        Pij = -neighbor.mass * (this.pressures [ i ] /
            (this.densities [ i ] * this.densities [ i ] + eps) +
            this.pressures [ j ] /
                (this.densities [ j ] * this.densities [ j ] + eps));
        this.gradw(r_vec, gradW);
        // Add to pressure acceleration
        gradW.scale(Pij, gradW);
        a_pressure.vadd(gradW, a_pressure);
        // Viscosity contribution
        neighbor.velocity.vsub(particle.velocity, u);
        u.scale((1.0 / (0.0001 + this.densities [ i ] * this.densities [ j ])) *
            this.viscosity * neighbor.mass, u);
        nabla = this.nablaw(r);
        u.scale(nabla, u);
        // Add to viscosity acceleration
        a_visc.vadd(u, a_visc);
      }
      // Calculate force
      a_visc.scale(particle.mass, a_visc);
      a_pressure.scale(particle.mass, a_pressure);
      // Add force to particles
      particle.force.vadd(a_visc, particle.force);
      particle.force.vadd(a_pressure, particle.force);
    }
  }

  // Calculate the weight using the W(r) weightfunction
  num w(num r) {
    // 315
    final h = this.smoothingRadius;
    return (315.0 / (64.0 * pi * pow(h, 9))) * pow((h * h - r * r), 3);
  }

  // calculate gradient of the weight function
  void gradw(Vec3 rVec, Vec3 resultVec) {
    final r = rVec.length();
    final h = this.smoothingRadius;
    rVec.scale(
        (945.0 / (32.0 * pi * pow(h, 9))) * pow((h * h - r * r), 2), resultVec);
  }

  // Calculate nabla(W)
  num nablaw(num r) {
    final h = this.smoothingRadius;
    final nabla = (945.0 / (32.0 * pi * pow(h, 9))) * (h * h - r * r) *
        (7 * r * r - 3 * h * h);
    return nabla;
  }
}
/**
 * Get neighbors within smoothing volume, save in the array neighbors
 * @method getNeighbors
 *
 *
 */
final SPHSystem_getNeighbors_dist = new Vec3 ();
// Temp vectors for calculation
final SPHSystem_update_dist = new Vec3 ();

final SPHSystem_update_a_pressure = new Vec3 ();

final SPHSystem_update_a_visc = new Vec3 ();

final SPHSystem_update_gradW = new Vec3 ();

final SPHSystem_update_r_vec = new Vec3 ();

final SPHSystem_update_u = new Vec3 ();