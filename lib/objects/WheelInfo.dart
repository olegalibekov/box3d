import "../collision/RaycastResult.dart" show RaycastResult;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;
import "../utils/Utils.dart" show Utils;

Map<dynamic, dynamic> WheelInfoOptions = {
// chassisConnectionPointLocal?: Vec3
// chassisConnectionPointWorld?: Vec3
// directionLocal?: Vec3
// directionWorld?: Vec3
// axleLocal?: Vec3
// axleWorld?: Vec3
// suspensionRestLength?: number
// suspensionMaxLength?: number
// radius?: number
// suspensionStiffness?: number
// dampingCompression?: number
// dampingRelaxation?: number
// frictionSlip?: number
// steering?: number
// rotation?: number
// deltaRotation?: number
// rollInfluence?: number
// maxSuspensionForce?: number
// isFrontWheel?: boolean
// clippedInvContactDotSuspension?: number
// suspensionRelativeVelocity?: number
// suspensionForce?: number
// slipInfo?: number
// skidInfo?: number
// suspensionLength?: number
// maxSuspensionTravel?: number
// useCustomSlidingRotationalSpeed?: boolean
// customSlidingRotationalSpeed?: number
};

/// @class WheelInfo
/// @constructor
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
///
class WheelInfo {
  num maxSuspensionTravel;

  num customSlidingRotationalSpeed;

  bool useCustomSlidingRotationalSpeed;

  bool sliding;

  Vec3 chassisConnectionPointLocal;

  Vec3 chassisConnectionPointWorld;

  Vec3 directionLocal;

  Vec3 directionWorld;

  Vec3 axleLocal;

  Vec3 axleWorld;

  num suspensionRestLength;

  num suspensionMaxLength;

  num radius;

  num suspensionStiffness;

  num dampingCompression;

  num dampingRelaxation;

  num frictionSlip;

  num steering;

  num rotation;

  num deltaRotation;

  num rollInfluence;

  num maxSuspensionForce;

  num engineForce;

  num brake;

  bool isFrontWheel;

  num clippedInvContactDotSuspension;

  num suspensionRelativeVelocity;

  num suspensionForce;

  num slipInfo;

  num skidInfo;

  num suspensionLength;

  num sideImpulse;

  num forwardImpulse;

  WheelRaycastResult raycastResult;

  Transform worldTransform;

  bool isInContact;

  WheelInfo(Map<dynamic, dynamic> options) {
    options ??= {};

    ///ToDo check Utils.defaults everywhere. If no - add.
    options = Utils.defaults(options, {
      "chassisConnectionPointLocal": new Vec3(),
      "chassisConnectionPointWorld": new Vec3(),
      "directionLocal": new Vec3(),
      "directionWorld": new Vec3(),
      "axleLocal": new Vec3(),
      "axleWorld": new Vec3(),
      "suspensionRestLength": 1,
      "suspensionMaxLength": 2,
      "radius": 1,
      "suspensionStiffness": 100,
      "dampingCompression": 10,
      "dampingRelaxation": 10,
      "frictionSlip": 10000,
      "steering": 0,
      "rotation": 0,
      "deltaRotation": 0,
      "rollInfluence": 0.01,
      "maxSuspensionForce": double.maxFinite,
      "isFrontWheel": true,
      "clippedInvContactDotSuspension": 1,
      "suspensionRelativeVelocity": 0,
      "suspensionForce": 0,
      "slipInfo": 0,
      "skidInfo": 0,
      "suspensionLength": 0,
      "maxSuspensionTravel": 1,
      "useCustomSlidingRotationalSpeed": false,
      "customSlidingRotationalSpeed": -0.1
    });
    this.maxSuspensionTravel = options["maxSuspensionTravel"];
    this.customSlidingRotationalSpeed = options["customSlidingRotationalSpeed"];
    this.useCustomSlidingRotationalSpeed =
        options["useCustomSlidingRotationalSpeed"];
    this.sliding = false;
    this.chassisConnectionPointLocal =
        options["chassisConnectionPointLocal"].clone();
    this.chassisConnectionPointWorld =
        options["chassisConnectionPointWorld"].clone();
    this.directionLocal = options["directionLocal"].clone();
    this.directionWorld = options["directionWorld"].clone();
    this.axleLocal = options["axleLocal"].clone();
    this.axleWorld = options["axleWorld"].clone();
    this.suspensionRestLength = options["suspensionRestLength"];
    this.suspensionMaxLength = options["suspensionMaxLength"];
    this.radius = options["radius"];
    this.suspensionStiffness = options["suspensionStiffness"];
    this.dampingCompression = options["dampingCompression"];
    this.dampingRelaxation = options["dampingRelaxation"];
    this.frictionSlip = options["frictionSlip"];
    this.steering = 0;
    this.rotation = 0;
    this.deltaRotation = 0;
    this.rollInfluence = options["rollInfluence"];
    this.maxSuspensionForce = options["maxSuspensionForce"];
    this.engineForce = 0;
    this.brake = 0;
    this.isFrontWheel = options["isFrontWheel"];
    this.clippedInvContactDotSuspension = 1;
    this.suspensionRelativeVelocity = 0;
    this.suspensionForce = 0;
    this.slipInfo = 0;
    this.skidInfo = 0;
    this.suspensionLength = 0;
    this.sideImpulse = 0;
    this.forwardImpulse = 0;
    this.raycastResult = new WheelRaycastResult();
    this.worldTransform = new Transform();
    this.isInContact = false;
  }

  updateWheel(Body chassis) {
    final raycastResult = this.raycastResult;

    if (this.isInContact) {
      final project =
          raycastResult.hitNormalWorld.dot(raycastResult.directionWorld);
      raycastResult.hitPointWorld.vsub(chassis.position, relpos);
      chassis.getVelocityAtWorldPoint(relpos, chassis_velocity_at_contactPoint);
      final projVel =
          raycastResult.hitNormalWorld.dot(chassis_velocity_at_contactPoint);
      if (project >= -0.1) {
        this.suspensionRelativeVelocity = 0.0;
        this.clippedInvContactDotSuspension = 1.0 / 0.1;
      } else {
        final inv = -1 / project;
        this.suspensionRelativeVelocity = projVel * inv;
        this.clippedInvContactDotSuspension = inv;
      }
    } else {
      // Not in contact : position wheel in a nice (rest length) position
      raycastResult.suspensionLength = this.suspensionRestLength;
      this.suspensionRelativeVelocity = 0.0;
      raycastResult.directionWorld.scale(-1, raycastResult.hitNormalWorld);
      this.clippedInvContactDotSuspension = 1.0;
    }
  }
}

final chassis_velocity_at_contactPoint = new Vec3();

final relpos = new Vec3();

// Map<dynamic, dynamic> WheelRaycastResult = RaycastResult &
// Partial<{
//   suspensionLength: number
//   directionWorld: Vec3
//   groundObject: number
// }>

class WheelRaycastResult extends RaycastResult{
  num suspensionLength, groundObject;
  Vec3 directionWorld;
}
