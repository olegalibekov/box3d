import '../utils/EventContainer.dart';
import '../world/World.dart' show World;

import "../collision/AABB.dart" show AABB;
import "../material/Material.dart" show Material;
import "../math/Mat3.dart" show Mat3;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Vec3.dart" show Vec3;
import "../shapes/Box.dart" show Box;
import "../shapes/Shape.dart" show Shape;

enum BODY_TYPES {
  /// A dynamic body is fully simulated. Can be moved manually by the user, but normally they move according to forces. A dynamic body can collide with all body types. A dynamic body always has finite, non-zero mass.
  /// @static
  /// @property DYNAMIC
  /// @type {Number}
  DYNAMIC,

  /// A static body does not move during simulation and behaves as if it has infinite mass. Static bodies can be moved manually by setting the position of the body. The velocity of a static body is always zero. Static bodies do not collide with other static or kinematic bodies.
  /// @static
  /// @property STATIC
  /// @type {Number}
  STATIC,

  /// A kinematic body moves under simulation according to its velocity. They do not respond to forces. They can be moved manually, but normally a kinematic body is moved by setting its velocity. A kinematic body behaves as if it has infinite mass. Kinematic bodies do not collide with other static or kinematic bodies.
  /// @static
  /// @property KINEMATIC
  /// @type {Number}
  KINEMATIC,
}

enum BODY_SLEEP_STATES {
  /// @static
  /// @property AWAKE
  /// @type {number}
  AWAKE,
  SLEEPY,
  SLEEPING
}
enum BodyEvents {
  /// Dispatched after two bodies collide. This event is dispatched on each
  /// of the two bodies involved in the collision.
  /// @event collide
  collide,

  /// Dispatched after a sleeping body has woken up.
  /// @event wakeup
  wakeup,

  /// Dispatched after a body has gone in to the sleepy state.
  /// @event sleepy
  sleepy,

  /// Dispatched after a body has fallen asleep.
  /// @event sleep
  sleep
}

class BodyOptions {
  num collisionFilterGroup,
      collisionFilterMask,
      mass,
      linearDamping,
      sleepSpeedLimit,
      sleepTimeLimit,
      angularDamping;
  bool collisionResponse, allowSleep, fixedRotation;
  Vec3 position, velocity, angularVelocity, linearFactor, angularFactor;
  Material material;
  BODY_TYPES type;
  Quaternion quaternion;
  Shape shape;

  BodyOptions({this.mass, this.shape, this.position});
}

/// Base class for all body types.
/// @class Body
/// @constructor
/// @extends EventTarget
/// @example
///     const body = new Body({
///         mass: 1
///     });
///     const shape = new Sphere(1);
///     body.addShape(shape);
///     world.addBody(body);
class Body {
  num id;

  num index;

  World world;





  Vec3 vlambda;

  num collisionFilterGroup;

  num collisionFilterMask;

  bool collisionResponse;

  Vec3 position;

  Vec3 previousPosition;

  Vec3 interpolatedPosition;

  Vec3 initPosition;

  Vec3 velocity;

  Vec3 initVelocity;

  Vec3 force;

  num mass;

  num invMass;

  dynamic /* Material | null */ material;

  num linearDamping;

  BODY_TYPES type;

  bool allowSleep;

  BODY_SLEEP_STATES sleepState;

  num sleepSpeedLimit;

  num sleepTimeLimit;

  num timeLastSleepy;

  bool wakeUpAfterNarrowphase;

  Vec3 torque;

  Quaternion quaternion;

  Quaternion initQuaternion;

  Quaternion previousQuaternion;

  Quaternion interpolatedQuaternion;

  Vec3 angularVelocity;

  Vec3 initAngularVelocity;

  List<Shape> shapes;

  List<Vec3> shapeOffsets;

  List<Quaternion> shapeOrientations;

  Vec3 inertia;

  Vec3 invInertia;

  Mat3 invInertiaWorld;

  num invMassSolve;

  Vec3 invInertiaSolve;

  Mat3 invInertiaWorldSolve;

  bool fixedRotation;

  num angularDamping;

  Vec3 linearFactor;

  Vec3 angularFactor;

  AABB aabb;

  bool aabbNeedsUpdate;

  num boundingRadius;

  Vec3 wlambda;

  static num idCounter = 0;

  Body([BodyOptions options]) : super() {
    if (options == null) options = BodyOptions();
    /* super call moved to initializer */;
    this.id = Body.idCounter++;
    this.index = -1;
    this.world = null;
    this.vlambda = new Vec3();
    this.collisionFilterGroup =
        options.collisionFilterGroup is num ? options.collisionFilterGroup : 1;
    this.collisionFilterMask =
        options.collisionFilterMask is num ? options.collisionFilterMask : -1;
    this.collisionResponse =
        options.collisionResponse is num ? options.collisionResponse : true;
    this.position = new Vec3();
    this.previousPosition = new Vec3();
    this.interpolatedPosition = new Vec3();
    this.initPosition = new Vec3();
    if (options.position != null) {
      this.position.copy(options.position);
      this.previousPosition.copy(options.position);
      this.interpolatedPosition.copy(options.position);
      this.initPosition.copy(options.position);
    }
    this.velocity = new Vec3();
    if (options.velocity != null) {
      this.velocity.copy(options.velocity);
    }
    this.initVelocity = new Vec3();
    this.force = new Vec3();
    final mass = options.mass is num ? options.mass : 0;
    this.mass = mass;
    this.invMass = mass > 0 ? 1.0 / mass : 0;
    this.material = options.material;
    this.linearDamping =
        options.linearDamping is num ? options.linearDamping : 0.01;
    this.type = mass <= 0.0 ? BODY_TYPES.STATIC : BODY_TYPES.DYNAMIC;
    if (this.type == BODY_TYPES.STATIC) {
      this.type = options.type;
    }
    this.allowSleep = options.allowSleep ?? true;
    this.sleepState = BODY_SLEEP_STATES.AWAKE;
    this.sleepSpeedLimit = options.sleepSpeedLimit ?? 0.1;
    this.sleepTimeLimit = options.sleepTimeLimit ?? 1;
    this.timeLastSleepy = 0;
    this.wakeUpAfterNarrowphase = false;
    this.torque = new Vec3();
    this.quaternion = new Quaternion();
    this.initQuaternion = new Quaternion();
    this.previousQuaternion = new Quaternion();
    this.interpolatedQuaternion = new Quaternion();
    if (options.quaternion != null) {
      this.quaternion.copy(options.quaternion);
      this.initQuaternion.copy(options.quaternion);
      this.previousQuaternion.copy(options.quaternion);
      this.interpolatedQuaternion.copy(options.quaternion);
    }
    this.angularVelocity = new Vec3();
    if (options.angularVelocity != null) {
      this.angularVelocity.copy(options.angularVelocity);
    }
    this.initAngularVelocity = new Vec3();
    this.shapes = [];
    this.shapeOffsets = [];
    this.shapeOrientations = [];
    this.inertia = new Vec3();
    this.invInertia = new Vec3();
    this.invInertiaWorld = new Mat3();
    this.invMassSolve = 0;
    this.invInertiaSolve = new Vec3();
    this.invInertiaWorldSolve = new Mat3();
    this.fixedRotation = options.fixedRotation ?? false;
    this.angularDamping = options.angularDamping ?? 0.01;
    this.linearFactor = new Vec3(1, 1, 1);
    if (options.linearFactor != null) {
      this.linearFactor.copy(options.linearFactor);
    }
    this.angularFactor = new Vec3(1, 1, 1);
    if (options.angularFactor != null) {
      this.angularFactor.copy(options.angularFactor);
    }
    this.aabb = new AABB();
    this.aabbNeedsUpdate = true;
    this.boundingRadius = 0;
    this.wlambda = new Vec3();
    if (options.shape != null) {
      this.addShape(options.shape);
    }
    this.updateMassProperties();
  }

  /**
   * Wake the body up.
   * @method wakeUp
   */
  void wakeUp() {
    final prevState = this.sleepState;
    this.sleepState = BODY_SLEEP_STATES.AWAKE;
    this.wakeUpAfterNarrowphase = false;
    if (identical(prevState, BODY_SLEEP_STATES.SLEEPING)) {
      world.worldStreamsController.add(EventContainer(BodyEvents.wakeup, this));
    }
  }

  /**
   * Force body sleep
   * @method sleep
   */
  void sleep() {
    this.sleepState = BODY_SLEEP_STATES.SLEEPING;
    this.velocity.set(0, 0, 0);
    this.angularVelocity.set(0, 0, 0);
    this.wakeUpAfterNarrowphase = false;
  }

  /**
   * Called every timestep to update internal sleep timer and change sleep state if needed.
   * @method sleepTick
   *
   */
  void sleepTick(num time) {
    if (this.allowSleep) {
      final sleepState = this.sleepState;
      final speedSquared =
          this.velocity.lengthSquared() + this.angularVelocity.lengthSquared();
      final speedLimitSquared = this.sleepSpeedLimit * this.sleepSpeedLimit;
      if (identical(sleepState, BODY_SLEEP_STATES.AWAKE) &&
          speedSquared < speedLimitSquared) {
        this.sleepState = BODY_SLEEP_STATES.SLEEPY;
        this.timeLastSleepy = time;
        world.dispatchEvent(EventContainer(BodyEvents.sleepy, this));
      } else if (identical(sleepState, BODY_SLEEP_STATES.SLEEPY) &&
          speedSquared > speedLimitSquared) {
        this.wakeUp();
      } else if (identical(sleepState, BODY_SLEEP_STATES.SLEEPY) &&
          time - this.timeLastSleepy > this.sleepTimeLimit) {
        this.sleep();
        world.dispatchEvent(EventContainer(BodyEvents.sleep, this));
      }
    }
  }

  /**
   * If the body is sleeping, it should be immovable / have infinite mass during solve. We solve it by having a separate "solve mass".
   * @method updateSolveMassProperties
   */
  void updateSolveMassProperties() {
    if (identical(this.sleepState, BODY_SLEEP_STATES.SLEEPING) ||
        identical(this.type, BODY_TYPES.KINEMATIC)) {
      this.invMassSolve = 0;
      this.invInertiaSolve.setZero();
      this.invInertiaWorldSolve.setZero();
    } else {
      this.invMassSolve = this.invMass;
      this.invInertiaSolve.copy(this.invInertia);
      this.invInertiaWorldSolve.copy(this.invInertiaWorld);
    }
  }

  /// Convert a world point to local body frame.
  /// @method pointToLocalFrame
  Vec3 pointToLocalFrame(Vec3 worldPoint, [result]) {
    if (result == null) result = Vec3();
    worldPoint.vsub(this.position, result);
    this.quaternion.conjugate().vmult(result, result);
    return result;
  }

  /**
   * Convert a world vector to local body frame.
   * @method vectorToLocalFrame
   *
   *
   *
   */
  Vec3 vectorToLocalFrame(Vec3 worldVector, [result]) {
    if (result == null) result = Vec3();
    this.quaternion.conjugate().vmult(worldVector, result);
    return result;
  }

  /**
   * Convert a local body point to world frame.
   * @method pointToWorldFrame
   *
   *
   *
   */
  Vec3 pointToWorldFrame(Vec3 localPoint, [result]) {
    if (result == null) result = Vec3();
    this.quaternion.vmult(localPoint, result);
    result.vadd(this.position, result);
    return result;
  }

  /**
   * Convert a local body point to world frame.
   * @method vectorToWorldFrame
   *
   *
   *
   */
  Vec3 vectorToWorldFrame(Vec3 localVector, [result]) {
    if (result == null) result = Vec3();
    this.quaternion.vmult(localVector, result);
    return result;
  }

  /// Add a shape to the body with a local offset and orientation.
  /// @method addShape
  Body addShape(Shape shape, [Vec3 _offset, Quaternion _orientation]) {
    final offset = new Vec3();
    final orientation = new Quaternion();
    if (_offset != null) {
      offset.copy(_offset);
    }
    if (_orientation != null) {
      orientation.copy(_orientation);
    }
    this.shapes.add(shape);
    this.shapeOffsets.add(offset);
    this.shapeOrientations.add(orientation);
    this.updateMassProperties();
    this.updateBoundingRadius();
    this.aabbNeedsUpdate = true;
    shape.body = this;
    return this;
  }

  /// Update the bounding radius of the body. Should be done if any of the shapes are changed.
  /// @method updateBoundingRadius
  void updateBoundingRadius() {
    final shapes = this.shapes;
    final shapeOffsets = this.shapeOffsets;
    final N = shapes.length;
    var radius = 0.0;
    for (var i = 0; !identical(i, N); i++) {
      final shape = shapes[i];
      shape.updateBoundingSphereRadius();
      final offset = shapeOffsets[i].length();
      final r = shape.boundingSphereRadius;
      if (offset + r > radius) {
        radius = offset + r;
      }
    }
    this.boundingRadius = radius;
  }

  /// Updates the .aabb
  /// @method computeAABB
  /// @todo rename to updateAABB()
  void computeAABB() {
    final shapes = this.shapes;
    final shapeOffsets = this.shapeOffsets;
    final shapeOrientations = this.shapeOrientations;
    final N = shapes.length;
    final offset = tmpVec;
    final orientation = tmpQuat;
    final bodyQuat = this.quaternion;
    final aabb = this.aabb;
    final shapeAABB = computeAABB_shapeAABB;
    for (var i = 0; !identical(i, N); i++) {
      final shape = shapes[i];
      // Get shape world position
      bodyQuat.vmult(shapeOffsets[i], offset);
      offset.vadd(this.position, offset);
      // Get shape world quaternion
      bodyQuat.mult(shapeOrientations[i], orientation);
      // Get shape AABB
      shape.calculateWorldAABB(
          offset, orientation, shapeAABB.lowerBound, shapeAABB.upperBound);
      if (identical(i, 0)) {
        aabb.copy(shapeAABB);
      } else {
        aabb.extend(shapeAABB);
      }
    }
    this.aabbNeedsUpdate = false;
  }

  /// Update .inertiaWorld and .invInertiaWorld
  /// @method updateInertiaWorld
  void updateInertiaWorld([bool force = false]) {
    final I = this.invInertia;
    if (identical(I.x, I.y) && identical(I.y, I.z) && !force) {
      // If inertia M = s*I, where I is identity and s a scalar, then
      //    R*M*R' = R*(s*I)*R' = s*R*I*R' = s*R*R' = s*I = M
      // where R is the rotation matrix.
      // In other words, we don't have to transform the inertia if all
      // inertia diagonal entries are equal.
    } else {
      final m1 = uiw_m1;
      final m2 = uiw_m2;
      final m3 = uiw_m3;
      m1.setRotationFromQuaternion(this.quaternion);
      m1.transpose(m2);
      m1.scale(I, m1);
      m1.mmult(m2, this.invInertiaWorld);
    }
  }

  void applyForce(Vec3 force, Vec3 relativePoint) {
    if (!identical(this.type, BODY_TYPES.DYNAMIC)) {
      // Needed?
      return;
    }
    // Compute produced rotational force
    final rotForce = Body_applyForce_rotForce;
    relativePoint.cross(force, rotForce);
    // Add linear force
    this.force.vadd(force, this.force);
    // Add rotational force
    this.torque.vadd(rotForce, this.torque);
  }

  void applyLocalForce(Vec3 localForce, Vec3 localPoint) {
    if (!identical(this.type, BODY_TYPES.DYNAMIC)) {
      return;
    }
    final worldForce = Body_applyLocalForce_worldForce;
    final relativePointWorld = Body_applyLocalForce_relativePointWorld;
    // Transform the force vector to world space
    this.vectorToWorldFrame(localForce, worldForce);
    this.vectorToWorldFrame(localPoint, relativePointWorld);
    this.applyForce(worldForce, relativePointWorld);
  }

  void applyImpulse(Vec3 impulse, Vec3 relativePoint) {
    if (!identical(this.type, BODY_TYPES.DYNAMIC)) {
      return;
    }
    // Compute point position relative to the body center
    final r = relativePoint;
    // Compute produced central impulse velocity
    final velo = Body_applyImpulse_velo;
    velo.copy(impulse);
    velo.scale(this.invMass, velo);
    // Add linear impulse
    this.velocity.vadd(velo, this.velocity);
    // Compute produced rotational impulse velocity
    final rotVelo = Body_applyImpulse_rotVelo;
    r.cross(impulse, rotVelo);
    /*
     rotVelo.x *= this.invInertia.x;
     rotVelo.y *= this.invInertia.y;
     rotVelo.z *= this.invInertia.z;
     */
    this.invInertiaWorld.vmult(rotVelo, rotVelo);
    // Add rotational Impulse
    this.angularVelocity.vadd(rotVelo, this.angularVelocity);
  }

  void applyLocalImpulse(Vec3 localImpulse, Vec3 localPoint) {
    if (!identical(this.type, BODY_TYPES.DYNAMIC)) {
      return;
    }
    final worldImpulse = Body_applyLocalImpulse_worldImpulse;
    final relativePointWorld = Body_applyLocalImpulse_relativePoint;
    // Transform the force vector to world space
    this.vectorToWorldFrame(localImpulse, worldImpulse);
    this.vectorToWorldFrame(localPoint, relativePointWorld);
    this.applyImpulse(worldImpulse, relativePointWorld);
  }

  /**
   * Should be called whenever you change the body shape or mass.
   * @method updateMassProperties
   */
  void updateMassProperties() {
    final halfExtents = Body_updateMassProperties_halfExtents;
    this.invMass = this.mass > 0 ? 1.0 / this.mass : 0;
    final I = this.inertia;
    final fixed = this.fixedRotation;
    // Approximate with AABB box
    this.computeAABB();
    halfExtents.set(
        (this.aabb.upperBound.x - this.aabb.lowerBound.x) / 2,
        (this.aabb.upperBound.y - this.aabb.lowerBound.y) / 2,
        (this.aabb.upperBound.z - this.aabb.lowerBound.z) / 2);
    Box.calculateInertia(halfExtents, this.mass, I);
    this.invInertia.set(I.x > 0 && !fixed ? 1.0 / I.x : 0,
        I.y > 0 && !fixed ? 1.0 / I.y : 0, I.z > 0 && !fixed ? 1.0 / I.z : 0);
    this.updateInertiaWorld(true);
  }

  /**
   * Get world velocity of a point in the body.
   * @method getVelocityAtWorldPoint
   *
   *
   *
   */
  Vec3 getVelocityAtWorldPoint(Vec3 worldPoint, Vec3 result) {
    final r = new Vec3();
    worldPoint.vsub(this.position, r);
    this.angularVelocity.cross(r, result);
    this.velocity.vadd(result, result);
    return result;
  }

  /**
   * Move the body forward in time.
   *
   *
   *
   */
  void integrate(num dt, bool quatNormalize, bool quatNormalizeFast) {
    // Save previous position
    this.previousPosition.copy(this.position);
    this.previousQuaternion.copy(this.quaternion);
    if (!(identical(this.type, BODY_TYPES.DYNAMIC) ||
            identical(this.type, BODY_TYPES.KINEMATIC)) ||
        identical(this.sleepState, BODY_SLEEP_STATES.SLEEPING)) {
      // Only for dynamic
      return;
    }
    final velo = this.velocity;
    final angularVelo = this.angularVelocity;
    final pos = this.position;
    final force = this.force;
    final torque = this.torque;
    final quat = this.quaternion;
    final invMass = this.invMass;
    final invInertia = this.invInertiaWorld;
    final linearFactor = this.linearFactor;
    final iMdt = invMass * dt;
    velo.x += force.x * iMdt * linearFactor.x;
    velo.y += force.y * iMdt * linearFactor.y;
    velo.z += force.z * iMdt * linearFactor.z;
    final e = invInertia.elements;
    final angularFactor = this.angularFactor;
    final tx = torque.x * angularFactor.x;
    final ty = torque.y * angularFactor.y;
    final tz = torque.z * angularFactor.z;
    angularVelo.x += dt * (e[0] * tx + e[1] * ty + e[2] * tz);
    angularVelo.y += dt * (e[3] * tx + e[4] * ty + e[5] * tz);
    angularVelo.z += dt * (e[6] * tx + e[7] * ty + e[8] * tz);
    // Use new velocity  - leap frog
    pos.x += velo.x * dt;
    pos.y += velo.y * dt;
    pos.z += velo.z * dt;
    quat.integrate(this.angularVelocity, dt, this.angularFactor, quat);
    if (quatNormalize) {
      if (quatNormalizeFast) {
        quat.normalizeFast();
      } else {
        quat.normalize();
      }
    }
    this.aabbNeedsUpdate = true;
    // Update world inertia
    this.updateInertiaWorld();
  }
}

final tmpVec = new Vec3();

final tmpQuat = new Quaternion();

final computeAABB_shapeAABB = new AABB();

final uiw_m1 = new Mat3();

final uiw_m2 = new Mat3();

final uiw_m3 = new Mat3();
/**
 * Apply force to a world point. This could for example be a point on the Body surface. Applying force this way will add to Body.force and Body.torque.
 * @method applyForce
 *
 *
 */
final Body_applyForce_rotForce = new Vec3();
/**
 * Apply force to a local point in the body.
 * @method applyLocalForce
 *
 *
 */
final Body_applyLocalForce_worldForce = new Vec3();

final Body_applyLocalForce_relativePointWorld = new Vec3();
/**
 * Apply impulse to a world point. This could for example be a point on the Body surface. An impulse is a force added to a body during a short period of time (impulse = force * time). Impulses will be added to Body.velocity and Body.angularVelocity.
 * @method applyImpulse
 *
 *
 */
final Body_applyImpulse_velo = new Vec3();

final Body_applyImpulse_rotVelo = new Vec3();
/**
 * Apply locally-defined impulse to a local point in the body.
 * @method applyLocalImpulse
 *
 *
 */
final Body_applyLocalImpulse_worldImpulse = new Vec3();

final Body_applyLocalImpulse_relativePoint = new Vec3();

final Body_updateMassProperties_halfExtents = new Vec3();
