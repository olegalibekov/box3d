import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

/**
 * A spring, connecting two bodies.
 *
 * @class Spring
 * @constructor
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
class Spring {
  num restLength;

  num stiffness;

  num damping;

  Body bodyA;

  Body bodyB;

  Vec3 localAnchorA;

  Vec3 localAnchorB;

  Spring(Body bodyA, Body bodyB,
      {num restLength,
      num stiffness,
      num damping,
      Vec3 localAnchorA,
      Vec3 localAnchorB,
      Vec3 worldAnchorA,
      Vec3 worldAnchorB}) {
    this.restLength = restLength ?? 1;
    this.stiffness = stiffness ?? 100;
    this.damping = damping ?? 1;
    this.bodyA = bodyA;
    this.bodyB = bodyB;
    this.localAnchorA = new Vec3();
    this.localAnchorB = new Vec3();
    if (localAnchorA != null) {
      this.localAnchorA.copy(localAnchorA);
    }
    if (localAnchorB != null) {
      this.localAnchorB.copy(localAnchorB);
    }
    if (worldAnchorA != null) {
      this.setWorldAnchorA(worldAnchorA);
    }
    if (worldAnchorB != null) {
      this.setWorldAnchorB(worldAnchorB);
    }
  }

  /**
   * Set the anchor point on body A, using world coordinates.
   * @method setWorldAnchorA
   *
   */
  void setWorldAnchorA(Vec3 worldAnchorA) {
    this.bodyA.pointToLocalFrame(worldAnchorA, this.localAnchorA);
  }

  /**
   * Set the anchor point on body B, using world coordinates.
   * @method setWorldAnchorB
   *
   */
  void setWorldAnchorB(Vec3 worldAnchorB) {
    this.bodyB.pointToLocalFrame(worldAnchorB, this.localAnchorB);
  }

  /**
   * Get the anchor point on body A, in world coordinates.
   * @method getWorldAnchorA
   *
   */
  void getWorldAnchorA(Vec3 result) {
    this.bodyA.pointToWorldFrame(this.localAnchorA, result);
  }

  /**
   * Get the anchor point on body B, in world coordinates.
   * @method getWorldAnchorB
   *
   */
  void getWorldAnchorB(Vec3 result) {
    this.bodyB.pointToWorldFrame(this.localAnchorB, result);
  }

  /**
   * Apply the spring force to the connected bodies.
   * @method applyForce
   */
  void applyForce() {
    final k = this.stiffness;
    final d = this.damping;
    final l = this.restLength;
    final bodyA = this.bodyA;
    final bodyB = this.bodyB;
    final r = applyForce_r;
    final r_unit = applyForce_r_unit;
    final u = applyForce_u;
    final f = applyForce_f;
    final tmp = applyForce_tmp;
    final worldAnchorA = applyForce_worldAnchorA;
    final worldAnchorB = applyForce_worldAnchorB;
    final ri = applyForce_ri;
    final rj = applyForce_rj;
    final ri_x_f = applyForce_ri_x_f;
    final rj_x_f = applyForce_rj_x_f;
    // Get world anchors
    this.getWorldAnchorA(worldAnchorA);
    this.getWorldAnchorB(worldAnchorB);
    // Get offset points
    worldAnchorA.vsub(bodyA.position, ri);
    worldAnchorB.vsub(bodyB.position, rj);
    // Compute distance vector between world anchor points
    worldAnchorB.vsub(worldAnchorA, r);
    final rlen = r.length();
    r_unit.copy(r);
    r_unit.normalize();
    // Compute relative velocity of the anchor points, u
    bodyB.velocity.vsub(bodyA.velocity, u);
    // Add rotational velocity
    bodyB.angularVelocity.cross(rj, tmp);
    u.vadd(tmp, u);
    bodyA.angularVelocity.cross(ri, tmp);
    u.vsub(tmp, u);
    // F = - k * ( x - L ) - D * ( u )
    r_unit.scale(-k * (rlen - l) - d * u.dot(r_unit), f);
    // Add forces to bodies
    bodyA.force.vsub(f, bodyA.force);
    bodyB.force.vadd(f, bodyB.force);
    // Angular force
    ri.cross(f, ri_x_f);
    rj.cross(f, rj_x_f);
    bodyA.torque.vsub(ri_x_f, bodyA.torque);
    bodyB.torque.vadd(rj_x_f, bodyB.torque);
  }
}

final applyForce_r = new Vec3();

final applyForce_r_unit = new Vec3();

final applyForce_u = new Vec3();

final applyForce_f = new Vec3();

final applyForce_worldAnchorA = new Vec3();

final applyForce_worldAnchorB = new Vec3();

final applyForce_ri = new Vec3();

final applyForce_rj = new Vec3();

final applyForce_ri_x_f = new Vec3();

final applyForce_rj_x_f = new Vec3();

final applyForce_tmp = new Vec3();
