import "../shapes/Shape.dart" show Shape;
import "../math/Vec3.dart" show Vec3;
import "../math/Quaternion.dart" show Quaternion;
import 'Shape.dart';

/**
 * A plane, facing in the Z direction. The plane has its surface at z=0 and everything below z=0 is assumed to be solid plane. To make the plane face in some other direction than z, you must put it inside a Body and rotate that body. See the demos.
 * @class Plane
 * @constructor
 * @extends Shape
 * @author schteppe
 */
class Plane extends Shape {
  Vec3 worldNormal;
  bool worldNormalNeedsUpdate;
  num boundingSphereRadius;

  Plane() : super(SHAPE_TYPES.PLANE) {
    /* super call moved to initializer */;
    // World oriented normal
    this.worldNormal = new Vec3();
    this.worldNormalNeedsUpdate = true;
    this.boundingSphereRadius = double.maxFinite;
  }

  void computeWorldNormal(Quaternion quat) {
    final n = this.worldNormal;
    n.set(0, 0, 1);
    quat.vmult(n, n);
    this.worldNormalNeedsUpdate = false;
  }

  Vec3 calculateLocalInertia(num mass, target) {
    target ??= Vec3();
    return target;
  }

  num volume() {
    return (
        // The plane is infinite...
        double.maxFinite);
  }

  void calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    // The plane AABB is infinite, except if the normal is pointing along any axis
    tempNormal.set(0, 0, 1);
    quat.vmult(tempNormal, tempNormal);
    final maxVal = double.maxFinite;
    min.set(-maxVal, -maxVal, -maxVal);
    max.set(maxVal, maxVal, maxVal);
    if (identical(tempNormal.x, 1)) {
      max.x = pos.x;
    } else if (identical(tempNormal.x, -1)) {
      min.x = pos.x;
    }
    if (identical(tempNormal.y, 1)) {
      max.y = pos.y;
    } else if (identical(tempNormal.y, -1)) {
      min.y = pos.y;
    }
    if (identical(tempNormal.z, 1)) {
      max.z = pos.z;
    } else if (identical(tempNormal.z, -1)) {
      min.z = pos.z;
    }
  }

  void updateBoundingSphereRadius() {
    this.boundingSphereRadius = double.maxFinite;
  }
}

final tempNormal = new Vec3();
