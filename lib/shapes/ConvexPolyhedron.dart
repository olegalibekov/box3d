import 'dart:math';

import "../math/Quaternion.dart" show Quaternion;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;
import "../shapes/Shape.dart" show Shape, SHAPE_TYPES;

class ConvexPolyhedronContactPoint {
  final Vec3 point;

  final Vec3 normal;
  final num depth;

  ConvexPolyhedronContactPoint(this.point, this.normal, this.depth);
}

/**
 * A set of polygons describing a convex shape.
 * @class ConvexPolyhedron
 * @constructor
 * @extends Shape
 *  The shape MUST be convex for the code to work properly. No polygons may be coplanar (contained
 * in the same 3D plane), instead these should be merged into one polygon.
 *
 *
 *
 *
 * @author qiao / https://github.com/qiao (original author, see https://github.com/qiao/three.js/commit/85026f0c769e4000148a67d45a9e9b9c5108836f)
 * @author schteppe / https://github.com/schteppe
 * @see http://www.altdevblogaday.com/2011/05/13/contact-generation-between-3d-convex-meshes/
 *
 * @todo Move the clipping functions to ContactGenerator?
 * @todo Automatically merge coplanar polygons in constructor.
 */
class ConvexPolyhedron extends Shape {
  List<Vec3> vertices;

  List<List<num>> faces;

  List<Vec3> faceNormals;

  List<Vec3> worldVertices;

  bool worldVerticesNeedsUpdate;

  List<Vec3> worldFaceNormals;

  bool worldFaceNormalsNeedsUpdate;

  dynamic /* List < Vec3 > | null */ uniqueAxes;

  List<Vec3> uniqueEdges;

  /**
   * Get face normal given 3 vertices
   * @static
   * @method computeNormal
   *
   *
   *
   *
   */
  static void computeNormal(Vec3 va, Vec3 vb, Vec3 vc, Vec3 target) {
    final cb = new Vec3();
    final ab = new Vec3();
    vb.vsub(va, ab);
    vc.vsub(vb, cb);
    cb.cross(ab, target);
    if (!target.isZero()) {
      target.normalize();
    }
  }

  final List<num> maxminA = [];

  final List<num> maxminB = [];

  /**
   * Get max and min dot product of a convex hull at position (pos,quat) projected onto an axis.
   * Results are saved in the array maxmin.
   * @static
   * @method project
   *
   *
   *
   *
   *
   */
  static void project(ConvexPolyhedron shape, Vec3 axis, Vec3 pos,
      Quaternion quat, List<num> result) {
    final n = shape.vertices.length;
    final localAxis = new Vec3();
    var max = 0;
    var min = 0;
    final localOrigin = new Vec3();
    final vs = shape.vertices;
    localOrigin.setZero();
    // Transform the axis to local
    Transform.vectorToLocalFrame(pos, quat, axis, localAxis);
    Transform.pointToLocalFrame(pos, quat, localOrigin, localOrigin);
    final add = localOrigin.dot(localAxis);
    min = max = vs[0].dot(localAxis);
    for (var i = 1; i < n; i++) {
      final val = vs[i].dot(localAxis);
      if (val > max) {
        max = val;
      }
      if (val < min) {
        min = val;
      }
    }
    min -= add;
    max -= add;
    if (min > max) {
      // Inconsistent - swap
      final temp = min;
      min = max;
      max = temp;
    }
    // Output
    result[0] = max;
    result[1] = min;
  }

  ConvexPolyhedron(
      {List<Vec3> vertices,
      List<List<num>> faces,
      List<Vec3> normals,
      List<Vec3> axes,
      num boundingSphereRadius})
      : super(SHAPE_TYPES.CONVEXPOLYHEDRON) {
    /* super call moved to initializer */
    this.vertices = vertices;
    this.faces = faces;
    this.faceNormals = normals;
    if (identical(this.faceNormals?.length, 0)) {
      this.computeNormals();
    }
    if (boundingSphereRadius == null) {
      this.updateBoundingSphereRadius();
    } else {
      this.boundingSphereRadius = boundingSphereRadius;
    }
    this.worldVertices = [];
    this.worldVerticesNeedsUpdate = true;
    this.worldFaceNormals = [];
    this.worldFaceNormalsNeedsUpdate = true;
    this.uniqueAxes = axes;
    this.uniqueEdges = [];
    this.computeEdges();
  }

  /**
   * Computes uniqueEdges
   * @method computeEdges
   */
  void computeEdges() {
    final faces = this.faces;
    final vertices = this.vertices;
    final edges = this.uniqueEdges;
    edges.length = 0;
    final edge = new Vec3();
    for (var i = 0; !identical(i, faces.length); i++) {
      final face = faces[i];
      final numVertices = face.length;
      for (var j = 0; !identical(j, numVertices); j++) {
        final k = (j + 1) % numVertices;
        vertices[face[j]].vsub(vertices[face[k]], edge);
        edge.normalize();
        var found = false;
        for (var p = 0; !identical(p, edges.length); p++) {
          if (edges[p].almostEquals(edge) || edges[p].almostEquals(edge)) {
            found = true;
            break;
          }
        }
        if (!found) {
          edges.add(edge.clone());
        }
      }
    }
  }

  /**
   * Compute the normals of the faces. Will reuse existing Vec3 objects in the .faceNormals array if they exist.
   * @method computeNormals
   */
  void computeNormals() {
    this.faceNormals.length = this.faces.length;
    // Generate normals
    for (var i = 0; i < this.faces.length; i++) {
      // Check so all vertices exists for this face
      for (var j = 0; j < this.faces[i].length; j++) {
        if (this.vertices[this.faces[i][j]] == null) {
          throw ('''Vertex ${this.faces[i][j]} not found!''');
        }
      }
      final n = this.faceNormals[i] ?? Vec3();
      this.getFaceNormal(i, n);
      n.negate(n);
      this.faceNormals[i] = n;
      final vertex = this.vertices[this.faces[i][0]];
      if (n.dot(vertex) < 0) {
        print(
            '''.faceNormals[${i}] = Vec3(${n.toString()}) looks like it points into the shape? The vertices follow. Make sure they are ordered CCW around the normal, using the right hand rule.''');
        for (var j = 0; j < this.faces[i].length; j++) {
          print(
              '''.vertices[${this.faces[i][j]}] = Vec3(${this.vertices[this.faces[i][j]].toString()})''');
        }
      }
    }
  }

  /// Compute the normal of a face from its vertices
  /// @method getFaceNormal
  ///
  ///
  void getFaceNormal(num i, Vec3 target) {
    final f = this.faces[i];
    final va = this.vertices[f[0]];
    final vb = this.vertices[f[1]];
    final vc = this.vertices[f[2]];
    ConvexPolyhedron.computeNormal(va, vb, vc, target);
  }

  /// @method clipAgainstHull
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  void clipAgainstHull(
      Vec3 posA,
      Quaternion quatA,
      ConvexPolyhedron hullB,
      Vec3 posB,
      Quaternion quatB,
      Vec3 separatingNormal,
      num minDist,
      num maxDist,
      List<ConvexPolyhedronContactPoint> result) {
    final WorldNormal = new Vec3();
    var closestFaceB = -1;
    var dmax = -double.maxFinite;
    for (var face = 0; face < hullB.faces.length; face++) {
      WorldNormal.copy(hullB.faceNormals[face]);
      quatB.vmult(WorldNormal, WorldNormal);
      final d = WorldNormal.dot(separatingNormal);
      if (d > dmax) {
        dmax = d;
        closestFaceB = face;
      }
    }
    final worldVertsB1 = [];
    for (var i = 0; i < hullB.faces[closestFaceB].length; i++) {
      final b = hullB.vertices[hullB.faces[closestFaceB][i]];
      final worldb = new Vec3();
      worldb.copy(b);
      quatB.vmult(worldb, worldb);
      posB.vadd(worldb, worldb);
      worldVertsB1.add(worldb);
    }
    if (closestFaceB >= 0) {
      this.clipFaceAgainstHull(separatingNormal, posA, quatA, worldVertsB1,
          minDist, maxDist, result);
    }
  }

  /**
   * Find the separating axis between this hull and another
   * @method findSeparatingAxis
   *
   *
   *
   *
   *
   *
   *
   */
  bool findSeparatingAxis(ConvexPolyhedron hullB, Vec3 posA, Quaternion quatA,
      Vec3 posB, Quaternion quatB, Vec3 target,
      [dynamic /* List < num > | null */ faceListA,
      dynamic /* List < num > | null */ faceListB]) {
    final faceANormalWS3 = new Vec3();
    final Worldnormal1 = new Vec3();
    final deltaC = new Vec3();
    final worldEdge0 = new Vec3();
    final worldEdge1 = new Vec3();
    final Cross = new Vec3();
    var dmin = double.maxFinite;
    final hullA = this;
    var curPlaneTests = 0;
    if (!hullA.uniqueAxes) {
      final numFacesA = faceListA ? faceListA.length : hullA.faces.length;
      // Test face normals from hullA
      for (var i = 0; i < numFacesA; i++) {
        final fi = faceListA ? faceListA[i] : i;
        // Get world face normal
        faceANormalWS3.copy(hullA.faceNormals[fi]);
        quatA.vmult(faceANormalWS3, faceANormalWS3);
        final d =
            hullA.testSepAxis(faceANormalWS3, hullB, posA, quatA, posB, quatB);
        if (identical(d, false)) {
          return false;
        }
        if (d < dmin) {
          dmin = d;
          target.copy(faceANormalWS3);
        }
      }
    } else {
      // Test unique axes
      for (var i = 0; !identical(i, hullA.uniqueAxes.length); i++) {
        // Get world axis
        quatA.vmult(hullA.uniqueAxes[i], faceANormalWS3);
        final d =
            hullA.testSepAxis(faceANormalWS3, hullB, posA, quatA, posB, quatB);
        if (identical(d, false)) {
          return false;
        }
        if (d < dmin) {
          dmin = d;
          target.copy(faceANormalWS3);
        }
      }
    }
    if (!hullB.uniqueAxes) {
      // Test face normals from hullB
      final numFacesB = faceListB ? faceListB.length : hullB.faces.length;
      for (var i = 0; i < numFacesB; i++) {
        final fi = faceListB ? faceListB[i] : i;
        Worldnormal1.copy(hullB.faceNormals[fi]);
        quatB.vmult(Worldnormal1, Worldnormal1);
        curPlaneTests++;
        final d =
            hullA.testSepAxis(Worldnormal1, hullB, posA, quatA, posB, quatB);
        if (identical(d, false)) {
          return false;
        }
        if (d < dmin) {
          dmin = d;
          target.copy(Worldnormal1);
        }
      }
    } else {
      // Test unique axes in B
      for (var i = 0; !identical(i, hullB.uniqueAxes.length); i++) {
        quatB.vmult(hullB.uniqueAxes[i], Worldnormal1);
        curPlaneTests++;
        final d =
            hullA.testSepAxis(Worldnormal1, hullB, posA, quatA, posB, quatB);
        if (identical(d, false)) {
          return false;
        }
        if (d < dmin) {
          dmin = d;
          target.copy(Worldnormal1);
        }
      }
    }
    // Test edges
    for (var e0 = 0; !identical(e0, hullA.uniqueEdges.length); e0++) {
      // Get world edge
      quatA.vmult(hullA.uniqueEdges[e0], worldEdge0);
      for (var e1 = 0; !identical(e1, hullB.uniqueEdges.length); e1++) {
        // Get world edge 2
        quatB.vmult(hullB.uniqueEdges[e1], worldEdge1);
        worldEdge0.cross(worldEdge1, Cross);
        if (!Cross.almostZero()) {
          Cross.normalize();
          final dist =
              hullA.testSepAxis(Cross, hullB, posA, quatA, posB, quatB);
          if (identical(dist, false)) {
            return false;
          }
          if (dist < dmin) {
            dmin = dist;
            target.copy(Cross);
          }
        }
      }
    }
    posB.vsub(posA, deltaC);
    if (deltaC.dot(target) > 0.0) {
      target.negate(target);
    }
    return true;
  }

  /**
   * Test separating axis against two hulls. Both hulls are projected onto the axis and the overlap size is returned if there is one.
   * @method testSepAxis
   *
   *
   *
   *
   *
   *
   *
   */
  dynamic /* num |  */ testSepAxis(Vec3 axis, ConvexPolyhedron hullB, Vec3 posA,
      Quaternion quatA, Vec3 posB, Quaternion quatB) {
    final hullA = this;
    ConvexPolyhedron.project(hullA, axis, posA, quatA, maxminA);
    ConvexPolyhedron.project(hullB, axis, posB, quatB, maxminB);
    final maxA = maxminA[0];
    final minA = maxminA[1];
    final maxB = maxminB[0];
    final minB = maxminB[1];
    if (maxA < minB || maxB < minA) {
      return false;
    }
    final d0 = maxA - minB;
    final d1 = maxB - minA;
    final depth = d0 < d1 ? d0 : d1;
    return depth;
  }

  /**
   * @method calculateLocalInertia
   *
   *
   */
  calculateLocalInertia(num mass, Vec3 target) {
// Approximate with box inertia
// Exact inertia calculation is overkill, but see http://geometrictools.com/Documentation/PolyhedralMassProperties.pdf for the correct way to do it
    final aabbmax = new Vec3();
    final aabbmin = new Vec3();
    computeLocalAABB(aabbmin, aabbmax);
    final x = aabbmax.x - aabbmin.x;
    final y = aabbmax.y - aabbmin.y;
    final z = aabbmax.z - aabbmin.z;
    target.x = (1.0 / 12.0) * mass * (2 * y * 2 * y + 2 * z * 2 * z);
    target.y = (1.0 / 12.0) * mass * (2 * x * 2 * x + 2 * z * 2 * z);
    target.z = (1.0 / 12.0) * mass * (2 * y * 2 * y + 2 * x * 2 * x);
  }

  /**
   * @method getPlaneConstantOfFace
   *
   *
   */
  getPlaneConstantOfFace(num face_i) {
    final f = this.faces[face_i];
    final n = this.faceNormals[face_i];
    final v = this.vertices[f[0]];
    final c = -n.dot(v);
    return c;
  }

  /**
   * Clip a face against a hull.
   * @method clipFaceAgainstHull
   *
   *
   *
   *
   *
   *
   *
   */
  clipFaceAgainstHull(
      Vec3 separatingNormal,
      Vec3 posA,
      Quaternion quatA,
      List<Vec3> worldVertsB1,
      num minDist,
      num maxDist,
      List<ConvexPolyhedronContactPoint> result) {
    final faceANormalWS = new Vec3();
    final edge0 = new Vec3();
    final WorldEdge0 = new Vec3();
    final worldPlaneAnormal1 = new Vec3();
    final planeNormalWS1 = new Vec3();
    final worldA1 = new Vec3();
    final localPlaneNormal = new Vec3();
    final planeNormalWS = new Vec3();
    final hullA = this;
    final List<Vec3> worldVertsB2 = [];
    final pVtxIn = worldVertsB1;
    final pVtxOut = worldVertsB2;

    num closestFaceA = -1;
    num dmin = double.maxFinite;

    // Find the face with normal closest to the separating axis
    for (var face = 0; face < hullA.faces.length; face++) {
      faceANormalWS.copy(hullA.faceNormals[face]);
      quatA.vmult(faceANormalWS, faceANormalWS);
      final d = faceANormalWS.dot(separatingNormal);
      if (d < dmin) {
        dmin = d;
        closestFaceA = face;
      }
    }
    if (closestFaceA < 0) {
      return;
    }

    // Get the face and construct connected faces
    final polyA = hullA.faces[closestFaceA];
    List<num> connectedFaces = [];
    for (var i = 0; i < hullA.faces.length; i++) {
      for (var j = 0; j < hullA.faces[i].length; j++) {
        if (
            /* Sharing a vertex*/
            polyA.indexOf(hullA.faces[i][j]) != -1 &&
                /* Not the one we are looking for connections from */
                i != closestFaceA &&
                /* Not already added */
                connectedFaces.indexOf(i) == -1) {
          connectedFaces.add(i);
        }
      }
    }

    // Clip the polygon to the back of the planes of all faces of hull A,
    // that are adjacent to the witness face
    final numVerticesA = polyA.length;
    for (var i = 0; i < numVerticesA; i++) {
      final a = hullA.vertices[polyA[i]];
      final b = hullA.vertices[polyA[(i + 1) % numVerticesA]];
      a.vsub(b, edge0);
      WorldEdge0.copy(edge0);
      quatA.vmult(WorldEdge0, WorldEdge0);
      posA.vadd(WorldEdge0, WorldEdge0);
      worldPlaneAnormal1.copy(this.faceNormals[closestFaceA]);
      quatA.vmult(worldPlaneAnormal1, worldPlaneAnormal1);
      posA.vadd(worldPlaneAnormal1, worldPlaneAnormal1);
      WorldEdge0.cross(worldPlaneAnormal1, planeNormalWS1);
      planeNormalWS1.negate(planeNormalWS1);
      worldA1.copy(a);
      quatA.vmult(worldA1, worldA1);
      posA.vadd(worldA1, worldA1);

      final otherFace = connectedFaces[i];
      localPlaneNormal.copy(this.faceNormals[otherFace]);
      final localPlaneEq = this.getPlaneConstantOfFace(otherFace);
      planeNormalWS.copy(localPlaneNormal);
      quatA.vmult(planeNormalWS, planeNormalWS);
      final planeEqWS = localPlaneEq - planeNormalWS.dot(posA);

      // Clip face against our constructed plane
      this.clipFaceAgainstPlane(pVtxIn, pVtxOut, planeNormalWS, planeEqWS);

      // Throw away all clipped points, but save the remaining until next clip
      while (pVtxIn.length > 0) {
        pVtxIn.removeAt(0);
      }
      while (pVtxOut.length > 0) {
        pVtxIn.add(pVtxOut.removeAt(0));
      }
    }

    // only keep contact points that are behind the witness face
    localPlaneNormal.copy(this.faceNormals[closestFaceA]);

    final localPlaneEq = this.getPlaneConstantOfFace(closestFaceA);
    planeNormalWS.copy(localPlaneNormal);
    quatA.vmult(planeNormalWS, planeNormalWS);

    final planeEqWS = localPlaneEq - planeNormalWS.dot(posA);
    for (var i = 0; i < pVtxIn.length; i++) {
      var depth = planeNormalWS.dot(pVtxIn[i]) + planeEqWS; // ???

      if (depth <= minDist) {
        print('clamped: depth=${depth} to minDist=${minDist}');
        depth = minDist;
      }

      if (depth <= maxDist) {
        final point = pVtxIn[i];
        if (depth <= 1e-6) {
          final p = ConvexPolyhedronContactPoint(point, planeNormalWS, depth);
          result.add(p);
        }
      }
    }
  }

  /**
   * Clip a face in a hull against the back of a plane.
   * @method clipFaceAgainstPlane
   *
   *
   *
   *
   */
  List<Vec3> clipFaceAgainstPlane(List<Vec3> inVertices, List<Vec3> outVertices,
      Vec3 planeNormal, num planeConstant) {
    var nDotFirst;
    var nDotLast;
    final numVerts = inVertices.length;
    if (numVerts < 2) {
      return outVertices;
    }
    var firstVertex = inVertices[inVertices.length - 1];
    var lastVertex = inVertices[0];
    nDotFirst = planeNormal.dot(firstVertex) + planeConstant;
    for (var vi = 0; vi < numVerts; vi++) {
      lastVertex = inVertices[vi];
      nDotLast = planeNormal.dot(lastVertex) + planeConstant;
      if (nDotFirst < 0) {
        if (nDotLast < 0) {
// Start < 0, end < 0, so output lastVertex
          final newv = new Vec3();
          newv.copy(lastVertex);
          outVertices.add(newv);
        } else {
// Start < 0, end >= 0, so output intersection
          final newv = new Vec3();
          firstVertex.lerp(
              lastVertex, nDotFirst / (nDotFirst - nDotLast), newv);
          outVertices.add(newv);
        }
      } else {
        if (nDotLast < 0) {
// Start >= 0, end < 0 so output intersection and end
          final newv = new Vec3();
          firstVertex.lerp(
              lastVertex, nDotFirst / (nDotFirst - nDotLast), newv);
          outVertices.add(newv);
          outVertices.add(lastVertex);
        }
      }
      firstVertex = lastVertex;
      nDotFirst = nDotLast;
    }
    return outVertices;
  }

// Updates .worldVertices and sets .worldVerticesNeedsUpdate to false.
  computeWorldVertices(Vec3 position, Quaternion quat) {
    while (this.worldVertices.length < this.vertices.length) {
      this.worldVertices.add(new Vec3());
    }

    final verts = this.vertices;
    final worldVerts = this.worldVertices;
    for (var i = 0; i != this.vertices.length; i++) {
      quat.vmult(verts[i], worldVerts[i]);
      position.vadd(worldVerts[i], worldVerts[i]);
    }

    this.worldVerticesNeedsUpdate = false;
  }

  computeLocalAABB(Vec3 aabbmin, Vec3 aabbmax) {
    final vertices = this.vertices;

    aabbmin.set(double.maxFinite, double.maxFinite, double.maxFinite);
    aabbmax.set(-double.maxFinite, -double.maxFinite, -double.maxFinite);

    for (var i = 0; i < this.vertices.length; i++) {
      final v = vertices[i];
      if (v.x < aabbmin.x) {
        aabbmin.x = v.x;
      } else if (v.x > aabbmax.x) {
        aabbmax.x = v.x;
      }
      if (v.y < aabbmin.y) {
        aabbmin.y = v.y;
      } else if (v.y > aabbmax.y) {
        aabbmax.y = v.y;
      }
      if (v.z < aabbmin.z) {
        aabbmin.z = v.z;
      } else if (v.z > aabbmax.z) {
        aabbmax.z = v.z;
      }
    }
  }

  /// Updates .worldVertices and sets .worldVerticesNeedsUpdate to false.
  /// @method computeWorldFaceNormals
  ///

  computeWorldFaceNormals(Quaternion quat) {
    final N = this.faceNormals.length;
    while (this.worldFaceNormals.length < N) {
      this.worldFaceNormals.add(new Vec3());
    }

    final normals = this.faceNormals;
    final worldNormals = this.worldFaceNormals;
    for (var i = 0; i != N; i++) {
      quat.vmult(normals[i], worldNormals[i]);
    }

    this.worldFaceNormalsNeedsUpdate = false;
  }

  /**
   * @method updateBoundingSphereRadius
   */
  updateBoundingSphereRadius() {
    // Assume points are distributed with local (0,0,0) as center
    var max2 = 0.0;
    final verts = this.vertices;
    for (var i = 0; i != verts.length; i++) {
      final norm2 = verts[i].lengthSquared();
      if (norm2 > max2) {
        max2 = norm2;
      }
    }
    this.boundingSphereRadius = sqrt(max2);
  }

  /**
   * @method calculateWorldAABB
   *
   *
   *
   *
   */
  calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    final verts = this.vertices;
    num minx;
    num miny;
    num minz;
    num maxx;
    num maxy;
    num maxz;
    final tempWorldVertex = new Vec3();
    for (var i = 0; i < verts.length; i++) {
      tempWorldVertex.copy(verts[i]);
      quat.vmult(tempWorldVertex, tempWorldVertex);
      pos.vadd(tempWorldVertex, tempWorldVertex);
      final v = tempWorldVertex;
      if (minx == null || v.x < minx) {
        minx = v.x;
      }

      if (maxx == null || v.x > maxx) {
        maxx = v.x;
      }

      if (miny == null || v.y < miny) {
        miny = v.y;
      }

      if (maxy == null || v.y > maxy) {
        maxy = v.y;
      }

      if (minz == null || v.z < minz) {
        minz = v.z;
      }

      if (maxz == null || v.z > maxz) {
        maxz = v.z;
      }
    }
    min.set(minx, miny, minz);
    max.set(maxx, maxy, maxz);
  }

  /**
   * Get approximate convex volume
   * @method volume
   *
   */
  num volume() {
    return (4.0 * pi * this.boundingSphereRadius) / 3.0;
  }

  /**
   * Get an average of all the vertices positions
   * @method getAveragePointLocal
   *
   *
   */
  getAveragePointLocal(target) {
    if (target == null) target = Vec3();
    final verts = this.vertices;
    for (var i = 0; i < verts.length; i++) {
      target.vadd(verts[i], target);
    }
    target.scale(1 / verts.length, target);
    return target;
  }

  /**
   * Transform all local points. Will change the .vertices
   * @method transformAllPoints
   *
   *
   */
  transformAllPoints(offset, Vec3, quat, Quaternion) {
    final n = this.vertices.length;
    final verts = this.vertices;

    // Apply rotation
    if (quat) {
      // Rotate vertices
      for (var i = 0; i < n; i++) {
        final v = verts[i];
        quat.vmult(v, v);
      }
      // Rotate face normals
      for (var i = 0; i < this.faceNormals.length; i++) {
        final v = this.faceNormals[i];
        quat.vmult(v, v);
      }
      /*
            // Rotate edges
            for(let i=0; i<this.uniqueEdges.length; i++){
                const v = this.uniqueEdges[i];
                quat.vmult(v,v);
            }*/
    }

    // Apply offset
    if (offset) {
      for (var i = 0; i < n; i++) {
        final v = verts[i];
        v.vadd(offset, v);
      }
    }
  }

  /**
   * Checks whether p is inside the polyhedra. Must be in local coords.
   * The point lies outside of the convex hull of the other points if and only if the direction
   * of all the vectors from it to those other points are on less than one half of a sphere around it.
   * @method pointIsInside
   *
   *
   */
  pointIsInside(Vec3 p) {
    final verts = this.vertices;
    final faces = this.faces;
    final normals = this.faceNormals;
    final positiveResult = null;
    final pointInside = new Vec3();
    this.getAveragePointLocal(pointInside);
    for (var i = 0; i < this.faces.length; i++) {
      var n = normals[i];
      final v = verts[faces[i][0]];
// This dot product determines which side of the edge the point is
      final vToP = new Vec3();
      p.vsub(v, vToP);
      final r1 = n.dot(vToP);
      final vToPointInside = new Vec3();
      pointInside.vsub(v, vToPointInside);
      final r2 = n.dot(vToPointInside);
      if ((r1 < 0 && r2 > 0) || (r1 > 0 && r2 < 0)) {
        return false;
      }
    }
// If we got here, all dot products were of the same sign.
    return positiveResult ? 1 : -1;
  }
}
