import "../material/Material.dart" show Material;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Vec3.dart" show Vec3;
import "../objects/Body.dart" show Body;

enum SHAPE_TYPES {
  SPHERE,
  PLANE,
  BOX,
  COMPOUND,
  CONVEXPOLYHEDRON,
  HEIGHTFIELD,
  PARTICLE,
  CYLINDER,
  TRIMESH
}

class ShapeOptions {
  String type;
  bool collisionResponse;
  num collisionFilterGroup;
  num collisionFilterMask;
  Material material;
}

/**
 * Base class for shapes
 * @class Shape
 * @constructor
 *
 *
 *
 *
 *
 * @author schteppe
 */
class Shape {
  num id;

  SHAPE_TYPES type;

  num boundingSphereRadius;

  bool collisionResponse;

  num collisionFilterGroup;

  num collisionFilterMask;

  Material material;

  Body body;

  static num idCounter = 0;

  Shape(this.type, [ShapeOptions options]) {
    this.id = Shape.idCounter++;
    this.type ??= options?.type ?? SHAPE_TYPES.BOX;
    this.boundingSphereRadius = 0;
    this.collisionResponse = options?.collisionResponse ?? true;
    this.collisionFilterGroup = options

            /// TODO wtf?
            ?.collisionFilterGroup ??
        1;
    this.collisionFilterMask = options?.collisionFilterMask ?? -1;
    this.material = options?.material;
    this.body = null;
  }

  /**
   * Computes the bounding sphere radius. The result is stored in the property .boundingSphereRadius
   * @method updateBoundingSphereRadius
   */
  void updateBoundingSphereRadius() {
    throw '''computeBoundingSphereRadius() not implemented for shape type ${this.type}''';
  }

  /**
   * Get the volume of this shape
   * @method volume
   *
   */
  num volume() {
    throw '''volume() not implemented for shape type ${this.type}''';
  }

  /**
   * Calculates the inertia in the local frame for this shape.
   * @method calculateLocalInertia
   *
   *
   * @see http://en.wikipedia.org/wiki/List_of_moments_of_inertia
   */
  void calculateLocalInertia(num mass, Vec3 target) {
    throw '''calculateLocalInertia() not implemented for shape type ${this.type}''';
  }

  void calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    throw '''calculateWorldAABB() not implemented for shape type ${this.type}''';
  }
}

/**
 * The available shape types.
 * @static
 * @property types
 * @type {Object}
 */
