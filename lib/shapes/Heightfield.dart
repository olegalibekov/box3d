import 'dart:math';

import "../shapes/Shape.dart" show Shape;
import "../shapes/ConvexPolyhedron.dart" show ConvexPolyhedron;
import "../math/Vec3.dart" show Vec3;
import "../utils/Utils.dart" show Utils;
import "../collision/AABB.dart" show AABB;
import "../math/Quaternion.dart" show Quaternion;
import 'Shape.dart';

/**
 * Heightfield shape class. Height data is given as an array. These data points are spread out evenly with a given distance.
 * @class Heightfield
 * @extends Shape
 * @constructor
 *
 *
 *
 *
 *
 * @todo Should be possible to use along all axes, not just y
 * @todo should be possible to scale along all axes
 * @todo Refactor elementSize to elementSizeX and elementSizeY
 *
 * @example
 *     // Generate some height data (y-values).
 *     const data = [];
 *     for(let i = 0; i < 1000; i++){
 *         const y = 0.5 * Math.cos(0.2 * i);
 *         data.push(y);
 *     }
 *
 *     // Create the heightfield shape
 *     const heightfieldShape = new Heightfield(data, {
 *         elementSize: 1 // Distance between the data points in X and Y directions
 *     });
 *     const heightfieldBody = new Body();
 *     heightfieldBody.addShape(heightfieldShape);
 *     world.addBody(heightfieldBody);
 */
Map<dynamic, dynamic> HeightfieldOptions = {
// maxValue?: number | null
// minValue?: number | null
// elementSize?: number
};

class HeightfieldPillar {
  var convex, offset;

  HeightfieldPillar({this.convex, this.offset});
}

class Heightfield extends Shape {
  List <List <num>> data;

  dynamic /* num | null */ maxValue;

  dynamic /* num | null */ minValue;

  num elementSize;

  bool cacheEnabled;

  ConvexPolyhedron pillarConvex;

  Vec3 pillarOffset;

  Map <String, HeightfieldPillar> _cachedPillars;

  Heightfield(List <List <num>> data, Map<dynamic, dynamic> options)
      : super (SHAPE_TYPES.HEIGHTFIELD) {
    options ??= {};
    options = Utils.defaults(options, { "maxValue": null,
      "minValue": null,
      "elementSize": 1}); /* super call moved to initializer */;
    this.data = data;
    this.maxValue = options["maxValue"];
    this.minValue = options["minValue"];
    this.elementSize = options["elementSize"];
    if (identical(options["minValue"], null)) {
      this.updateMinValue();
    }
    if (identical(options["maxValue"], null)) {
      this.updateMaxValue();
    }
    this.cacheEnabled = true;
    this.pillarConvex = new ConvexPolyhedron ();
    this.pillarOffset = new Vec3 ();
    this.updateBoundingSphereRadius();
    // "i_j_isUpper" => { convex: ..., offset: ... }

    // for example:

    // _cachedPillars["0_2_1"]
    this._cachedPillars = {};
  }

  /**
   * Call whenever you change the data array.
   * @method update
   */
  void update() {
    this._cachedPillars = {};
  }

  /**
   * Update the .minValue property
   * @method updateMinValue
   */
  void updateMinValue() {
    final data = this.data;
    var minValue = data [ 0 ] [ 0 ];
    for (var i = 0; !identical(i, data.length); i ++) {
      for (var j = 0; !identical(j, data [ i ].length); j ++) {
        final v = data [ i ] [ j ];
        if (v < minValue) {
          minValue = v;
        }
      }
    }
    this.minValue = minValue;
  }

  /**
   * Update the .maxValue property
   * @method updateMaxValue
   */
  void updateMaxValue() {
    final data = this.data;
    var maxValue = data [ 0 ] [ 0 ];
    for (var i = 0; !identical(i, data.length); i ++) {
      for (var j = 0; !identical(j, data [ i ].length); j ++) {
        final v = data [ i ] [ j ];
        if (v > maxValue) {
          maxValue = v;
        }
      }
    }
    this.maxValue = maxValue;
  }

  /**
   * Set the height value at an index. Don't forget to update maxValue and minValue after you're done.
   * @method setHeightValueAtIndex
   *
   *
   *
   */
  void setHeightValueAtIndex(num xi, num yi, num value) {
    final data = this.data;
    data [ xi ] [ yi ] = value;
    // Invalidate cache
    this.clearCachedConvexTrianglePillar(xi, yi, false);
    if (xi > 0) {
      this.clearCachedConvexTrianglePillar(xi - 1, yi, true);
      this.clearCachedConvexTrianglePillar(xi - 1, yi, false);
    }
    if (yi > 0) {
      this.clearCachedConvexTrianglePillar(xi, yi - 1, true);
      this.clearCachedConvexTrianglePillar(xi, yi - 1, false);
    }
    if (yi > 0 && xi > 0) {
      this.clearCachedConvexTrianglePillar(xi - 1, yi - 1, true);
    }
  }

  /**
   * Get max/min in a rectangle in the matrix data
   * @method getRectMinMax
   *
   *
   *
   *
   *
   *
   */
  void getRectMinMax(num iMinX, num iMinY, num iMaxX, num iMaxY,
      [ List <num> result = const [] ]) {
    // Get max and min of the data
    final data = this.data;
    var max = this.minValue;
    for (var i = iMinX; i <= iMaxX; i ++) {
      for (var j = iMinY; j <= iMaxY; j ++) {
        final height = data [ i ] [ j ];
        if (height > max) {
          max = height;
        }
      }
    }
    result [ 0 ] = this.minValue;
    result [ 1 ] = max;
  }

  /**
   * Get the index of a local position on the heightfield. The indexes indicate the rectangles, so if your terrain is made of N x N height data points, you will have rectangle indexes ranging from 0 to N-1.
   * @method getIndexOfPosition
   *
   *
   *
   *
   *
   */
  bool getIndexOfPosition(num x, num y, List <num> result, bool clamp) {
    // Get the index of the data points to test against
    final w = this.elementSize;
    final data = this.data;
    var xi = (x / w).floor();
    var yi = (y / w).floor();
    result [ 0 ] = xi;
    result [ 1 ] = yi;
    if (clamp) {
      // Clamp index to edges
      if (xi < 0) {
        xi = 0;
      }
      if (yi < 0) {
        yi = 0;
      }
      if (xi >= data.length - 1) {
        xi = data.length - 1;
      }
      if (yi >= data [ 0 ].length - 1) {
        yi = data [ 0 ].length - 1;
      }
    }
    // Bail out if we are out of the terrain
    if (xi < 0 || yi < 0 || xi >= data.length - 1 ||
        yi >= data [ 0 ].length - 1) {
      return false;
    }
    return true;
  }

  bool getTriangleAt(num x, num y, bool edgeClamp, Vec3 a, Vec3 b, Vec3 c) {
    final idx = getHeightAt_idx;
    this.getIndexOfPosition(x, y, idx, edgeClamp);
    var xi = idx [ 0 ];
    var yi = idx [ 1 ];
    final data = this.data;
    if (edgeClamp) {
      xi = min(data.length - 2, max(0, xi));
      yi = min(data [ 0 ].length - 2, max(0, yi));
    }
    final elementSize = this.elementSize;
    final lowerDist2 = pow((x / elementSize - xi), 2) +
        pow((y / elementSize - yi), 2);
    final upperDist2 = pow((x / elementSize - (xi + 1)), 2) +
        pow((y / elementSize - (yi + 1)), 2);
    final upper = lowerDist2 > upperDist2;
    this.getTriangle(xi, yi, upper, a, b, c);
    return upper;
  }

  void getNormalAt(num x, num y, bool edgeClamp, Vec3 result) {
    final a = getNormalAt_a;
    final b = getNormalAt_b;
    final c = getNormalAt_c;
    final e0 = getNormalAt_e0;
    final e1 = getNormalAt_e1;
    this.getTriangleAt(x, y, edgeClamp, a, b, c);
    b.vsub(a, e0);
    c.vsub(a, e1);
    e0.cross(e1, result);
    result.normalize();
  }

  /**
   * Get an AABB of a square in the heightfield
   *
   *
   *
   */
  void getAabbAtIndex(num xi, num yi, { lowerBound, upperBound }) {
    final data = this.data;
    final elementSize = this.elementSize;
    lowerBound.set(xi * elementSize, yi * elementSize, data [ xi ] [ yi ]);
    upperBound.set((xi + 1) * elementSize, (yi + 1) * elementSize,
        data [ xi + 1 ] [ yi + 1 ]);
  }

  /**
   * Get the height in the heightfield at a given position
   *
   *
   *
   *
   */
  num getHeightAt(num x, num y, bool edgeClamp) {
    final data = this.data;
    final a = getHeightAt_a;
    final b = getHeightAt_b;
    final c = getHeightAt_c;
    final idx = getHeightAt_idx;
    this.getIndexOfPosition(x, y, idx, edgeClamp);
    var xi = idx [ 0 ];
    var yi = idx [ 1 ];
    if (edgeClamp) {
      xi = min(data.length - 2, max(0, xi));
      yi = min(data [ 0 ].length - 2, max(0, yi));
    }
    final upper = this.getTriangleAt(x, y, edgeClamp, a, b, c);
    barycentricWeights(
        x,
        y,
        a.x,
        a.y,
        b.x,
        b.y,
        c.x,
        c.y,
        getHeightAt_weights);
    final w = getHeightAt_weights;
    if (upper) {
      // Top triangle verts
      return data [ xi + 1 ] [ yi + 1 ] * w.x + data [ xi ] [ yi + 1 ] * w.y +
          data [ xi + 1 ] [ yi ] * w.z;
    } else {
      // Top triangle verts
      return data [ xi ] [ yi ] * w.x + data [ xi + 1 ] [ yi ] * w.y +
          data [ xi ] [ yi + 1 ] * w.z;
    }
  }

  String getCacheConvexTrianglePillarKey(num xi, num yi,
      bool getUpperTriangle) {
    return '''${ xi}_${ yi}_${ getUpperTriangle ? 1 : 0}''';
  }

  HeightfieldPillar getCachedConvexTrianglePillar(num xi, num yi,
      bool getUpperTriangle) {
    return this._cachedPillars [ this.getCacheConvexTrianglePillarKey(
        xi, yi, getUpperTriangle) ];
  }

  void setCachedConvexTrianglePillar(num xi, num yi, bool getUpperTriangle,
      ConvexPolyhedron convex, Vec3 offset) {
    this._cachedPillars [ this.getCacheConvexTrianglePillarKey(
        xi, yi, getUpperTriangle) ] =
        HeightfieldPillar(convex: convex, offset: offset);
  }

  void clearCachedConvexTrianglePillar(num xi, num yi, bool getUpperTriangle) {
    ;
  }

  /**
   * Get a triangle from the heightfield
   *
   *
   *
   *
   *
   *
   */
  void getTriangle(num xi, num yi, bool upper, Vec3 a, Vec3 b, Vec3 c) {
    final data = this.data;
    final elementSize = this.elementSize;
    if (upper) {
      // Top triangle verts
      a.set((xi + 1) * elementSize, (yi + 1) * elementSize,
          data [ xi + 1 ] [ yi + 1 ]);
      b.set(xi * elementSize, (yi + 1) * elementSize, data [ xi ] [ yi + 1 ]);
      c.set((xi + 1) * elementSize, yi * elementSize, data [ xi + 1 ] [ yi ]);
    } else {
      // Top triangle verts
      a.set(xi * elementSize, yi * elementSize, data [ xi ] [ yi ]);
      b.set((xi + 1) * elementSize, yi * elementSize, data [ xi + 1 ] [ yi ]);
      c.set(xi * elementSize, (yi + 1) * elementSize, data [ xi ] [ yi + 1 ]);
    }
  }

  /**
   * Get a triangle in the terrain in the form of a triangular convex shape.
   * @method getConvexTrianglePillar
   *
   *
   *
   */
  void getConvexTrianglePillar(num xi, num yi, bool getUpperTriangle) {
    var result = this.pillarConvex;
    var offsetResult = this.pillarOffset;
    if (this.cacheEnabled) {
      final data = this.getCachedConvexTrianglePillar(xi, yi, getUpperTriangle);
      if (data != null) {
        this.pillarConvex = data.convex;
        this.pillarOffset = data.offset;
        return;
      }
      result = new ConvexPolyhedron ();
      offsetResult = new Vec3 ();
      this.pillarConvex = result;
      this.pillarOffset = offsetResult;
    }
    final data = this.data;
    final elementSize = this.elementSize;
    final faces = result.faces;
    // Reuse verts if possible
    result.vertices.length = 6;
    for (var i = 0; i < 6; i ++) {
      if (result.vertices [ i ] == null) {
        result.vertices [ i ] = new Vec3 ();
      }
    }
    // Reuse faces if possible
    faces.length = 5;
    for (var i = 0; i < 5; i ++) {
      if (faces [ i ] == null) {
        faces [ i ] = [];
      }
    }
    final verts = result.vertices;
    final h = ([
      data[xi][yi],
      data[xi + 1][yi],
      data[xi][yi + 1],
      data[xi + 1][yi + 1]
    ].reduce(min) - this.minValue) / 2 +
        this.minValue;

    if (!getUpperTriangle) {
      // Center of the triangle pillar - all polygons are given relative to this one
      offsetResult.set((xi + 0.25) * elementSize, (yi + 0.25) * elementSize, h);
      // Top triangle verts
      verts [ 0 ].set(
          -0.25 * elementSize, -0.25 * elementSize, data [ xi ] [ yi ] - h);
      verts [ 1 ].set(
          0.75 * elementSize, -0.25 * elementSize, data [ xi + 1 ] [ yi ] - h);
      verts [ 2 ].set(
          -0.25 * elementSize, 0.75 * elementSize, data [ xi ] [ yi + 1 ] - h);
      // bottom triangle verts
      verts [ 3 ].set(-0.25 * elementSize, -0.25 * elementSize, -(h).abs() - 1);
      verts [ 4 ].set(0.75 * elementSize, -0.25 * elementSize, -(h).abs() - 1);
      verts [ 5 ].set(-0.25 * elementSize, 0.75 * elementSize, -(h).abs() - 1);
      // top triangle
      faces [ 0 ] [ 0 ] = 0;
      faces [ 0 ] [ 1 ] = 1;
      faces [ 0 ] [ 2 ] = 2;
      // bottom triangle
      faces [ 1 ] [ 0 ] = 5;
      faces [ 1 ] [ 1 ] = 4;
      faces [ 1 ] [ 2 ] = 3;
      // -x facing quad
      faces [ 2 ] [ 0 ] = 0;
      faces [ 2 ] [ 1 ] = 2;
      faces [ 2 ] [ 2 ] = 5;
      faces [ 2 ] [ 3 ] = 3;
      // -y facing quad
      faces [ 3 ] [ 0 ] = 1;
      faces [ 3 ] [ 1 ] = 0;
      faces [ 3 ] [ 2 ] = 3;
      faces [ 3 ] [ 3 ] = 4;
      // +xy facing quad
      faces [ 4 ] [ 0 ] = 4;
      faces [ 4 ] [ 1 ] = 5;
      faces [ 4 ] [ 2 ] = 2;
      faces [ 4 ] [ 3 ] = 1;
    } else {
      // Center of the triangle pillar - all polygons are given relative to this one
      offsetResult.set((xi + 0.75) * elementSize, (yi + 0.75) * elementSize, h);
      // Top triangle verts
      verts [ 0 ].set(0.25 * elementSize, 0.25 * elementSize,
          data [ xi + 1 ] [ yi + 1 ] - h);
      verts [ 1 ].set(
          -0.75 * elementSize, 0.25 * elementSize, data [ xi ] [ yi + 1 ] - h);
      verts [ 2 ].set(
          0.25 * elementSize, -0.75 * elementSize, data [ xi + 1 ] [ yi ] - h);
      // bottom triangle verts
      verts [ 3 ].set(0.25 * elementSize, 0.25 * elementSize, -(h).abs() - 1);
      verts [ 4 ].set(-0.75 * elementSize, 0.25 * elementSize, -(h).abs() - 1);
      verts [ 5 ].set(0.25 * elementSize, -0.75 * elementSize, -(h).abs() - 1);
      // Top triangle
      faces [ 0 ] [ 0 ] = 0;
      faces [ 0 ] [ 1 ] = 1;
      faces [ 0 ] [ 2 ] = 2;
      // bottom triangle
      faces [ 1 ] [ 0 ] = 5;
      faces [ 1 ] [ 1 ] = 4;
      faces [ 1 ] [ 2 ] = 3;
      // +x facing quad
      faces [ 2 ] [ 0 ] = 2;
      faces [ 2 ] [ 1 ] = 5;
      faces [ 2 ] [ 2 ] = 3;
      faces [ 2 ] [ 3 ] = 0;
      // +y facing quad
      faces [ 3 ] [ 0 ] = 3;
      faces [ 3 ] [ 1 ] = 4;
      faces [ 3 ] [ 2 ] = 1;
      faces [ 3 ] [ 3 ] = 0;
      // -xy facing quad
      faces [ 4 ] [ 0 ] = 1;
      faces [ 4 ] [ 1 ] = 4;
      faces [ 4 ] [ 2 ] = 5;
      faces [ 4 ] [ 3 ] = 2;
    }
    result.computeNormals();
    result.computeEdges();
    result.updateBoundingSphereRadius();
    this.setCachedConvexTrianglePillar(
        xi, yi, getUpperTriangle, result, offsetResult
    );
  }

  Vec3 calculateLocalInertia(num mass, [target]) {
    if (target == null) {
      target = Vec3();
    }
    target.set(0, 0, 0);
    return target;
  }

  num volume() {
    return (
        // The terrain is infinite
        double.maxFinite);
  }

  void calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    // TODO: do it properly
    min.set(-double.maxFinite, -double.maxFinite, -double.maxFinite);
    max.set(double.maxFinite, double.maxFinite, double.maxFinite);
  }

  void updateBoundingSphereRadius() {
    // Use the bounding box of the min/max values
    final data = this.data;
    final s = this.elementSize;
    this.boundingSphereRadius = new Vec3(
        data.length * s,
        data[0].length * s,
        max((this.maxValue).abs() as num, (this.minValue).abs() as num)
    ).length();
  }

  /**
   * Sets the height values from an image. Currently only supported in browser.
   * @method setHeightsFromImage
   *
   *
   */
  // void setHeightsFromImage(HTMLImageElement image, Vec3 scale) {
  //   final = scale ; final canvas = document . createElement ( "canvas" ) ; canvas . width = image . width ; canvas . height = image . height ; final context = ; context . drawImage ( image , 0 , 0 ) ; final imageData = context . getImageData ( 0 , 0 , image . width , image . height ) ; final matrix = this . data ; matrix . length = 0 ; this . elementSize = ( x ).abs() / imageData . width ; for ( var i = 0 ; i < imageData . height ; i ++ ) { final row = [ ] ; for ( var j = 0 ; j < imageData . width ; j ++ ) { final a = imageData . data [ ( i * imageData . height + j ) * 4 ] ; final b = imageData . data [ ( i * imageData . height + j ) * 4 + 1 ] ; final c = imageData . data [ ( i * imageData . height + j ) * 4 + 2 ] ; final height = ( ( a + b + c ) / 4 / 255 ) * z ; if ( x < 0 ) { row.add ( height ) ; } else { row . unshift ( height ) ; } } if ( y < 0 ) { matrix . unshift ( row ) ; } else { matrix.add ( row ) ; } } this . updateMaxValue ( ) ; this . updateMinValue ( );
  //   this
  //   .
  //   update
  //   (
  //   );
  // }
}

final List <num> getHeightAt_idx = [];

final getHeightAt_weights = new Vec3 ();

final getHeightAt_a = new Vec3 ();

final getHeightAt_b = new Vec3 ();

final getHeightAt_c = new Vec3 ();

final getNormalAt_a = new Vec3 ();

final getNormalAt_b = new Vec3 ();

final getNormalAt_c = new Vec3 ();

final getNormalAt_e0 = new Vec3 ();

final getNormalAt_e1 = new Vec3 ();
// from https://en.wikipedia.org/wiki/Barycentric_coordinate_system
void barycentricWeights(num x, num y, num ax, num ay, num bx, num by, num cx,
    num cy, Vec3 result) {
  result.x = ((by - cy) * (x - cx) + (cx - bx) * (y - cy)) /
      ((by - cy) * (ax - cx) + (cx - bx) * (ay - cy));
  result.y = ((cy - ay) * (x - cx) + (ax - cx) * (y - cy)) /
      ((by - cy) * (ax - cx) + (cx - bx) * (ay - cy));
  result.z = 1 - result.x - result.y;
}