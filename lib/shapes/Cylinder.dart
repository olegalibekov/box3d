import "../shapes/ConvexPolyhedron.dart" show ConvexPolyhedron;
import "../math/Vec3.dart" show Vec3;
import 'dart:math' as Math;

/**
 * @class Cylinder
 * @constructor
 * @extends ConvexPolyhedron
 * @author schteppe / https://github.com/schteppe
 *
 *
 *
 *
 */
class Cylinder extends ConvexPolyhedron {
  factory Cylinder(
      num radiusTop, num radiusBottom, num height, num numSegments) {
    final N = numSegments;
    final vertices = [];
    final axes = [];
    final faces = [];
    final bottomface = [];
    final topface = [];
    final cos = Math.cos;
    final sin = Math.sin;
    // First bottom point
    vertices.add(
        new Vec3(-radiusBottom * sin(0), -height * 0.5, radiusBottom * cos(0)));
    bottomface.add(0);
    // First top point
    vertices
        .add(new Vec3(-radiusTop * sin(0), height * 0.5, radiusTop * cos(0)));
    topface.add(1);
    for (var i = 0; i < N; i++) {
      final theta = ((2 * Math.pi) / N) * (i + 1);
      final thetaN = ((2 * Math.pi) / N) * (i + 0.5);
      if (i < N - 1) {
        // Bottom
        vertices.add(new Vec3(-radiusBottom * sin(theta), -height * 0.5,
            radiusBottom * cos(theta)));
        bottomface.add(2 * i + 2);
        // Top
        vertices.add(new Vec3(
            -radiusTop * sin(theta), height * 0.5, radiusTop * cos(theta)));
        topface.add(2 * i + 3);
        // Face
        faces.add([2 * i, 2 * i + 1, 2 * i + 3, 2 * i + 2]);
      } else {
        faces.add([2 * i, 2 * i + 1, 1, 0]);
      }
      // Axis: we can cut off half of them if we have even number of segments
      if (identical(N % 2, 1) || i < N / 2) {
        axes.add(new Vec3(-sin(thetaN), 0, cos(thetaN)));
      }
    }
    faces.add(bottomface);
    axes.add(new Vec3(0, 1, 0));
    // Reorder top face
    final temp = [];
    for (var i = 0; i < topface.length; i++) {
      temp.add(topface[topface.length - i - 1]);
    }
    faces.add(temp);
    /* super call moved to initializer */

    return Cylinder._(vertices, faces, axes);
  }

  Cylinder._(vertices, faces, axes)
      : super(vertices: vertices, faces: faces, axes: axes) {}
}
