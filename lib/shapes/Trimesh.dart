import 'dart:math';
import 'dart:typed_data';

import "../collision/AABB.dart" show AABB;
import "../math/Quaternion.dart" show Quaternion;
import "../math/Transform.dart" show Transform;
import "../math/Vec3.dart" show Vec3;
import "../shapes/Shape.dart" show Shape, SHAPE_TYPES;
import "../utils/Octree.dart" show Octree;

/// @class Trimesh
/// @constructor
///
///
/// @extends Shape
/// @example
///     // How to make a mesh with a single triangle
///     const vertices = [
///         0, 0, 0, // vertex 0
///         1, 0, 0, // vertex 1
///         0, 1, 0  // vertex 2
///     ];
///     const indices = [
///         0, 1, 2  // triangle 0
///     ];
///     const trimeshShape = new Trimesh(vertices, indices);
class Trimesh extends Shape {
  Float32List vertices;

  Int16List indices;

  Float32List normals;

  AABB aabb;

  dynamic /* Int16Array | null */ edges;

  Vec3 scale;

  Octree tree;

  /// Get face normal given 3 vertices
  /// @static
  /// @method computeNormal
  static void computeNormal(Vec3 va, Vec3 vb, Vec3 vc, Vec3 target) {
    vb.vsub(va, ab);
    vc.vsub(vb, cb);
    cb.cross(ab, target);
    if (!target.isZero()) {
      target.normalize();
    }
  }

  /// Create a Trimesh instance, shaped as a torus.
  /// @static
  /// @method createTorus
  static Trimesh createTorus(
      [radius = 1,
      tube = 0.5,
      radialSegments = 8,
      tubularSegments = 6,
      arc = pi * 2]) {
    final vertices = [];
    final indices = [];
    for (var j = 0; j <= radialSegments; j++) {
      for (var i = 0; i <= tubularSegments; i++) {
        final u = (i / tubularSegments) * arc;
        final v = (j / radialSegments) * pi * 2;
        final x = (radius + tube * cos(v)) * cos(u);
        final y = (radius + tube * cos(v)) * sin(u);
        final z = tube * sin(v);
        vertices.addAll([x, y, z]);
      }
    }
    for (var j = 1; j <= radialSegments; j++) {
      for (var i = 1; i <= tubularSegments; i++) {
        final a = (tubularSegments + 1) * j + i - 1;
        final b = (tubularSegments + 1) * (j - 1) + i - 1;
        final c = (tubularSegments + 1) * (j - 1) + i;
        final d = (tubularSegments + 1) * j + i;
        indices.addAll([a, b, d]);
        indices.addAll([b, c, d]);
      }
    }
    return new Trimesh(vertices, indices);
  }

  Trimesh(List<num> vertices, List<num> indices) : super(SHAPE_TYPES.TRIMESH) {
    /* super call moved to initializer */;
    this.vertices = Float32List.fromList(vertices);
    this.indices = new Int16List.fromList(indices);
    this.normals = new Float32List(indices.length);
    this.aabb = new AABB();
    this.edges = null;
    this.scale = new Vec3(1, 1, 1);
    this.tree = new Octree();
    this.updateEdges();
    this.updateNormals();
    this.updateAABB();
    this.updateBoundingSphereRadius();
    this.updateTree();
  }

  /// @method updateTree
  void updateTree() {
    final tree = this.tree;
    tree.reset();
    tree.aabb.copy(this.aabb);
    final scale = this.scale;
    tree.aabb.lowerBound.x *= 1 / scale.x;
    tree.aabb.lowerBound.y *= 1 / scale.y;
    tree.aabb.lowerBound.z *= 1 / scale.z;
    tree.aabb.upperBound.x *= 1 / scale.x;
    tree.aabb.upperBound.y *= 1 / scale.y;
    tree.aabb.upperBound.z *= 1 / scale.z;
    // Insert all triangles
    final triangleAABB = new AABB();
    final a = new Vec3();
    final b = new Vec3();
    final c = new Vec3();
    final points = [a, b, c];
    for (var i = 0; i < this.indices.length / 3; i++) {
      //this.getTriangleVertices(i, a, b, c);

      // Get unscaled triangle verts
      final i3 = i * 3;
      this._getUnscaledVertex(this.indices[i3], a);
      this._getUnscaledVertex(this.indices[i3 + 1], b);
      this._getUnscaledVertex(this.indices[i3 + 2], c);
      triangleAABB.setFromPoints(points);
      tree.insert(triangleAABB, i);
    }
    tree.removeEmptyNodes();
  }

  /// Get triangles in a local AABB from the trimesh.
  /// @method getTrianglesInAABB
  List<num> getTrianglesInAABB(AABB aabb, List<num> result) {
    unscaledAABB.copy(aabb);
    // Scale it to local
    final scale = this.scale;
    final isx = scale.x;
    final isy = scale.y;
    final isz = scale.z;
    final l = unscaledAABB.lowerBound;
    final u = unscaledAABB.upperBound;
    l.x /= isx;
    l.y /= isy;
    l.z /= isz;
    u.x /= isx;
    u.y /= isy;
    u.z /= isz;
    return this.tree.aabbQuery(unscaledAABB, result);
  }

  /// @method setScale
  ///
  void setScale(Vec3 scale) {
    final wasUniform = identical(this.scale.x, this.scale.y) &&
        identical(this.scale.y, this.scale.z);
    final isUniform =
        identical(scale.x, scale.y) && identical(scale.y, scale.z);
    if (!(wasUniform && isUniform)) {
      // Non-uniform scaling. Need to update normals.
      this.updateNormals();
    }
    this.scale.copy(scale);
    this.updateAABB();
    this.updateBoundingSphereRadius();
  }

  /// Compute the normals of the faces. Will save in the .normals array.
  /// @method updateNormals
  void updateNormals() {
    final n = computeNormals_n;
    // Generate normals
    final normals = this.normals;
    for (var i = 0; i < this.indices.length / 3; i++) {
      final i3 = i * 3;
      final a = this.indices[i3];
      final b = this.indices[i3 + 1];
      final c = this.indices[i3 + 2];
      this.getVertex(a, va);
      this.getVertex(b, vb);
      this.getVertex(c, vc);
      Trimesh.computeNormal(vb, va, vc, n);
      normals[i3] = n.x;
      normals[i3 + 1] = n.y;
      normals[i3 + 2] = n.z;
    }
  }

  /// Update the .edges property
  /// @method updateEdges
  void updateEdges() {
    final Map<String, bool> edges = {};
    final add = (num a, num b) {
      final key = a < b ? '''${a}_${b}''' : '''${b}_${a}''';
      edges[key] = true;
    };
    for (var i = 0; i < this.indices.length / 3; i++) {
      final i3 = i * 3;
      final a = this.indices[i3];
      final b = this.indices[i3 + 1];
      final c = this.indices[i3 + 2];
      add(a, b);
      add(b, c);
      add(c, a);
    }
    final keys = edges.keys.toList();
    this.edges = new Int16List(keys.length * 2);
    for (var i = 0; i < keys.length; i++) {
      final indices = keys[i].split("_");
      this.edges[2 * i] = int.parse(indices[0], radix: 10);
      this.edges[2 * i + 1] = int.parse(indices[1], radix: 10);
    }
  }

  /// Get an edge vertex
  /// @method getEdgeVertex
  void getEdgeVertex(num edgeIndex, num firstOrSecond, Vec3 vertexStore) {
    final vertexIndex =
        this.edges[edgeIndex * 2 + (firstOrSecond != null ? 1 : 0)];
    this.getVertex(vertexIndex, vertexStore);
  }

  /// Get a vector along an edge.
  /// @method getEdgeVector

  void getEdgeVector(num edgeIndex, Vec3 vectorStore) {
    final va = getEdgeVector_va;
    final vb = getEdgeVector_vb;
    this.getEdgeVertex(edgeIndex, 0, va);
    this.getEdgeVertex(edgeIndex, 1, vb);
    vb.vsub(va, vectorStore);
  }

  /**
   * Get vertex i.
   * @method getVertex
   *
   *
   *
   */
  Vec3 getVertex(num i, Vec3 out) {
    final scale = this.scale;
    this._getUnscaledVertex(i, out);
    out.x *= scale.x;
    out.y *= scale.y;
    out.z *= scale.z;
    return out;
  }

  /**
   * Get raw vertex i
   * @private
   * @method _getUnscaledVertex
   *
   *
   *
   */
  Vec3 _getUnscaledVertex(num i, Vec3 out) {
    final i3 = i * 3;
    final vertices = this.vertices;
    return out.set(vertices[i3], vertices[i3 + 1], vertices[i3 + 2]);
  }

  /**
   * Get a vertex from the trimesh,transformed by the given position and quaternion.
   * @method getWorldVertex
   *
   *
   *
   *
   *
   */
  Vec3 getWorldVertex(num i, Vec3 pos, Quaternion quat, Vec3 out) {
    this.getVertex(i, out);
    Transform.pointToWorldFrame(pos, quat, out, out);
    return out;
  }

  /**
   * Get the three vertices for triangle i.
   * @method getTriangleVertices
   *
   *
   *
   *
   */
  void getTriangleVertices(num i, Vec3 a, Vec3 b, Vec3 c) {
    final i3 = i * 3;
    this.getVertex(this.indices[i3], a);
    this.getVertex(this.indices[i3 + 1], b);
    this.getVertex(this.indices[i3 + 2], c);
  }

  /**
   * Compute the normal of triangle i.
   * @method getNormal
   *
   *
   *
   */
  Vec3 getNormal(num i, Vec3 target) {
    final i3 = i * 3;
    return target.set(
        this.normals[i3], this.normals[i3 + 1], this.normals[i3 + 2]);
  }

  /**
   * @method calculateLocalInertia
   *
   *
   *
   */
  Vec3 calculateLocalInertia(num mass, Vec3 target) {
    // Approximate with box inertia

    // Exact inertia calculation is overkill, but see http://geometrictools.com/Documentation/PolyhedralMassProperties.pdf for the correct way to do it
    this.computeLocalAABB(cli_aabb);
    final x = cli_aabb.upperBound.x - cli_aabb.lowerBound.x;
    final y = cli_aabb.upperBound.y - cli_aabb.lowerBound.y;
    final z = cli_aabb.upperBound.z - cli_aabb.lowerBound.z;
    return target.set(
        (1.0 / 12.0) * mass * (2 * y * 2 * y + 2 * z * 2 * z),
        (1.0 / 12.0) * mass * (2 * x * 2 * x + 2 * z * 2 * z),
        (1.0 / 12.0) * mass * (2 * y * 2 * y + 2 * x * 2 * x));
  }

  /**
   * Compute the local AABB for the trimesh
   * @method computeLocalAABB
   *
   */
  void computeLocalAABB(AABB aabb) {
    final l = aabb.lowerBound;
    final u = aabb.upperBound;
    final n = this.vertices.length;
    final vertices = this.vertices;
    final v = computeLocalAABB_worldVert;
    this.getVertex(0, v);
    l.copy(v);
    u.copy(v);
    for (var i = 0; !identical(i, n); i++) {
      this.getVertex(i, v);
      if (v.x < l.x) {
        l.x = v.x;
      } else if (v.x > u.x) {
        u.x = v.x;
      }
      if (v.y < l.y) {
        l.y = v.y;
      } else if (v.y > u.y) {
        u.y = v.y;
      }
      if (v.z < l.z) {
        l.z = v.z;
      } else if (v.z > u.z) {
        u.z = v.z;
      }
    }
  }

  /**
   * Update the .aabb property
   * @method updateAABB
   */
  void updateAABB() {
    this.computeLocalAABB(this.aabb);
  }

  /**
   * Will update the .boundingSphereRadius property
   * @method updateBoundingSphereRadius
   */
  void updateBoundingSphereRadius() {
    // Assume points are distributed with local (0,0,0) as center
    var max2 = 0;
    final vertices = this.vertices;
    final v = new Vec3();
    for (var i = 0, N = vertices.length / 3; !identical(i, N); i++) {
      this.getVertex(i, v);
      final norm2 = v.lengthSquared();
      if (norm2 > max2) {
        max2 = norm2;
      }
    }
    this.boundingSphereRadius = sqrt(max2);
  }

  /**
   * @method calculateWorldAABB
   *
   *
   *
   *
   */
  calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    /*
        const n = this.vertices.length / 3,
            verts = this.vertices;
        const minx,miny,minz,maxx,maxy,maxz;

        const v = tempWorldVertex;
        for(let i=0; i<n; i++){
            this.getVertex(i, v);
            quat.vmult(v, v);
            pos.vadd(v, v);
            if (v.x < minx || minx===undefined){
                minx = v.x;
            } else if(v.x > maxx || maxx===undefined){
                maxx = v.x;
            }

            if (v.y < miny || miny===undefined){
                miny = v.y;
            } else if(v.y > maxy || maxy===undefined){
                maxy = v.y;
            }

            if (v.z < minz || minz===undefined){
                minz = v.z;
            } else if(v.z > maxz || maxz===undefined){
                maxz = v.z;
            }
        }
        min.set(minx,miny,minz);
        max.set(maxx,maxy,maxz);
        */

    // Faster approximation using local AABB
    final frame = calculateWorldAABB_frame;
    final result = calculateWorldAABB_aabb;
    frame.position = pos;
    frame.quaternion = quat;
    this.aabb.toWorldFrame(frame, result);
    min.copy(result.lowerBound);
    max.copy(result.upperBound);
  }

  /**
   * Get approximate volume
   * @method volume
   *
   */
  volume() {
    return (4.0 * pi * this.boundingSphereRadius) / 3.0;
  }
}

final computeNormals_n = new Vec3();

final unscaledAABB = new AABB();

final getEdgeVector_va = new Vec3();

final getEdgeVector_vb = new Vec3();

final cb = new Vec3();

final ab = new Vec3();

final va = new Vec3();

final vb = new Vec3();

final vc = new Vec3();

final cli_aabb = new AABB();

final computeLocalAABB_worldVert = new Vec3();

final calculateWorldAABB_frame = new Transform();

final calculateWorldAABB_aabb = new AABB();
