import "../shapes/Shape.dart" show Shape;
import "../math/Vec3.dart" show Vec3;
import "../math/Quaternion.dart" show Quaternion;
import 'Shape.dart';

/**
 * Particle shape.
 * @class Particle
 * @constructor
 * @author schteppe
 * @extends Shape
 */
class Particle extends Shape {
  Particle() : super(SHAPE_TYPES.PARTICLE) {
    /* super call moved to initializer */;
  }

  /**
   * @method calculateLocalInertia
   *
   *
   *
   */
  Vec3 calculateLocalInertia(num mass, target) {
    target ??= Vec3();
    target.set(0, 0, 0);
    return target;
  }

  num volume() {
    return 0;
  }

  void updateBoundingSphereRadius() {
    this.boundingSphereRadius = 0;
  }

  void calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    // Get each axis max
    min.copy(pos);
    max.copy(pos);
  }
}
