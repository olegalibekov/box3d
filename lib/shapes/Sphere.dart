import 'dart:math';

import "../shapes/Shape.dart" show Shape;
import "../math/Vec3.dart" show Vec3;
import "../math/Quaternion.dart" show Quaternion;
import 'Shape.dart';

/**
 * Spherical shape
 * @class Sphere
 * @constructor
 * @extends Shape
 *
 * @author schteppe / http://github.com/schteppe
 */
class Sphere extends Shape {
  num radius;

  Sphere(num radius) : super(SHAPE_TYPES.SPHERE) {
    /* super call moved to initializer */;
    this.radius = !identical(radius, null) ? radius : 1.0;
    if (this.radius < 0) {
      throw ("The sphere radius cannot be negative.");
    }
    this.updateBoundingSphereRadius();
  }

  Vec3 calculateLocalInertia(num mass, target) {
    target ??= Vec3();
    final I = (2.0 * mass * this.radius * this.radius) / 5.0;
    target.x = I;
    target.y = I;
    target.z = I;
    return target;
  }

  num volume() {
    return (4.0 * pi * pow(this.radius, 3)) / 3.0;
  }

  void updateBoundingSphereRadius() {
    this.boundingSphereRadius = this.radius;
  }

  void calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    final r = this.radius;
    final axes = ["x", "y", "z"];
    for (var i = 0; i < axes.length; i++) {
      final ax = axes[i];
      if (ax == "x") {
        min.x = pos.x - r;
        max.x = pos.x + r;
      } else if (ax == "y") {
        min.y = pos.y - r;
        max.y = pos.y + r;
      } else if (ax == "z") {
        min.z = pos.z - r;
        max.z = pos.z + r;
      }
    }
  }
}
