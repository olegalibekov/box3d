import "../shapes/Shape.dart" show Shape, SHAPE_TYPES;
import "../math/Vec3.dart" show Vec3;
import "../shapes/ConvexPolyhedron.dart" show ConvexPolyhedron;
import "../math/Quaternion.dart" show Quaternion;

/**
 * A 3d box shape.
 * @class Box
 * @constructor
 *
 * @author schteppe
 * @extends Shape
 */
class Box extends Shape {
  Vec3 halfExtents;

  ConvexPolyhedron convexPolyhedronRepresentation;

  static dynamic calculateInertia = (Vec3 halfExtents, num mass, Vec3 target) {
    final e = halfExtents;
    target.x = (1.0 / 12.0) * mass * (2 * e.y * 2 * e.y + 2 * e.z * 2 * e.z);
    target.y = (1.0 / 12.0) * mass * (2 * e.x * 2 * e.x + 2 * e.z * 2 * e.z);
    target.z = (1.0 / 12.0) * mass * (2 * e.y * 2 * e.y + 2 * e.x * 2 * e.x);
  };

  Box(Vec3 halfExtents) : super(SHAPE_TYPES.BOX) {
    /* super call moved to initializer */;
    this.halfExtents = halfExtents;
    this.convexPolyhedronRepresentation = null;
    this.updateConvexPolyhedronRepresentation();
    this.updateBoundingSphereRadius();
  }

  /**
   * Updates the local convex polyhedron representation used for some collisions.
   * @method updateConvexPolyhedronRepresentation
   */
  void updateConvexPolyhedronRepresentation() {
    final sx = this.halfExtents.x;
    final sy = this.halfExtents.y;
    final sz = this.halfExtents.z;
    final vertices = [
      new Vec3(-sx, -sy, -sz),
      new Vec3(sx, -sy, -sz),
      new Vec3(sx, sy, -sz),
      new Vec3(-sx, sy, -sz),
      new Vec3(-sx, -sy, sz),
      new Vec3(sx, -sy, sz),
      new Vec3(sx, sy, sz),
      new Vec3(-sx, sy, sz)
    ];
    final faces = [
      [3, 2, 1, 0],
      [4, 5, 6, 7],
      [5, 4, 0, 1],
      [2, 3, 7, 6],
      [0, 4, 7, 3],
      [1, 2, 6, 5]
    ];
    final axes = [new Vec3(0, 0, 1), new Vec3(0, 1, 0), new Vec3(1, 0, 0)];
    final h =
        new ConvexPolyhedron(vertices: vertices, faces: faces, axes: axes);
    this.convexPolyhedronRepresentation = h;
    h.material = this.material;
  }

  /**
   * @method calculateLocalInertia
   *
   *
   *
   */
  Vec3 calculateLocalInertia(num mass, [target]) {
    if (target == null) target = Vec3();
    Box.calculateInertia(this.halfExtents, mass, target);
    return target;
  }

  /**
   * Get the box 6 side normals
   * @method getSideNormals
   *
   *
   *
   */
  List<Vec3> getSideNormals(List<Vec3> sixTargetVectors, Quaternion quat) {
    final sides = sixTargetVectors;
    final ex = this.halfExtents;
    sides[0].set(ex.x, 0, 0);
    sides[1].set(0, ex.y, 0);
    sides[2].set(0, 0, ex.z);
    sides[3].set(-ex.x, 0, 0);
    sides[4].set(0, -ex.y, 0);
    sides[5].set(0, 0, -ex.z);
    if (quat != null) {
      for (var i = 0; !identical(i, sides.length); i++) {
        quat.vmult(sides[i], sides[i]);
      }
    }
    return sides;
  }

  num volume() {
    return 8.0 * this.halfExtents.x * this.halfExtents.y * this.halfExtents.z;
  }

  void updateBoundingSphereRadius() {
    this.boundingSphereRadius = this.halfExtents.length();
  }

  void forEachWorldCorner(
      Vec3 pos, Quaternion quat, void callback(num x, num y, num z)) {
    final e = this.halfExtents;
    final corners = [
      [e.x, e.y, e.z],
      [-e.x, e.y, e.z],
      [-e.x, -e.y, e.z],
      [-e.x, -e.y, -e.z],
      [e.x, -e.y, -e.z],
      [e.x, e.y, -e.z],
      [-e.x, e.y, -e.z],
      [e.x, -e.y, e.z]
    ];
    for (var i = 0; i < corners.length; i++) {
      worldCornerTempPos.set(corners[i][0], corners[i][1], corners[i][2]);
      quat.vmult(worldCornerTempPos, worldCornerTempPos);
      pos.vadd(worldCornerTempPos, worldCornerTempPos);
      callback(
          worldCornerTempPos.x, worldCornerTempPos.y, worldCornerTempPos.z);
    }
  }

  void calculateWorldAABB(Vec3 pos, Quaternion quat, Vec3 min, Vec3 max) {
    final e = this.halfExtents;
    worldCornersTemp[0].set(e.x, e.y, e.z);
    worldCornersTemp[1].set(-e.x, e.y, e.z);
    worldCornersTemp[2].set(-e.x, -e.y, e.z);
    worldCornersTemp[3].set(-e.x, -e.y, -e.z);
    worldCornersTemp[4].set(e.x, -e.y, -e.z);
    worldCornersTemp[5].set(e.x, e.y, -e.z);
    worldCornersTemp[6].set(-e.x, e.y, -e.z);
    worldCornersTemp[7].set(e.x, -e.y, e.z);
    final wc = worldCornersTemp[0];
    quat.vmult(wc, wc);
    pos.vadd(wc, wc);
    max.copy(wc);
    min.copy(wc);
    for (var i = 1; i < 8; i++) {
      final wc = worldCornersTemp[i];
      quat.vmult(wc, wc);
      pos.vadd(wc, wc);
      final x = wc.x;
      final y = wc.y;
      final z = wc.z;
      if (x > max.x) {
        max.x = x;
      }
      if (y > max.y) {
        max.y = y;
      }
      if (z > max.z) {
        max.z = z;
      }
      if (x < min.x) {
        min.x = x;
      }
      if (y < min.y) {
        min.y = y;
      }
      if (z < min.z) {
        min.z = z;
      }
    }
  }
}

final worldCornerTempPos = new Vec3();

final worldCornersTemp = [
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3(),
  new Vec3()
];
