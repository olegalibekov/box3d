class MaterialOptions {
  num friction, restitution;
}

/**
 * Defines a physics material.
 * @class Material
 * @constructor
 *
 * @author schteppe
 */
class Material {
  String name;

  num id;

  num friction;

  num restitution;

  static num idCounter = 0;

  Material([dynamic options]) {
    var name = "";
    if (options is String) {
      name = options;
      return;
    } else {
      if (options == null) options = MaterialOptions();
    }

    this.name = name;
    this.id = Material.idCounter++;
    this.friction = options.friction ?? -1;
    this.restitution = options.restitution ?? -1;
  }
}
