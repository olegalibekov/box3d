import "../material/Material.dart" show Material;
import "../utils/Utils.dart" show Utils;

/// Defines what happens when two materials meet.
/// @class ContactMaterial
/// @constructor
/// @todo Refactor materials to materialA and materialB
class ContactMaterial {
  num id;

  var materials;

  num friction;

  num restitution;

  num contactEquationStiffness;

  num contactEquationRelaxation;

  num frictionEquationStiffness;

  num frictionEquationRelaxation;

  static num idCounter = 0;

  ContactMaterial(Material m1, Material m2, options) {
    options = Utils.defaults(options, <String, dynamic>{
      "friction": 0.3,
      "restitution": 0.3,
      "contactEquationStiffness": 1e7,
      "contactEquationRelaxation": 3,
      "frictionEquationStiffness": 1e7,
      "frictionEquationRelaxation": 3
    });
    this.id = ContactMaterial.idCounter++;
    this.materials = [m1, m2];
    this.friction = options['friction'];
    this.restitution = options['restitution'];
    this.contactEquationStiffness = options['contactEquationStiffness'];
    this.contactEquationRelaxation = options['contactEquationRelaxation'];
    this.frictionEquationStiffness = options['frictionEquationStiffness'];
    this.frictionEquationRelaxation = options['frictionEquationRelaxation'];
  }
}
