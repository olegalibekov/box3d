import "../math/Quaternion.dart" show Quaternion;
import "../math/Vec3.dart" show Vec3;

class TransformOptions {
  Vec3 position;
  Quaternion quaternion;
}

class Transform {
  Vec3 position;
  Quaternion quaternion;

  Transform([TransformOptions options]) {
    if (options == null) options = TransformOptions();
    this.position = new Vec3();
    this.quaternion = new Quaternion();
    if (options.position != null) {
      this.position.copy(options.position);
    }
    if (options.quaternion != null) {
      this.quaternion.copy(options.quaternion);
    }
  }

  /**
   * Get a global point in local transform coordinates.
   */
  Vec3 pointToLocal(Vec3 worldPoint, [Vec3 result]) {
    return Transform.pointToLocalFrame(
        this.position, this.quaternion, worldPoint, result);
  }

  /**
   * Get a local point in global transform coordinates.
   */
  Vec3 pointToWorld(Vec3 localPoint, [Vec3 result]) {
    return Transform.pointToWorldFrame(
        this.position, this.quaternion, localPoint, result);
  }

  Vec3 vectorToWorldFrame(Vec3 localVector, [result]) {
    if (result == null) result = Vec3();
    this.quaternion.vmult(localVector, result);
    return result;
  }

  static Vec3 pointToLocalFrame(
      Vec3 position, Quaternion quaternion, Vec3 worldPoint,
      [result]) {
    if (result == null) result = Vec3();
    worldPoint.vsub(position, result);
    quaternion.conjugate(tmpQuat);
    tmpQuat.vmult(result, result);
    return result;
  }

  static Vec3 pointToWorldFrame(
      Vec3 position, Quaternion quaternion, Vec3 localPoint,
      [result]) {
    if (result == null) result = Vec3();
    quaternion.vmult(localPoint, result);
    result.vadd(position, result);
    return result;
  }

  static Vec3 vectorToWorldFrameStatic(Quaternion quaternion, Vec3 localVector,
      [result ]) {
    if (result == null) result = Vec3();
    quaternion.vmult(localVector, result);
    return result;
  }

  static Vec3 vectorToLocalFrame(
      Vec3 position, Quaternion quaternion, Vec3 worldVector,
      [result ]) {
    if (result == null) result = Vec3();
    quaternion.w *= -1;
    quaternion.vmult(worldVector, result);
    quaternion.w *= -1;
    return result;
  }
}

final tmpQuat = new Quaternion();
