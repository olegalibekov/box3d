import "../math/Quaternion.dart" show Quaternion;
import "../math/Vec3.dart" show Vec3;

/**
 * A 3x3 matrix.
 * @class Mat3
 * @constructor
 *
 * @author schteppe / http://github.com/schteppe
 */
class Mat3 {
  List<double> elements;

  Mat3([elements]) {
    this.elements = elements ?? <double>[0, 0, 0, 0, 0, 0, 0, 0, 0];
  }

  /**
   * Sets the matrix to identity
   * @method identity
   * @todo Should perhaps be renamed to setIdentity() to be more clear.
   * @todo Create another function that immediately creates an identity matrix eg. eye()
   */
  void identity() {
    final e = this.elements;
    e[0] = 1;
    e[1] = 0;
    e[2] = 0;
    e[3] = 0;
    e[4] = 1;
    e[5] = 0;
    e[6] = 0;
    e[7] = 0;
    e[8] = 1;
  }

  /**
   * Set all elements to zero
   * @method setZero
   */
  void setZero() {
    final e = this.elements;
    e[0] = 0;
    e[1] = 0;
    e[2] = 0;
    e[3] = 0;
    e[4] = 0;
    e[5] = 0;
    e[6] = 0;
    e[7] = 0;
    e[8] = 0;
  }

  /**
   * Sets the matrix diagonal elements from a Vec3
   * @method setTrace
   *
   */
  void setTrace(Vec3 vector) {
    final e = this.elements;
    e[0] = vector.x;
    e[4] = vector.y;
    e[8] = vector.z;
  }

  /**
   * Gets the matrix diagonal elements
   * @method getTrace
   *
   */
  void getTrace([target]) {
    if (target == null) {
      target = Vec3();
    }
    final e = this.elements;
    target.x = e[0];
    target.y = e[4];
    target.z = e[8];
  }

  /**
   * Matrix-Vector multiplication
   * @method vmult
   *
   *
   */
  Vec3 vmult(Vec3 v, [target]) {
    if (target == null) {
      target = Vec3();
    }
    final e = this.elements;
    final x = v.x;
    final y = v.y;
    final z = v.z;
    target.x = e[0] * x + e[1] * y + e[2] * z;
    target.y = e[3] * x + e[4] * y + e[5] * z;
    target.z = e[6] * x + e[7] * y + e[8] * z;
    return target;
  }

  /**
   * Matrix-scalar multiplication
   * @method smult
   *
   */
  void smult(num s) {
    for (var i = 0; i < this.elements.length; i++) {
      this.elements[i] *= s;
    }
  }

  /**
   * Matrix multiplication
   * @method mmult
   *
   *
   */
  Mat3 mmult(Mat3 matrix, [Mat3 target]) {
    if (target == null) {
      target = Mat3();
    }
    final elements = matrix.elements;
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        var sum = 0.0;
        for (var k = 0; k < 3; k++) {
          sum += elements[i + k * 3] * this.elements[k + j * 3];
        }
        target.elements[i + j * 3] = sum;
      }
    }
    return target;
  }

  /**
   * Scale each column of the matrix
   * @method scale
   *
   *
   */
  Mat3 scale(Vec3 vector, [target]) {
    if (target == null) {
      target = Mat3();
    }
    final e = this.elements;
    final t = target.elements.toList();
    for (var i = 0; !identical(i, 3); i++) {
      t[3 * i + 0] = vector.x * e[3 * i + 0];
      t[3 * i + 1] = vector.y * e[3 * i + 1];
      t[3 * i + 2] = vector.z * e[3 * i + 2];
    }
    return target;
  }

  /**
   * Solve Ax=b
   * @method solve
   *
   *
   *
   * @todo should reuse arrays
   */
  Vec3 solve(Vec3 b, [target]) {
    if (target == null) {
      target = Vec3();
    }
    // Construct equations
    const nr = 3;
    const nc = 4;
    final eqns = [];
    num i;
    num j;
    for (i = 0; i < nr * nc; i++) {
      eqns.add(0);
    }
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
        eqns[i + nc * j] = this.elements[i + 3 * j];
      }
    }
    eqns[3 + 4 * 0] = b.x;
    eqns[3 + 4 * 1] = b.y;
    eqns[3 + 4 * 2] = b.z;
    // Compute right upper triangular version of the matrix - Gauss elimination
    var n = 3;
    final k = n;
    var np;
    const kp = 4;
    var p;
    do {
      i = k - n;
      if (identical(eqns[i + nc * i], 0)) {
        // the pivot is null, swap lines
        for (j = i + 1; j < k; j++) {
          if (!identical(eqns[i + nc * j], 0)) {
            np = kp;
            do {
              // do ligne( i ) = ligne( i ) + ligne( k )
              p = kp - np;
              eqns[p + nc * i] += eqns[p + nc * j];
            } while (--np);
            break;
          }
        }
      }
      if (!identical(eqns[i + nc * i], 0)) {
        for (j = i + 1; j < k; j++) {
          final num multiplier = eqns[i + nc * j] / eqns[i + nc * i];
          np = kp;
          do {
            // do ligne( k ) = ligne( k ) - multiplier * ligne( i )
            p = kp - np;
            eqns[p + nc * j] =
                p <= i ? 0 : eqns[p + nc * j] - eqns[p + nc * i] * multiplier;
          } while (--np);
        }
      }
    } while (--n != null);
    // Get the solution
    target.z = eqns[2 * nc + 3] / eqns[2 * nc + 2];
    target.y =
        (eqns[1 * nc + 3] - eqns[1 * nc + 2] * target.z) / eqns[1 * nc + 1];
    target.x = (eqns[0 * nc + 3] -
            eqns[0 * nc + 2] * target.z -
            eqns[0 * nc + 1] * target.y) /
        eqns[0 * nc + 0];
    if (target.x.isNan ||
        target.y.isNan ||
        target.z.isNan ||
        identical(target.x, double.infinity) ||
        identical(target.y, double.infinity) ||
        identical(target.z, double.infinity)) {
      throw '''Could not solve equation! Got x=[${target.toString()}], b=[${b.toString()}], A=[${this.toString()}]''';
    }
    return target;
  }

  /**
   * Get an element in the matrix by index. Index starts at 0, not 1!!!
   * @method e
   *
   *
   *
   *
   */

  dynamic /* num | void */ e(num row, num column, [num value]) {
    if (identical(value, null)) {
      return this.elements[column + 3 * row];
    } else {
      // Set value
      this.elements[column + 3 * row] = value;
    }
  }

  /**
   * Copy another matrix into this matrix object.
   * @method copy
   *
   *
   */
  Mat3 copy(Mat3 matrix) {
    for (var i = 0; i < matrix.elements.length; i++) {
      this.elements[i] = matrix.elements[i];
    }
    return this;
  }

  /**
   * Returns a string representation of the matrix.
   * @method toString
   *
   */
  String toString() {
    var r = "";
    const sep = ",";
    for (var i = 0; i < 9; i++) {
      r += (this.elements[i]).toString() + sep;
    }
    return r;
  }

  /**
   * reverse the matrix
   * @method reverse
   *
   *
   */
  Mat3 reverse([target]) {
    if (target == null) {
      target = Mat3();
    }
    // Construct equations
    const nr = 3;
    const nc = 6;
    final eqns = [];
    num i;
    num j;
    for (i = 0; i < nr * nc; i++) {
      eqns.add(0);
    }
    for (i = 0; i < 3; i++) {
      for (j = 0; j < 3; j++) {
        eqns[i + nc * j] = this.elements[i + 3 * j];
      }
    }
    eqns[3 + 6 * 0] = 1;
    eqns[3 + 6 * 1] = 0;
    eqns[3 + 6 * 2] = 0;
    eqns[4 + 6 * 0] = 0;
    eqns[4 + 6 * 1] = 1;
    eqns[4 + 6 * 2] = 0;
    eqns[5 + 6 * 0] = 0;
    eqns[5 + 6 * 1] = 0;
    eqns[5 + 6 * 2] = 1;
    // Compute right upper triangular version of the matrix - Gauss elimination
    var n = 3;
    final k = n;
    var np;
    final kp = nc;
    var p;
    do {
      i = k - n;
      if (identical(eqns[i + nc * i], 0)) {
        // the pivot is null, swap lines
        for (j = i + 1; j < k; j++) {
          if (!identical(eqns[i + nc * j], 0)) {
            np = kp;
            do {
              // do line( i ) = line( i ) + line( k )
              p = kp - np;
              eqns[p + nc * i] += eqns[p + nc * j];
            } while (--np);
            break;
          }
        }
      }
      if (!identical(eqns[i + nc * i], 0)) {
        for (j = i + 1; j < k; j++) {
          final num multiplier = eqns[i + nc * j] / eqns[i + nc * i];
          np = kp;
          do {
            // do line( k ) = line( k ) - multiplier * line( i )
            p = kp - np;
            eqns[p + nc * j] =
                p <= i ? 0 : eqns[p + nc * j] - eqns[p + nc * i] * multiplier;
          } while (--np);
        }
      }
    } while (--n != null);
    // eliminate the upper left triangle of the matrix
    i = 2;
    do {
      j = i - 1;
      do {
        final num multiplier = eqns[i + nc * j] / eqns[i + nc * i];
        np = nc;
        do {
          p = nc - np;
          eqns[p + nc * j] = eqns[p + nc * j] - eqns[p + nc * i] * multiplier;
        } while (--np);
      } while (j-- != null);
    } while (--i != null);
    // operations on the diagonal
    i = 2;
    do {
      final num multiplier = 1 / eqns[i + nc * i];
      np = nc;
      do {
        p = nc - np;
        eqns[p + nc * i] = eqns[p + nc * i] * multiplier;
      } while (--np);
    } while (i-- != null);
    i = 2;
    do {
      j = 2;
      do {
        p = eqns[nr + j + nc * i];
        if (p.isNan || identical(p, double.infinity)) {
          throw '''Could not reverse! A=[${this.toString()}]''';
        }
        target.e(i, j, p);
      } while (j-- != null);
    } while (i-- != null);
    return target;
  }

  /**
   * Set the matrix from a quaterion
   * @method setRotationFromQuaternion
   *
   */
  Mat3 setRotationFromQuaternion(Quaternion q) {
    final x = q.x;
    final y = q.y;
    final z = q.z;
    final w = q.w;
    final x2 = x + x;
    final y2 = y + y;
    final z2 = z + z;
    final xx = x * x2;
    final xy = x * y2;
    final xz = x * z2;
    final yy = y * y2;
    final yz = y * z2;
    final zz = z * z2;
    final wx = w * x2;
    final wy = w * y2;
    final wz = w * z2;
    final e = this.elements.toList();
    e[3 * 0 + 0] = 1 - (yy + zz) * 1.0;
    e[3 * 0 + 1] = xy - wz * 1.0;
    e[3 * 0 + 2] = xz + wy * 1.0;
    e[3 * 1 + 0] = xy + wz * 1.0;
    e[3 * 1 + 1] = 1 - (xx + zz) * 1.0;
    e[3 * 1 + 2] = yz - wx * 1.0;
    e[3 * 2 + 0] = xz - wy * 1.0;
    e[3 * 2 + 1] = yz + wx * 1.0;
    e[3 * 2 + 2] = 1 - (xx + yy) * 1.0;
    return this;
  }

  /**
   * Transpose the matrix
   * @method transpose
   *
   *
   */
  Mat3 transpose([target]) {
    if (target == null) {
      target = Mat3();
    }
    final Mt =
        target?.elements is List ? target.elements.toList() : target.elements;
    final M = this.elements.toList();
    for (var i = 0; !identical(i, 3); i++) {
      for (var j = 0; !identical(j, 3); j++) {
        Mt[3 * i + j] = M[3 * j + i];
      }
    }
    return target;
  }
}
