import 'dart:math';

import "../math/Vec3.dart" show Vec3;

/**
 * A Quaternion describes a rotation in 3D space. The Quaternion is mathematically defined as Q = x*i + y*j + z*k + w, where (i,j,k) are imaginary basis vectors. (x,y,z) can be seen as a vector related to the axis of rotation, while the real multiplier, w, is related to the amount of rotation.
 *
 *
 *
 *
 * @see http://en.wikipedia.org/wiki/Quaternion
 */
class Quaternion {
  num x;

  num y;

  num z;

  num w;

  Quaternion([ x = 0, y = 0, z = 0, w = 1 ]) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w;
  }

  /**
   * Set the value of the quaternion.
   */
  Quaternion set(num x, num y, num z, num w) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w;
    return this;
  }

  /**
   * Convert to a readable format
   *
   */
  String toString() {
    return '''${ this.x},${ this.y},${ this.z},${ this.w}''';
  }

  /**
   * Convert to an Array
   *
   */
  toArray() {
    return [ this.x, this.y, this.z, this.w];
  }

  /**
   * Set the quaternion components given an axis and an angle in radians.
   */
  Quaternion setFromAxisAngle(Vec3 vector, num angle) {
    final s = sin(angle * 0.5);
    this.x = vector.x * s;
    this.y = vector.y * s;
    this.z = vector.z * s;
    this.w = cos(angle * 0.5);
    return this;
  }

  /**
   * Converts the quaternion to [ axis, angle ] representation.
   *
   *
   */
  toAxisAngle([ targetAxis]) {
    if (targetAxis == null){
      targetAxis = Vec3();
    }
    this.normalize();
    final angle = 2 * acos(this.w);
    final s = sqrt(1 - this.w * this.w);
    if (s < 0.001) {
      // test to avoid divide by zero, s is always positive due to sqrt

      // if s close to zero then direction of axis not important
      targetAxis.x = this.x;
      targetAxis.y = this.y;
      targetAxis.z = this.z;
    } else {
      targetAxis.x = this.x / s;
      targetAxis.y = this.y / s;
      targetAxis.z = this.z / s;
    }
    return [ targetAxis, angle];
  }

  /**
   * Set the quaternion value given two vectors. The resulting rotation will be the needed rotation to rotate u to v.
   */
  Quaternion setFromVectors(Vec3 u, Vec3 v) {
    if (u.isAntiparallelTo(v)) {
      final t1 = sfv_t1;
      final t2 = sfv_t2;
      u.tangents(t1, t2);
      this.setFromAxisAngle(t1, pi);
    } else {
      final a = u.cross(v);
      this.x = a.x;
      this.y = a.y;
      this.z = a.z;
      this.w = sqrt(pow(u.length(), 2) * pow(v.length(), 2)) + u.dot(v);
      this.normalize();
    }
    return this;
  }

  /**
   * Multiply the quaternion with an other quaternion.
   */
  Quaternion mult(Quaternion quat, [ target]) {
    if (target == null){
      target = Quaternion ();
    }
    final ax = this.x;
    final ay = this.y;
    final az = this.z;
    final aw = this.w;
    final bx = quat.x;
    final by = quat.y;
    final bz = quat.z;
    final bw = quat.w;
    target.x = ax * bw + aw * bx + ay * bz - az * by;
    target.y = ay * bw + aw * by + az * bx - ax * bz;
    target.z = az * bw + aw * bz + ax * by - ay * bx;
    target.w = aw * bw - ax * bx - ay * by - az * bz;
    return target;
  }

  /**
   * Get the inverse quaternion rotation.
   */
  Quaternion inverse([ target ]) {
    if (target == null){
      target = Quaternion ();
    }
    final x = this.x;
    final y = this.y;
    final z = this.z;
    final w = this.w;
    this.conjugate(target);
    final inorm2 = 1 / (x * x + y * y + z * z + w * w);
    target.x *= inorm2;
    target.y *= inorm2;
    target.z *= inorm2;
    target.w *= inorm2;
    return target;
  }

  /**
   * Get the quaternion conjugate
   */
  Quaternion conjugate([ target]) {
    if (target == null){
      target =  Quaternion ();
    }
    target.x = -this.x;
    target.y = -this.y;
    target.z = -this.z;
    target.w = this.w;
    return target;
  }

  /**
   * Normalize the quaternion. Note that this changes the values of the quaternion.
   * @method normalize
   */
  Quaternion normalize() {
    var l = sqrt(
        this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w);
    if (identical(l, 0)) {
      this.x = 0;
      this.y = 0;
      this.z = 0;
      this.w = 0;
    } else {
      l = 1 / l;
      this.x *= l;
      this.y *= l;
      this.z *= l;
      this.w *= l;
    }
    return this;
  }

  /**
   * Approximation of quaternion normalization. Works best when quat is already almost-normalized.
   * @see http://jsperf.com/fast-quaternion-normalization
   * @author unphased, https://github.com/unphased
   */
  Quaternion normalizeFast() {
    final f = (3.0 - (this.x * this.x + this.y * this.y + this.z * this.z +
        this.w * this.w)) / 2.0;
    if (identical(f, 0)) {
      this.x = 0;
      this.y = 0;
      this.z = 0;
      this.w = 0;
    } else {
      this.x *= f;
      this.y *= f;
      this.z *= f;
      this.w *= f;
    }
    return this;
  }

  /**
   * Multiply the quaternion by a vector
   */
  Vec3 vmult(Vec3 v, [ target ]) {
    if (target == null){
      target = Vec3();
    }
    final x = v.x;
    final y = v.y;
    final z = v.z;
    final qx = this.x;
    final qy = this.y;
    final qz = this.z;
    final qw = this.w;
    // q*v
    final ix = qw * x + qy * z - qz * y;
    final iy = qw * y + qz * x - qx * z;
    final iz = qw * z + qx * y - qy * x;
    final iw = -qx * x - qy * y - qz * z;
    target.x = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    target.y = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    target.z = iz * qw + iw * -qz + ix * -qy - iy * -qx;
    return target;
  }

  /**
   * Copies value of source to this quaternion.
   * @method copy
   *
   *
   */
  Quaternion copy(Quaternion quat) {
    this.x = quat.x;
    this.y = quat.y;
    this.z = quat.z;
    this.w = quat.w;
    return this;
  }

  /**
   * Convert the quaternion to euler angle representation. Order: YZX, as this page describes: http://www.euclideanspace.com/maths/standards/index.htm
   * @method toEuler
   *
   *
   */
  void toEuler(Vec3 target, [ order = "YZX" ]) {
    var heading;
    var attitude;
    var bank;
    final x = this.x;
    final y = this.y;
    final z = this.z;
    final w = this.w;
    switch (order) {
      case "YZX" :
        final test = x * y + z * w;
        if (test > 0.499) {
          // singularity at north pole
          heading = 2 * atan2(x, w);
          attitude = pi / 2;
          bank = 0;
        }
        if (test < -0.499) {
          // singularity at south pole
          heading = -2 * atan2(x, w);
          attitude = -pi / 2;
          bank = 0;
        }
        if (identical(heading, null)) {
          final sqx = x * x;
          final sqy = y * y;
          final sqz = z * z;
          heading = atan2(2 * y * w - 2 * x * z, 1 - 2 * sqy - 2 * sqz);
          attitude = asin(2 * test);
          bank = atan2(2 * x * w - 2 * y * z, 1 - 2 * sqx - 2 * sqz);
        }
        break;
      default :
        throw ('''Euler order ${order} not supported yet.''');
    }
    target.y = heading;
    target.z = attitude as num;
    target.x = bank as num;
  }

  /**
   *
   *
   *
   *
   * @see http://www.mathworks.com/matlabcentral/fileexchange/20696-function-to-convert-between-dcm-euler-angles-quaternions-and-euler-vectors/content/SpinCalc.m
   */
  Quaternion setFromEuler(num x, num y, num z, [ order = "XYZ" ]) {
    final c1 = cos(x / 2);
    final c2 = cos(y / 2);
    final c3 = cos(z / 2);
    final s1 = sin(x / 2);
    final s2 = sin(y / 2);
    final s3 = sin(z / 2);
    if (identical(order, "XYZ")) {
      this.x = s1 * c2 * c3 + c1 * s2 * s3;
      this.y = c1 * s2 * c3 - s1 * c2 * s3;
      this.z = c1 * c2 * s3 + s1 * s2 * c3;
      this.w = c1 * c2 * c3 - s1 * s2 * s3;
    } else if (identical(order, "YXZ")) {
      this.x = s1 * c2 * c3 + c1 * s2 * s3;
      this.y = c1 * s2 * c3 - s1 * c2 * s3;
      this.z = c1 * c2 * s3 - s1 * s2 * c3;
      this.w = c1 * c2 * c3 + s1 * s2 * s3;
    } else if (identical(order, "ZXY")) {
      this.x = s1 * c2 * c3 - c1 * s2 * s3;
      this.y = c1 * s2 * c3 + s1 * c2 * s3;
      this.z = c1 * c2 * s3 + s1 * s2 * c3;
      this.w = c1 * c2 * c3 - s1 * s2 * s3;
    } else if (identical(order, "ZYX")) {
      this.x = s1 * c2 * c3 - c1 * s2 * s3;
      this.y = c1 * s2 * c3 + s1 * c2 * s3;
      this.z = c1 * c2 * s3 - s1 * s2 * c3;
      this.w = c1 * c2 * c3 + s1 * s2 * s3;
    } else if (identical(order, "YZX")) {
      this.x = s1 * c2 * c3 + c1 * s2 * s3;
      this.y = c1 * s2 * c3 + s1 * c2 * s3;
      this.z = c1 * c2 * s3 - s1 * s2 * c3;
      this.w = c1 * c2 * c3 - s1 * s2 * s3;
    } else if (identical(order, "XZY")) {
      this.x = s1 * c2 * c3 - c1 * s2 * s3;
      this.y = c1 * s2 * c3 - s1 * c2 * s3;
      this.z = c1 * c2 * s3 + s1 * s2 * c3;
      this.w = c1 * c2 * c3 + s1 * s2 * s3;
    }
    return this;
  }

  /**
   * @method clone
   *
   */
  Quaternion clone() {
    return new Quaternion (this.x, this.y, this.z, this.w);
  }

  /**
   * Performs a spherical linear interpolation between two quat
   *
   *
   *
   *
   *
   */
  Quaternion slerp(Quaternion toQuat, num t, [ target ]) {
    if (target == null ){
      target = Quaternion ();
    }
    final ax = this.x;
    final ay = this.y;
    final az = this.z;
    final aw = this.w;
    var bx = toQuat.x;
    var by = toQuat.y;
    var bz = toQuat.z;
    var bw = toQuat.w;
    var omega;
    var cosom;
    var sinom;
    var scale0;
    var scale1;
    // calc cosine
    cosom = ax * bx + ay * by + az * bz + aw * bw;
    // adjust signs (if necessary)
    if (cosom < 0.0) {
      cosom = -cosom;
      bx = -bx;
      by = -by;
      bz = -bz;
      bw = -bw;
    }
    // calculate coefficients
    if (1.0 - cosom > 0.000001) {
      // standard case (slerp)
      omega = acos(cosom);
      sinom = sin(omega);
      scale0 = sin((1.0 - t) * omega) / sinom;
      scale1 = sin(t * omega) / sinom;
    } else {
      // "from" and "to" quaternions are very close

      //  ... so we can do a linear interpolation
      scale0 = 1.0 - t;
      scale1 = t;
    }
    // calculate final values
    target.x = scale0 * ax + scale1 * bx;
    target.y = scale0 * ay + scale1 * by;
    target.z = scale0 * az + scale1 * bz;
    target.w = scale0 * aw + scale1 * bw;
    return target;
  }

  /**
   * Rotate an absolute orientation quaternion given an angular velocity and a time step.
   */
  Quaternion integrate(Vec3 angularVelocity, num dt, Vec3 angularFactor,
      [ target ]) {
    if (target == null){
      target = Quaternion ();
    }
    final ax = angularVelocity.x * angularFactor.x,
        ay = angularVelocity.y * angularFactor.y,
        az = angularVelocity.z * angularFactor.z,
        bx = this.x,
        by = this.y,
        bz = this.z,
        bw = this.w;
    final half_dt = dt * 0.5;
    target.x += half_dt * (ax * bw + ay * bz - az * by);
    target.y += half_dt * (ay * bw + az * bx - ax * bz);
    target.z += half_dt * (az * bw + ax * by - ay * bx);
    target.w += half_dt * (-ax * bx - ay * by - az * bz);
    return target;
  }
}

final sfv_t1 = new Vec3 ();

final sfv_t2 = new Vec3 ();