import "../math/Vec3.dart" show Vec3;

/**
 * An element containing 6 entries, 3 spatial and 3 rotational degrees of freedom.
 */
class JacobianElement {
  Vec3 spatial;
  Vec3 rotational;
  JacobianElement() {
    this.spatial = new Vec3();
    this.rotational = new Vec3();
  }
  /**
   * Multiply with other JacobianElement
   */
  num multiplyElement(JacobianElement element) {
    return element.spatial.dot(this.spatial) +
        element.rotational.dot(this.rotational);
  }

  /**
   * Multiply with two vectors
   */
  num multiplyVectors(Vec3 spatial, Vec3 rotational) {
    return spatial.dot(this.spatial) + rotational.dot(this.rotational);
  }
}
