import 'dart:math' as Math;

import "../math/Mat3.dart" show Mat3;

/**
 * 3-dimensional vector
 * @class Vec3
 * @constructor
 *
 *
 *
 * @author schteppe
 * @example
 *     const v = new Vec3(1, 2, 3);
 *     console.log('x=' + v.x); // x=1
 */
class Vec3 {
  num x;

  num y;

  num z;

  static Vec3 ZERO = new Vec3(0, 0, 0);

  static Vec3 UNIT_X = new Vec3(1, 0, 0);

  static Vec3 UNIT_Y = new Vec3(0, 1, 0);

  static Vec3 UNIT_Z = new Vec3(0, 0, 1);

  Vec3([x = 0.0, y = 0.0, z = 0.0])
      : this.x = x,
        this.y = y,
        this.z = z;

  /**
   * Vector cross product
   * @method cross
   *
   *
   *
   */
  Vec3 cross(Vec3 vector, [target]) {
    final vx = vector.x;
    final vy = vector.y;
    final vz = vector.z;
    final x = this.x;
    final y = this.y;
    final z = this.z;
    if (target == null) target = Vec3();
    target.x = y * vz - z * vy;
    target.y = z * vx - x * vz;
    target.z = x * vy - y * vx;
    return target;
  }

  /**
   * Set the vectors' 3 elements
   * @method set
   *
   *
   *
   *
   */
  Vec3 set(num x, num y, num z) {
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
  }

  /**
   * Set all components of the vector to zero.
   * @method setZero
   */
  void setZero() {
    this.x = this.y = this.z = 0;
  }

  /**
   * Vector addition
   * @method vadd
   *
   *
   *
   */

  dynamic /* Vec3 | void */ vadd(Vec3 vector, [Vec3 target]) {
    if (target != null) {
      target.x = vector.x + this.x;
      target.y = vector.y + this.y;
      target.z = vector.z + this.z;
    } else {
      return new Vec3(this.x + vector.x, this.y + vector.y, this.z + vector.z);
    }
  }

  /**
   * Vector subtraction
   * @method vsub
   *
   *
   *
   */

  dynamic vsub(Vec3 vector, [Vec3 target]) {
    if (target != null) {
      target.x = this.x - vector.x;
      target.y = this.y - vector.y;
      target.z = this.z - vector.z;
    } else {
      return new Vec3(this.x - vector.x, this.y - vector.y, this.z - vector.z);
    }
  }

  /**
   * Get the cross product matrix a_cross from a vector, such that a x b = a_cross * b = c
   * @method crossmat
   * @see http://www8.cs.umu.se/kurser/TDBD24/VT06/lectures/Lecture6.pdf
   *
   */
  Mat3 crossmat() {
    return new Mat3(
        [0, -this.z, this.y, this.z, 0, -this.x, -this.y, this.x, 0]);
  }

  /**
   * Normalize the vector. Note that this changes the values in the vector.
   * @method normalize
   *
   */
  num normalize() {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    final n = Math.sqrt(x * x + y * y + z * z);
    if (n > 0.0) {
      final invN = 1 / n;
      this.x *= invN;
      this.y *= invN;
      this.z *= invN;
    } else {
      // Make something up
      this.x = 0;
      this.y = 0;
      this.z = 0;
    }
    return n;
  }

  /**
   * Get the version of this vector that is of length 1.
   * @method unit
   *
   *
   */
  Vec3 unit([target]) {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    var ninv = Math.sqrt(x * x + y * y + z * z);
    if (target == null) target = Vec3();
    if (ninv > 0.0) {
      ninv = 1.0 / ninv;
      target.x = x * ninv;
      target.y = y * ninv;
      target.z = z * ninv;
    } else {
      target.x = 1;
      target.y = 0;
      target.z = 0;
    }
    return target;
  }

  /**
   * Get the length of the vector
   * @method length
   *
   */
  num length() {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    return Math.sqrt(x * x + y * y + z * z);
  }

  /**
   * Get the squared length of the vector.
   * @method lengthSquared
   *
   */
  num lengthSquared() {
    return this.dot(this);
  }

  /**
   * Get distance from this point to another point
   * @method distanceTo
   *
   *
   */
  num distanceTo(Vec3 p) {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    final px = p.x;
    final py = p.y;
    final pz = p.z;
    return Math.sqrt(
        (px - x) * (px - x) + (py - y) * (py - y) + (pz - z) * (pz - z));
  }

  /**
   * Get squared distance from this point to another point
   * @method distanceSquared
   *
   *
   */
  num distanceSquared(Vec3 p) {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    final px = p.x;
    final py = p.y;
    final pz = p.z;
    return (px - x) * (px - x) + (py - y) * (py - y) + (pz - z) * (pz - z);
  }

  /**
   * Multiply all the components of the vector with a scalar.
   * @method scale
   *
   *
   *
   */
  Vec3 scale(num scalar, [target]) {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    if (target == null) target = Vec3();
    target.x = scalar * x;
    target.y = scalar * y;
    target.z = scalar * z;
    return target;
  }

  /**
   * Multiply the vector with an other vector, component-wise.
   * @method vmult
   *
   *
   *
   */
  Vec3 vmul(Vec3 vector, [target]) {
    if (target == null) target = Vec3();
    target.x = vector.x * this.x;
    target.y = vector.y * this.y;
    target.z = vector.z * this.z;
    return target;
  }

  /**
   * Scale a vector and add it to this vector. Save the result in "target". (target = this + vector * scalar)
   * @method addScaledVector
   *
   *
   *
   *
   */
  Vec3 addScaledVector(num scalar, Vec3 vector, [target]) {
    if (target == null) target = Vec3();
    target.x = this.x + scalar * vector.x;
    target.y = this.y + scalar * vector.y;
    target.z = this.z + scalar * vector.z;
    return target;
  }

  /**
   * Calculate dot product
   * @method dot
   *
   *
   */
  num dot(Vec3 vector) {
    return this.x * vector.x + this.y * vector.y + this.z * vector.z;
  }

  /**
   * @method isZero
   *
   */
  bool isZero() {
    return identical(this.x, 0) && identical(this.y, 0) && identical(this.z, 0);
  }

  /**
   * Make the vector point in the opposite direction.
   * @method negate
   *
   *
   */
  Vec3 negate([target]) {
    if (target == null) target = Vec3();
    target.x = -this.x;
    target.y = -this.y;
    target.z = -this.z;
    return target;
  }

  /**
   * Compute two artificial tangents to the vector
   * @method tangents
   *
   *
   */
  void tangents(Vec3 t1, Vec3 t2) {
    final norm = this.length();
    if (norm > 0.0) {
      final n = Vec3_tangents_n;
      final inorm = 1 / norm;
      n.set(this.x * inorm, this.y * inorm, this.z * inorm);
      final randVec = Vec3_tangents_randVec;
      if ((n.x).abs() < 0.9) {
        randVec.set(1, 0, 0);
        n.cross(randVec, t1);
      } else {
        randVec.set(0, 1, 0);
        n.cross(randVec, t1);
      }
      n.cross(t1, t2);
    } else {
      // The normal length is zero, make something up
      t1.set(1, 0, 0);
      t2.set(0, 1, 0);
    }
  }

  /**
   * Converts to a more readable format
   * @method toString
   *
   */
  String toString() {
    return '''${this.x},${this.y},${this.z}''';
  }

  /**
   * Converts to an array
   * @method toArray
   *
   */
  toArray() {
    return [this.x, this.y, this.z];
  }

  /**
   * Copies value of source to this vector.
   * @method copy
   *
   *
   */
  Vec3 copy(Vec3 vector) {
    this.x = vector.x;
    this.y = vector.y;
    this.z = vector.z;
    return this;
  }

  /**
   * Do a linear interpolation between two vectors
   * @method lerp
   *
   *
   *
   */
  void lerp(Vec3 vector, num t, Vec3 target) {
    final x = this.x;
    final y = this.y;
    final z = this.z;
    target.x = x + (vector.x - x) * t;
    target.y = y + (vector.y - y) * t;
    target.z = z + (vector.z - z) * t;
  }

  /**
   * Check if a vector equals is almost equal to another one.
   * @method almostEquals
   *
   *
   *
   */
  bool almostEquals(Vec3 vector, [precision = 1e-6]) {
    if ((this.x - vector.x).abs() > precision ||
        (this.y - vector.y).abs() > precision ||
        (this.z - vector.z).abs() > precision) {
      return false;
    }
    return true;
  }

  /**
   * Check if a vector is almost zero
   * @method almostZero
   *
   */
  bool almostZero([precision = 1e-6]) {
    if ((this.x).abs() > precision ||
        (this.y).abs() > precision ||
        (this.z).abs() > precision) {
      return false;
    }
    return true;
  }

  /**
   * Check if the vector is anti-parallel to another vector.
   * @method isAntiparallelTo
   *
   *
   *
   */
  bool isAntiparallelTo(Vec3 vector, [num precision]) {
    this.negate(antip_neg);
    return antip_neg.almostEquals(vector, precision);
  }

  /**
   * Clone the vector
   * @method clone
   *
   */
  Vec3 clone() {
    return new Vec3(this.x, this.y, this.z);
  }
}

/**
 * Compute two artificial tangents to the vector
 * @method tangents
 *
 *
 */
final Vec3_tangents_n = new Vec3();

final Vec3_tangents_randVec = new Vec3();

final antip_neg = new Vec3();
